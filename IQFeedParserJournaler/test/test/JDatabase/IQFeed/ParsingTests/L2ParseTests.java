package test.JDatabase.IQFeed.ParsingTests;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.TimeZone;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.JDatabase.Events.ConnectionEvent;
import com.JDatabase.Events.DataEvent;
import com.JDatabase.Events.DepthEvent;
import com.JDatabase.Events.ErrorEvent;
import com.JDatabase.Events.EventType;
import com.JDatabase.Events.SymbolNotFoundEvent;
import com.JDatabase.IQFeed.Feed;
import com.JDatabase.IQFeed.IQFeedUpdateEvent;
import com.JDatabase.IQFeed.L2Events.L2Parser;
import com.JDatabase.Utils.StringUtils;

public class L2ParseTests {
	static IQFeedUpdateEvent iqFeedUpdate;

	@Before
	public void setUp() throws Exception {
		iqFeedUpdate = IQFeedUpdateEvent.EVENT_FACTORY.newInstance();
	}

	@After
	public void tearDown() throws Exception {
		iqFeedUpdate = null;
	}
	
	@Test
	public void testL2SystemMessages() {
		String[] L2_SYSTEM_MESSAGES = {
				"S,SERVER DISCONNECTED",
				"S,SERVER CONNECTED",
				"S,SERVER RECONNECT FAILED",
				"S,CURRENT PROTOCOL,5.1,"
		};
		for(String message: L2_SYSTEM_MESSAGES)
		{
			iqFeedUpdate.notifyStringMessage(Feed.L2, TimeZone.getDefault(), System.currentTimeMillis(), message);
			assertTrue(iqFeedUpdate.getFeedType() == Feed.L2);
			DataEvent[] out = L2Parser.getParsedMessage(iqFeedUpdate.getFeedType(), iqFeedUpdate.getTimeZone(), 
					iqFeedUpdate.getReceiveTime(), iqFeedUpdate.getMessage());
			if(message.equals("S,SERVER CONNECTED") && (
					out == null || ! (out[0] instanceof ConnectionEvent) ||
					((ConnectionEvent) out[0]).isConnected == false))	fail();
			else if(message.equals("S,SERVER DISCONNECTED") && (
					out == null || ! (out[0] instanceof ConnectionEvent) ||
					((ConnectionEvent) out[0]).isConnected == true))	fail();
			else if(message.equals("S,SERVER RECONNECT FAILED") &&
					out != null)fail();
			else if(message.equals("S,CURRENT PROTOCOL,5.1,") &&
					out != null)fail();

		}
		
		
	}
	@Test
	public void testL2ErrorMessage() {
		String[] L2_ERROR_MESSAGES = {
				"E,ERROR_MESSAGE1",
				"E,ERROR_MESSAGE2"
		};

		TimeZone timeZone = TimeZone.getDefault();
		long receiveTime = System.currentTimeMillis();
		
		for(String message: L2_ERROR_MESSAGES)
		{
			iqFeedUpdate.notifyStringMessage(Feed.L2, timeZone, receiveTime, message);
			assertTrue(iqFeedUpdate.getFeedType() == Feed.L2);
			String tokens[] = StringUtils.getCommaSplitString(message);
			DataEvent[] out = L2Parser.getParsedMessage(iqFeedUpdate.getFeedType(), iqFeedUpdate.getTimeZone(), 
					iqFeedUpdate.getReceiveTime(), iqFeedUpdate.getMessage());
			ErrorEvent errorMsg = new ErrorEvent(timeZone, receiveTime, tokens[1]);
			assertTrue(out != null && out.length == 1 && out[0] instanceof ErrorEvent);
			assertTrue(((ErrorEvent) out[0]).equals(errorMsg));

		}
	}
	
	@Test
	public void testL2SymbolNotFoundMessages() {
		String[] L2_SYMBOL_NOT_FOUND_MESSAGES = {
				"n,AAPL",
				"n,@ES#"
		};
		
		for(String message: L2_SYMBOL_NOT_FOUND_MESSAGES)
		{
			iqFeedUpdate.notifyStringMessage(Feed.L2, TimeZone.getDefault(), System.currentTimeMillis(), message);
			assertTrue(iqFeedUpdate.getFeedType() == Feed.L2);
			String tokens[] = StringUtils.getCommaSplitString(message);
			DataEvent[] out = L2Parser.getParsedMessage(iqFeedUpdate.getFeedType(), iqFeedUpdate.getTimeZone(), 
					iqFeedUpdate.getReceiveTime(), iqFeedUpdate.getMessage());
			assertTrue(out != null && out.length == 1 && out[0] instanceof SymbolNotFoundEvent && ((SymbolNotFoundEvent) out[0]).symbol.equals(tokens[1]));

		}
		
	}
	@Test
	public void testL2TimeStampMessages() {
		String[] L2_TIMESTAMP_MESSAGES = {
				"T,20131216 09:02:56",
				"T,20131216 09:02:57"
		};
		
		for(String message: L2_TIMESTAMP_MESSAGES)
		{
			iqFeedUpdate.notifyStringMessage(Feed.L2, TimeZone.getDefault(), System.currentTimeMillis(), message);
			assertTrue(iqFeedUpdate.getFeedType() == Feed.L2);
			DataEvent[] out = L2Parser.getParsedMessage(iqFeedUpdate.getFeedType(), iqFeedUpdate.getTimeZone(), 
					iqFeedUpdate.getReceiveTime(), iqFeedUpdate.getMessage());
			assertTrue(out == null);
		}
		
	}
	@Test
	public void testL2SummaryMessages() {
		String[] L2_SUMMARY_MESSAGES = {
				"Z,@ES#,MD04,1777.,1778.75,561,599,09:03:07.827,2013-12-16,52,09:03:09.344,T,T,T,",
				"Z,@ES#,MD05,1776.75,1779.,905,677,09:03:11.085,2013-12-16,52,09:03:07.867,T,T,T,",
				"Z,@ES#,MD06,1776.5,1779.25,554,592,09:03:10.266,2013-12-16,52,09:03:10.334,T,T,T,",
				"Z,@ES#,MD10,1775.5,1780.25,844,719,09:03:08.430,2013-12-16,52,09:03:08.430,T,T,T,",
				"Z,@ES#,MD07,1776.25,1779.5,727,757,09:03:08.010,2013-12-16,52,09:03:08.010,T,T,T,",
				"Z,@ES#,MD01,1777.75,1778.,232,185,09:03:11.023,2013-12-16,52,09:03:11.023,T,T,T,",
				"Z,@ES#,MD02,1777.5,1778.25,832,974,09:03:08.429,2013-12-16,52,09:03:07.734,T,T,T,",
				"Z,@ES#,MD03,1777.25,1778.5,731,871,09:03:08.429,2013-12-16,52,09:03:07.810,T,T,T,",
				"Z,@ES#,MD08,1776.,1779.75,773,703,09:03:08.728,2013-12-16,52,09:03:08.100,T,T,T,",
				"Z,@ES#,MD09,1775.75,1780.,573,995,09:03:08.191,2013-12-16,52,09:03:08.191,T,T,T,"
		};
		
		TimeZone timeZone = TimeZone.getDefault();
		long receiveTime = System.currentTimeMillis();
		for(String message: L2_SUMMARY_MESSAGES)
		{
			iqFeedUpdate.notifyStringMessage(Feed.L2, timeZone, receiveTime, message);
			assertTrue(iqFeedUpdate.getFeedType() == Feed.L2);
			DataEvent[] out = L2Parser.getParsedMessage(iqFeedUpdate.getFeedType(), iqFeedUpdate.getTimeZone(),
					iqFeedUpdate.getReceiveTime(), iqFeedUpdate.getMessage());
			int count = 0;
			
			String tokens[] = StringUtils.getCommaSplitString(message);

			if(tokens[11].equals("T"))
			{
				assertTrue(out != null && out[count] instanceof DepthEvent);
				DepthEvent bid = (DepthEvent) out[count++];
				DepthEvent bidTest;
				try {
					bidTest = new DepthEvent(timeZone, receiveTime, tokens[1], EventType.BID_DEPTH, tokens[2], tokens[8], tokens[7],
							StringUtils.getDoubleFromString(tokens[3], Double.MIN_VALUE), 
							StringUtils.getIntFromString(tokens[5], Integer.MIN_VALUE));
				} catch (ParseException e) {
					bidTest = null;
					e.printStackTrace();
				}
				assertTrue(bid.equals(bidTest));
			}
			if(tokens[12].equals("T"))
			{
				assertTrue(out != null && out[count] instanceof DepthEvent);
				DepthEvent ask = (DepthEvent) out[count++];
				DepthEvent askTest;
				try {
					askTest = new DepthEvent(timeZone, receiveTime, tokens[1], EventType.ASK_DEPTH, tokens[2], tokens[8], tokens[10],
							StringUtils.getDoubleFromString(tokens[4], Double.MIN_VALUE), 
							StringUtils.getIntFromString(tokens[6], Integer.MIN_VALUE));
				} catch (ParseException e) {
					askTest = null;
					e.printStackTrace();
				}
				assertTrue(ask.equals(askTest));
			}
		}
		
	}
	
	@Test
	public void testL2UpdateMessages() {
		String[] L2_UPDATE_MESSAGES = {
				"2,@ES#,MD01,1777.75,1778.,232,186,09:03:11.023,2013-12-16,52,09:03:11.366,T,T,T,",
				"2,@ES#,MD01,1777.75,1778.,233,186,09:03:12.091,2013-12-16,52,09:03:11.366,T,T,T,",
				"2,@ES#,MD06,1776.5,1779.25,552,592,09:03:12.092,2013-12-16,52,09:03:10.334,T,T,T,",
				"2,@ES#,MD05,1776.75,1779.,908,677,09:03:12.236,2013-12-16,52,09:03:07.867,T,T,T,",		
		};
		
		TimeZone timeZone = TimeZone.getDefault();
		long receiveTime = System.currentTimeMillis();
		for(String message: L2_UPDATE_MESSAGES)
		{
			iqFeedUpdate.notifyStringMessage(Feed.L2, timeZone, receiveTime, message);
			assertTrue(iqFeedUpdate.getFeedType() == Feed.L2);
			DataEvent[] out = L2Parser.getParsedMessage(iqFeedUpdate.getFeedType(), iqFeedUpdate.getTimeZone(),
					iqFeedUpdate.getReceiveTime(), iqFeedUpdate.getMessage());
			int count = 0;
			
			String tokens[] = StringUtils.getCommaSplitString(message);

			if(tokens[11].equals("T"))
			{
				assertTrue(out != null && out[count] instanceof DepthEvent);
				DepthEvent bid = (DepthEvent) out[count++];
				DepthEvent bidTest;
				try {
					bidTest = new DepthEvent(timeZone, receiveTime, tokens[1], EventType.BID_DEPTH, tokens[2], tokens[8], tokens[7],
							StringUtils.getDoubleFromString(tokens[3], Double.MIN_VALUE), 
							StringUtils.getIntFromString(tokens[5], Integer.MIN_VALUE));
				} catch (ParseException e) {
					bidTest = null;
					e.printStackTrace();
				}
				assertTrue(bid.equals(bidTest));
			}
			if(tokens[12].equals("T"))
			{
				assertTrue(out != null && out[count] instanceof DepthEvent);
				DepthEvent ask = (DepthEvent) out[count++];
				DepthEvent askTest;
				try {
					askTest = new DepthEvent(timeZone, receiveTime, tokens[1], EventType.ASK_DEPTH, tokens[2], tokens[8], tokens[10],
							StringUtils.getDoubleFromString(tokens[4], Double.MIN_VALUE), 
							StringUtils.getIntFromString(tokens[6], Integer.MIN_VALUE));
				} catch (ParseException e) {
					askTest = null;
					e.printStackTrace();
				}
				assertTrue(ask.equals(askTest));
			}
		}
	}
	
	

}
