package test.JDatabase.IQFeed.ParsingTests;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.TimeZone;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.JDatabase.Events.ConnectionEvent;
import com.JDatabase.Events.DataEvent;
import com.JDatabase.Events.ErrorEvent;
import com.JDatabase.Events.EventType;
import com.JDatabase.Events.FundamentalEvent;
import com.JDatabase.Events.TradeEvent;
import com.JDatabase.Events.SymbolNotFoundEvent;
import com.JDatabase.Events.TOBEvent;
import com.JDatabase.IQFeed.Feed;
import com.JDatabase.IQFeed.IQFeedUpdateEvent;
import com.JDatabase.IQFeed.L1Events.L1Events;
import com.JDatabase.IQFeed.L1Events.L1Parser;
import com.JDatabase.Utils.StringUtils;

public class L1ParseTests {
	static IQFeedUpdateEvent iqFeedUpdate;

	@Before
	public void setUp() throws Exception {
		iqFeedUpdate = IQFeedUpdateEvent.EVENT_FACTORY.newInstance();
	}

	@After
	public void tearDown() throws Exception {
		iqFeedUpdate = null;
	}

	@Test
	public void testL1SystemMessages(){
		String[] L1_SYSTEM_MESSAGES = {
				"S,KEY,99999",
				"S,SERVER CONNECTED",
				"S,SERVER DISCONNECTED",
				"S,IP,66.112.156.181 60004,66.112.156.218 60002,66.112.156.213 60009,66.112.156.210 60001,66.112.156.211 60005,66.112.156.210 60012,66.112.156.200 60003,66.112.156.209 60015,66.112.156.216 60050,66.112.156.210 60014,66.112.156.211 60016,66.112.156.209 60018",
				"S,CUST,real_time,66.112.156.113,60003,h8A2F0i,5.1.0.3,0, NYSE NYMEX NASDAQ L2_SERV CME-LTD AMEX L2 ,,500,QT_API,,",
				"S,CURRENT PROTOCOL,5.1,",
				"S,CURRENT UPDATE FIELDNAMES,Symbol,Most Recent Trade,Most Recent Trade Size,Most Recent Trade TimeMS,Most Recent Trade Market Center,Total Volume,Bid,Bid Size,Ask,Ask Size,Open,High,Low,Close,Message Contents,Most Recent Trade Conditions",
				"S,CURRENT UPDATE FIELDNAMES,Symbol,Message Contents,Most Recent Trade Conditions,Exchange ID,Most Recent Trade Market Center,Most Recent Trade Date,Most Recent Trade TimeMS,Most Recent Trade,Most Recent Trade Size,TickID,Open Interest,VWAP,Bid Market Center,Bid TimeMS,Bid,Bid Size,Ask Market Center,Ask TimeMS,Ask,Ask Size"
		};
		for(String message: L1_SYSTEM_MESSAGES)
		{
			iqFeedUpdate.notifyStringMessage(Feed.L1, TimeZone.getDefault(), System.currentTimeMillis(), message);
			assertTrue(iqFeedUpdate.getFeedType() == Feed.L1);
			DataEvent[] out = L1Parser.getParsedMessage(iqFeedUpdate.getFeedType(), iqFeedUpdate.getTimeZone(), 
					iqFeedUpdate.getReceiveTime(), iqFeedUpdate.getMessage());
			if(message.equals("S,KEY,99999") &&
					out != null) fail();
			else if(message.equals("S,SERVER CONNECTED") && (
					out == null || ! (out[0] instanceof ConnectionEvent) ||
					((ConnectionEvent) out[0]).isConnected == false))	fail();
			else if(message.equals("S,SERVER DISCONNECTED") && (
					out == null || ! (out[0] instanceof ConnectionEvent) ||
					((ConnectionEvent) out[0]).isConnected == true))	fail();
			else if(message.equals("S,IP,66.112.156.181 60004,66.112.156.218 60002,66.112.156.213 60009,66.112.156.210 60001,66.112.156.211 60005,66.112.156.210 60012,66.112.156.200 60003,66.112.156.209 60015,66.112.156.216 60050,66.112.156.210 60014,66.112.156.211 60016,66.112.156.209 60018") &&
					out != null) fail();
			else if(message.equals("S,CUST,real_time,66.112.156.113,60003,h8A2F0i,5.1.0.3,0, NYSE NYMEX NASDAQ L2_SERV CME-LTD AMEX L2 ,,500,QT_API,,") &&
					out != null) fail();
			else if(message.equals("S,CURRENT PROTOCOL,5.1,") &&
					out != null)fail();
			else if(message.equals("S,CURRENT UPDATE FIELDNAMES,Symbol,Most Recent Trade,Most Recent Trade Size,Most Recent Trade TimeMS,Most Recent Trade Market Center,Total Volume,Bid,Bid Size,Ask,Ask Size,Open,High,Low,Close,Message Contents,Most Recent Trade Conditions") &&
					out != null) fail();
			else if(message.equals("S,CURRENT UPDATE FIELDNAMES,Symbol,Message Contents,Most Recent Trade Conditions,Exchange ID,Most Recent Trade Market Center,Most Recent Trade Date,Most Recent Trade TimeMS,Most Recent Trade,Most Recent Trade Size,TickID,Open Interest,VWAP,Bid Market Center,Bid TimeMS,Bid,Bid Size,Ask Market Center,Ask TimeMS,Ask,Ask Size") &&
					out != null) fail();

		}
		
	}
	
	@Test
	public void testL1TimeStampMessages() {
		String[] L1_TIMESTAMP_MESSAGES = {
				"T,20131216 09:02:56",
				"T,20131216 09:02:57"
		};
		for(String message: L1_TIMESTAMP_MESSAGES)
		{
			iqFeedUpdate.notifyStringMessage(Feed.L1, TimeZone.getDefault(), System.currentTimeMillis(), message);
			assertTrue(iqFeedUpdate.getFeedType() == Feed.L1);
			DataEvent[] out = L1Parser.getParsedMessage(iqFeedUpdate.getFeedType(), iqFeedUpdate.getTimeZone(), 
					iqFeedUpdate.getReceiveTime(), iqFeedUpdate.getMessage());
			assertTrue(out == null);
		}
		
	}
	@Test
	public void testL1SymbolNotFoundMessages() throws Exception {
		String[] L1_SYMBOL_NOT_FOUND_MESSAGES = {
				"n,AAPL",
				"n,@ES#"
		};
		for(String message: L1_SYMBOL_NOT_FOUND_MESSAGES)
		{
			iqFeedUpdate.notifyStringMessage(Feed.L1, TimeZone.getDefault(), System.currentTimeMillis(), message);
			assertTrue(iqFeedUpdate.getFeedType() == Feed.L1);
			String tokens[] = StringUtils.getCommaSplitString(message);
			DataEvent[] out = L1Parser.getParsedMessage(iqFeedUpdate.getFeedType(), iqFeedUpdate.getTimeZone(), 
					iqFeedUpdate.getReceiveTime(), iqFeedUpdate.getMessage());
			assertTrue(out != null && out.length == 1 && out[0] instanceof SymbolNotFoundEvent && ((SymbolNotFoundEvent) out[0]).symbol.equals(tokens[1]));

		}
	}
	
	@Test
	public void testL1ErrorMessage() {
		String[] L1_ERROR_MESSAGES = {
				"E,ERROR_MESSAGE1",
				"E,ERROR_MESSAGE2"
		};

		TimeZone timeZone = TimeZone.getDefault();
		long receiveTime = System.currentTimeMillis();
		
		for(String message: L1_ERROR_MESSAGES)
		{
			iqFeedUpdate.notifyStringMessage(Feed.L1, timeZone, receiveTime, message);
			assertTrue(iqFeedUpdate.getFeedType() == Feed.L1);
			String tokens[] = StringUtils.getCommaSplitString(message);
			DataEvent[] out = L1Parser.getParsedMessage(iqFeedUpdate.getFeedType(), iqFeedUpdate.getTimeZone(), 
					iqFeedUpdate.getReceiveTime(), iqFeedUpdate.getMessage());
			ErrorEvent errorMsg = new ErrorEvent(timeZone, receiveTime, tokens[1]);
			assertTrue(out != null && out.length == 1 && out[0] instanceof ErrorEvent);
			assertTrue(((ErrorEvent) out[0]).equals(errorMsg));

		}
	}
	
	@Test
	public void testL1FundamentalMessages() {
		String[] L1_FUNDAMENTAL_MESSAGES = {
				"F,AAPL,5,13.8,11453000,575.1358,385.1000,575.1360,385.1000,2.2000,3.0500,12.2000,11/14/2013,11/06/2013,,,,17038304,,40.03,,0.49,09,,APPLE,AAPL AAPL7,62.9,0.63,,73286.0,43658.0,09/30/2013,16960.0,899738,334220,0.50 02/28/2005,0.50 06/21/2000,,0,14,4,3571,18.43,1,21,12/05/2013,04/19/2013,12/05/2013,04/19/2013,532.17,,,,,334220,,",
				"F,@ES#,22,,,1805.75,1511.50,,,,,,,,,,,,,,,,,,E-MINI S&P 500 MARCH 2014,,,,,,,,,,, , ,,0,12,2,,10.49,8,43,11/29/2013,03/18/2013,,,,,,03/21/2014,,,ES,"
		};
		
		TimeZone timeZone = TimeZone.getDefault();
		long receiveTime = System.currentTimeMillis();
		for(String message: L1_FUNDAMENTAL_MESSAGES)
		{
			iqFeedUpdate.notifyStringMessage(Feed.L1, timeZone, receiveTime, message);
			assertTrue(iqFeedUpdate.getFeedType() == Feed.L1);
			DataEvent[] out = L1Parser.getParsedMessage(iqFeedUpdate.getFeedType(), iqFeedUpdate.getTimeZone(),
					iqFeedUpdate.getReceiveTime(), iqFeedUpdate.getMessage());
			assertTrue(out != null && out.length == 1 && out[0] instanceof FundamentalEvent);
			FundamentalEvent fundMsgTest = (FundamentalEvent) out[0];
			String tokens[] = StringUtils.getCommaSplitString(message);
			String symbol = tokens[1];
			String exchangeId = tokens[2];
			double PERatio = StringUtils.getDoubleFromString(tokens[3], Double.MIN_VALUE);
			int avgVolume = StringUtils.getIntFromString(tokens[4], Integer.MIN_VALUE);
			double price_52WkHigh = StringUtils.getDoubleFromString(tokens[5], Double.MIN_VALUE);
			double price_52WkLow = StringUtils.getDoubleFromString(tokens[6], Double.MIN_VALUE);
			double priceCalendarYearHigh = StringUtils.getDoubleFromString(tokens[7], Double.MIN_VALUE);
			double priceCalendarYearLow = StringUtils.getDoubleFromString(tokens[8], Double.MIN_VALUE);
			double dividendYield = StringUtils.getDoubleFromString(tokens[9], Double.MIN_VALUE);
			double dividendAmount = StringUtils.getDoubleFromString(tokens[10], Double.MIN_VALUE);
			double dividendRate = StringUtils.getDoubleFromString(tokens[11], Double.MIN_VALUE);
			String payDate = tokens[12];
			String exDividendDate = tokens[13];
			int shortInterest = StringUtils.getIntFromString(tokens[17], Integer.MIN_VALUE);
			double currentYearEPS = StringUtils.getDoubleFromString(tokens[19], Double.MIN_VALUE);
			double nextYearEPS = StringUtils.getDoubleFromString(tokens[20], Double.MIN_VALUE);
			double fiveYearGrowthPercentage = StringUtils.getDoubleFromString(tokens[21], Double.MIN_VALUE);
			int fiscalYearEnd = StringUtils.getIntFromString(tokens[22], Integer.MIN_VALUE);
			String companyName = tokens[24];
			String rootOptionSymbol = tokens[25];
			double percentHeldByInstitutions = StringUtils.getDoubleFromString(tokens[26], Double.MIN_VALUE);
			double beta = StringUtils.getDoubleFromString(tokens[27], Double.MIN_VALUE);
			String leaps = tokens[28];
			double currentAssets = StringUtils.getDoubleFromString(tokens[29], Double.MIN_VALUE);
			double currentLiabilities = StringUtils.getDoubleFromString(tokens[30], Double.MIN_VALUE);
			String balanceSheetDate = tokens[31];
			double longTermDebt = StringUtils.getDoubleFromString(tokens[32], Double.MIN_VALUE);
			double commonSharesOutstanding = StringUtils.getDoubleFromString(tokens[33], Double.MIN_VALUE);
			String splitFactor1 = tokens[35];
			String splitFactor2 = tokens[36];
			String formatCode = tokens[39];
			int precision = StringUtils.getIntFromString(tokens[40], Integer.MIN_VALUE);
			int SIC = StringUtils.getIntFromString(tokens[41], Integer.MIN_VALUE);
			double historicalVolatility = StringUtils.getDoubleFromString(tokens[42], Double.MIN_VALUE);
			String securityType = tokens[43];
			String listedMarket = tokens[44];
			String date52WkHigh = tokens[45];
			String date52WkLow = tokens[46];
			String calendarYearHighDate = tokens[47];
			String calendarYearLowDate = tokens[48];
			double yearEndClosePrice = StringUtils.getDoubleFromString(tokens[49], Double.MIN_VALUE);
			String maturityDate = tokens[50];
			double couponRate = StringUtils.getDoubleFromString(tokens[51], Double.MIN_VALUE);
			String expirationDate = tokens[52];
			double strikePrice = StringUtils.getDoubleFromString(tokens[53], Double.MIN_VALUE);
			int NAICS = StringUtils.getIntFromString(tokens[54], Integer.MIN_VALUE);
			String exchangeRoot = tokens[55];
			
			FundamentalEvent fundMsg = new FundamentalEvent(timeZone, receiveTime, symbol, exchangeId, PERatio, avgVolume, price_52WkHigh, price_52WkLow, 
					date52WkHigh, date52WkLow, priceCalendarYearHigh, priceCalendarYearLow, 
					calendarYearHighDate, calendarYearLowDate, dividendYield, dividendAmount, 
					dividendRate, payDate, exDividendDate, shortInterest, currentYearEPS, 
					nextYearEPS, fiveYearGrowthPercentage, fiscalYearEnd, companyName, 
					rootOptionSymbol, percentHeldByInstitutions, beta, leaps, currentAssets, 
					currentLiabilities, balanceSheetDate, longTermDebt, commonSharesOutstanding,
					splitFactor1, splitFactor2, formatCode, precision, SIC, historicalVolatility,
					securityType, listedMarket, yearEndClosePrice, maturityDate, couponRate,
					expirationDate, strikePrice, NAICS, exchangeRoot);
			assertTrue(fundMsg.equals(fundMsgTest));
			for(int i = 0; i < tokens.length; i++)
			{
				
				switch(i)
				{
				case 0: assertTrue(tokens[i].equals(L1Events.L1_FUNDAMENTAL.getFeedId())); break;
				case 1: assertTrue(tokens[i].equals(fundMsg.symbol)); break;
				case 2: assertTrue(tokens[i].equals(fundMsg.exchangeId)); break;
				case 3: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.PERatio); break;
				case 4: assertTrue(StringUtils.getIntFromString(tokens[i], Integer.MIN_VALUE) == fundMsg.avgVolume); break;
				case 5: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.price_52WkHigh); break;
				case 6: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.price_52WkLow); break;
				case 7: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.priceCalendarYearHigh); break;
				case 8: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.priceCalendarYearLow); break;
				case 9: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.dividendYield); break;
				case 10: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.dividendAmount); break;
				case 11: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.dividendRate); break;
				case 12: assertTrue(tokens[i].equals(fundMsg.payDate)); break;
				case 13: assertTrue(tokens[i].equals(fundMsg.exDividendDate)); break;
				case 17: assertTrue(StringUtils.getIntFromString(tokens[i], Integer.MIN_VALUE) == fundMsg.shortInterest); break;
				case 19: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.currentYearEPS); break;
				case 20: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.nextYearEPS); break;
				case 21: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.fiveYearGrowthPercentage); break;
				case 22: assertTrue(StringUtils.getIntFromString(tokens[i], Integer.MIN_VALUE) == fundMsg.fiscalYearEnd); break;
				case 24: assertTrue(tokens[i].equals(fundMsg.companyName)); break;
				case 25: assertTrue(tokens[i].equals(fundMsg.rootOptionSymbol)); break;
				case 26: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.percentHeldByInstitutions); break;
				case 27: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.beta); break;
				case 28: assertTrue(tokens[i].equals(fundMsg.leaps)); break;
				case 29: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.currentAssets); break;
				case 30: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.currentLiabilities); break;
				case 31: assertTrue(tokens[i].equals(fundMsg.balanceSheetDate)); break;
				case 32: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.longTermDebt); break;
				case 33: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.commonSharesOutstanding); break;
				case 35: assertTrue(tokens[i].equals(fundMsg.splitFactor1)); break;
				case 36: assertTrue(tokens[i].equals(fundMsg.splitFactor2)); break;
				case 39: assertTrue(tokens[i].equals(fundMsg.formatCode)); break;
				case 40: assertTrue(StringUtils.getIntFromString(tokens[i], Integer.MIN_VALUE) == fundMsg.precision); break;
				case 41: assertTrue(StringUtils.getIntFromString(tokens[i], Integer.MIN_VALUE) == fundMsg.SIC); break;
				case 42: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.historicalVolatility); break;
				case 43: assertTrue(tokens[i].equals(fundMsg.securityType)); break;
				case 44: assertTrue(tokens[i].equals(fundMsg.listedMarket)); break;
				case 45: assertTrue(tokens[i].equals(fundMsg.date52WkHigh)); break;
				case 46: assertTrue(tokens[i].equals(fundMsg.date52WkLow)); break;
				case 47: assertTrue(tokens[i].equals(fundMsg.calendarYearHighDate)); break;
				case 48: assertTrue(tokens[i].equals(fundMsg.calendarYearLowDate)); break;
				case 49: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.yearEndClosePrice); break;
				case 50: assertTrue(tokens[i].equals(fundMsg.maturityDate)); break;
				case 51: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.couponRate); break;
				case 52: assertTrue(tokens[i].equals(fundMsg.expirationDate)); break;
				case 53: assertTrue(StringUtils.getDoubleFromString(tokens[i], Double.MIN_VALUE) == fundMsg.strikePrice); break;
				case 54: assertTrue(StringUtils.getIntFromString(tokens[i], Integer.MIN_VALUE) == fundMsg.NAICS); break;
				case 55: assertTrue(tokens[i].equals(fundMsg.exchangeRoot)); break;
				}
			}

		}
		
	}
	@Test
	public void testL1SummaryMessages() {
		String[] L1_SUMMARY_MESSAGES = {
				"P,AAPL,E,17,5,26,12/16/2013,09:02:56.764,557.2100,100,8511,,555.8517,5,09:02:47.787,557.1100,500,26,09:02:47.787,557.3800,500,",
				"P,@ES#,b,01,22,43,12/16/2013,09:03:11.023,1778.00,1,1704475,1446253,,43,09:03:11.023,1777.75,232,43,09:03:11.023,1778.00,185,",
				"P,@ES#,a,01,22,43,12/16/2013,09:03:11.023,1778.00,1,1704475,1446253,,43,09:03:11.023,1777.75,232,43,09:03:11.023,1778.00,185,"
		};
		
		TimeZone timeZone = TimeZone.getDefault();
		long receiveTime = System.currentTimeMillis();
		for(String message: L1_SUMMARY_MESSAGES)
		{
			iqFeedUpdate.notifyStringMessage(Feed.L1, timeZone, receiveTime, message);
			assertTrue(iqFeedUpdate.getFeedType() == Feed.L1);
			DataEvent[] out = L1Parser.getParsedMessage(iqFeedUpdate.getFeedType(), iqFeedUpdate.getTimeZone(),
					iqFeedUpdate.getReceiveTime(), iqFeedUpdate.getMessage());
			int count = 0;

			String tokens[] = StringUtils.getCommaSplitString(message);

			if(tokens[2].contains("E") || tokens[2].contains("C") || tokens[2].contains("O"))
			{
				assertTrue(out != null && out[count] instanceof TradeEvent);
				TradeEvent last = (TradeEvent) out[count++];
				String tradeType = (tokens[2].contains("C")) ? "C" : (tokens[2].contains("E")) ? "E" : (tokens[2].contains("O")) ? "O" : ""; 
				TradeEvent lastTest;
				try {
					lastTest = new TradeEvent(timeZone, receiveTime, tokens[1], tradeType, tokens[3], tokens[4], tokens[5], tokens[6], tokens[7], 
							StringUtils.getDoubleFromString(tokens[8], Double.MIN_VALUE), StringUtils.getIntFromString(tokens[9],Integer.MIN_VALUE), 
							StringUtils.getIntFromString(tokens[10],Integer.MIN_VALUE), StringUtils.getIntFromString(tokens[11],Integer.MIN_VALUE), 
							StringUtils.getDoubleFromString(tokens[12], Double.MIN_VALUE));
				} catch (ParseException e) {
					lastTest = null;
					e.printStackTrace();
				}
				assertTrue(last.equals(lastTest));
			}
			if(tokens[2].contains("b"))
			{
				assertTrue(out != null && out[count] instanceof TOBEvent);
				TOBEvent bid = (TOBEvent) out[count++];
				TOBEvent bidTest;
				try {
					bidTest = new TOBEvent(timeZone, receiveTime, tokens[1], EventType.BID, tokens[4], tokens[13], tokens[14],
							StringUtils.getDoubleFromString(tokens[15], Double.MIN_VALUE), StringUtils.getIntFromString(tokens[16],Integer.MIN_VALUE));
				} catch (ParseException e) {
					bidTest = null;
					e.printStackTrace();
				}
				assertTrue(bid.equals(bidTest));
			}
			if(tokens[2].contains("a"))
			{
				assertTrue(out != null && out[count] instanceof TOBEvent);
				TOBEvent ask = (TOBEvent) out[count++];
				TOBEvent askTest;
				try {
					askTest = new TOBEvent(timeZone, receiveTime, tokens[1], EventType.ASK, tokens[4], tokens[17], tokens[18], 
							StringUtils.getDoubleFromString(tokens[19], Double.MIN_VALUE), StringUtils.getIntFromString(tokens[20],Integer.MIN_VALUE));
				} catch (ParseException e) {
					askTest = null;
					e.printStackTrace();
				}
				assertTrue(ask.equals(askTest));
			}
		}
	}
	
	@Test
	public void testL1UpdateMessages() {
		String[] L1_UPDATE_MESSAGES = {
				"Q,AAPL,ba,17,5,26,12/16/2013,09:02:56.764,557.2100,100,8511,,555.8517,26,09:03:05.963,557.2500,300,26,09:03:05.963,557.3800,500,",
				"Q,AAPL,E,17,5,19,12/16/2013,09:03:07.075,557.3800,250,8522,,555.8553,26,09:03:05.963,557.2500,300,26,09:03:05.963,557.3800,500,",
				"Q,AAPL,b,17,5,19,12/16/2013,09:03:07.075,557.3800,250,8522,,555.8553,5,09:03:10.099,557.3000,100,26,09:03:10.099,557.3800,500,",
				"Q,@ES#,a,01,22,43,12/16/2013,09:03:11.023,1778.00,1,1704475,1446253,,43,09:03:11.023,1777.75,232,43,09:03:11.366,1778.00,186,",
				"Q,AAPL,O,173D,5,5,12/16/2013,09:03:11.668,557.3000,100,8528,,555.8567,5,09:03:10.099,557.3000,100,26,09:03:10.099,557.3800,500,",
				"Q,AAPL,C,173D,5,5,12/16/2013,09:03:11.668,557.3000,100,8529,,555.8581,5,09:03:10.099,557.3000,100,26,09:03:10.099,557.3800,500,",
				"Q,AAPL,Cba,173D,5,5,12/16/2013,09:03:11.668,557.3000,100,8529,,555.8581,26,09:03:11.669,557.2500,300,26,09:03:11.669,557.3800,500,"
		};
		TimeZone timeZone = TimeZone.getDefault();
		long receiveTime = System.currentTimeMillis();
		for(String message: L1_UPDATE_MESSAGES)
		{
			iqFeedUpdate.notifyStringMessage(Feed.L1, timeZone, receiveTime, message);
			assertTrue(iqFeedUpdate.getFeedType() == Feed.L1);
			DataEvent[] out = L1Parser.getParsedMessage(iqFeedUpdate.getFeedType(), iqFeedUpdate.getTimeZone(),
					iqFeedUpdate.getReceiveTime(), iqFeedUpdate.getMessage());
			int count = 0;

			String tokens[] = StringUtils.getCommaSplitString(message);

			if(tokens[2].contains("E") || tokens[2].contains("C") || tokens[2].contains("O"))
			{
				assertTrue(out != null && out[count] instanceof TradeEvent);
				TradeEvent last = (TradeEvent) out[count++];
				String tradeType = (tokens[2].contains("C")) ? "C" : (tokens[2].contains("E")) ? "E" : (tokens[2].contains("O")) ? "O" : ""; 
				TradeEvent lastTest;
				try {
					lastTest = new TradeEvent(timeZone, receiveTime, tokens[1], tradeType, tokens[3], tokens[4], tokens[5], tokens[6], tokens[7], 
							StringUtils.getDoubleFromString(tokens[8], Double.MIN_VALUE), StringUtils.getIntFromString(tokens[9],Integer.MIN_VALUE), 
							StringUtils.getIntFromString(tokens[10],Integer.MIN_VALUE), StringUtils.getIntFromString(tokens[11],Integer.MIN_VALUE), 
							StringUtils.getDoubleFromString(tokens[12], Double.MIN_VALUE));
				} catch (ParseException e) {
					lastTest = null;
					e.printStackTrace();
				}
				assertTrue(last.equals(lastTest));
			}
			if(tokens[2].contains("b"))
			{
				assertTrue(out != null && out[count] instanceof TOBEvent);
				TOBEvent bid = (TOBEvent) out[count++];
				TOBEvent bidTest;
				try {
					bidTest = new TOBEvent(timeZone, receiveTime, tokens[1], EventType.BID, tokens[4], tokens[13], tokens[14],
							StringUtils.getDoubleFromString(tokens[15], Double.MIN_VALUE), StringUtils.getIntFromString(tokens[16],Integer.MIN_VALUE));
				} catch (ParseException e) {
					bidTest = null;
					e.printStackTrace();
				}
				assertTrue(bid.equals(bidTest));
			}
			if(tokens[2].contains("a"))
			{
				assertTrue(out != null && out[count] instanceof TOBEvent);
				TOBEvent ask = (TOBEvent) out[count++];
				TOBEvent askTest;
				try {
					askTest = new TOBEvent(timeZone, receiveTime, tokens[1], EventType.ASK, tokens[4], tokens[17], tokens[18], 
							StringUtils.getDoubleFromString(tokens[19], Double.MIN_VALUE), StringUtils.getIntFromString(tokens[20],Integer.MIN_VALUE));
				} catch (ParseException e) {
					askTest = null;
					e.printStackTrace();
				}
				assertTrue(ask.equals(askTest));
			}
		}
		
	}
	
	@Test
	public void testL1RegionalMessages() {
		String regionalMessage = "R,AAPL,,557.2100,100,09:02:56.764,555.8517,26,09:03:05.963,6,4,40";
		TimeZone timeZone = TimeZone.getDefault();
		long receiveTime = System.currentTimeMillis();
		
		iqFeedUpdate.notifyStringMessage(Feed.L1, timeZone, receiveTime, regionalMessage);
		assertTrue(iqFeedUpdate.getFeedType() == Feed.L1);
		DataEvent[] out = L1Parser.getParsedMessage(iqFeedUpdate.getFeedType(), iqFeedUpdate.getTimeZone(),
				iqFeedUpdate.getReceiveTime(), iqFeedUpdate.getMessage());
		int count = 0;
		
		String tokens[] = StringUtils.getCommaSplitString(regionalMessage);
		if(tokens.length == 12)
		{
			assertTrue(out != null && out[count] instanceof TOBEvent);
			TOBEvent bid = (TOBEvent) out[count++];
			TOBEvent bidTest;
			try {
				bidTest = new TOBEvent(timeZone, receiveTime, tokens[1], EventType.REGIONAL_BID, tokens[2], tokens[11], tokens[5],
						StringUtils.getDoubleFromString(tokens[3], Double.MIN_VALUE),
						StringUtils.getIntFromString(tokens[4], Integer.MIN_VALUE));
			} catch (ParseException e) {
				bidTest = null;
				e.printStackTrace();
			}
			assertTrue(bid.equals(bidTest));
			
			assertTrue(out != null && out[count] instanceof TOBEvent);
			TOBEvent ask = (TOBEvent) out[count++];
			TOBEvent askTest;
			try {
				askTest = new TOBEvent(timeZone, receiveTime, tokens[1], EventType.REGIONAL_ASK, tokens[2], tokens[11], tokens[8],
						StringUtils.getDoubleFromString(tokens[6], Double.MIN_VALUE),
						StringUtils.getIntFromString(tokens[7], Integer.MIN_VALUE));
			} catch (ParseException e) {
				askTest = null;
				e.printStackTrace();
			}
			assertTrue(ask.equals(askTest));
		} else fail();
		
	}
}
