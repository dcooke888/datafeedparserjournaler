package test.JDatabase.IQFeed.SerializationTests;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.TimeZone;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.JDatabase.Events.ConnectionEvent;
import com.JDatabase.Events.DataEvent;
import com.JDatabase.Events.DepthEvent;
import com.JDatabase.Events.ErrorEvent;
import com.JDatabase.Events.EventType;
import com.JDatabase.Events.FundamentalEvent;
import com.JDatabase.Events.TradeEvent;
import com.JDatabase.Events.SymbolNotFoundEvent;
import com.JDatabase.Events.TOBEvent;
import com.JDatabase.IQFeed.Feed;
import com.JDatabase.IQFeed.IQFeedUpdateEvent;
import com.JDatabase.IQFeed.L1Events.L1Parser;
import com.JDatabase.IQFeed.L2Events.L2Parser;
import com.JDatabase.Serialize.DirectBufferSerializer;
import com.JDatabase.Serialize.Serializer;
import com.JDatabase.Utils.StringUtils;

public class UnsafeSerializationEqualityTests {
	static IQFeedUpdateEvent iqFeedUpdate;
	Serializer serializer;

	@Before
	public void setUp() throws Exception {
		iqFeedUpdate = IQFeedUpdateEvent.EVENT_FACTORY.newInstance();
		serializer = new DirectBufferSerializer(1024 * 1000);
	}

	@After
	public void tearDown() throws Exception {
		iqFeedUpdate = null;
		serializer = null;
	}
	@Test
	public void testConnectionEvent() {
		String[] L1_SYSTEM_MESSAGES = {
				"S,SERVER CONNECTED",
				"S,SERVER DISCONNECTED",
		};
		for(String message: L1_SYSTEM_MESSAGES)
		{
			iqFeedUpdate.notifyStringMessage(Feed.L1, TimeZone.getDefault(), System.currentTimeMillis(), message);
			assertTrue(iqFeedUpdate.getFeedType() == Feed.L1);
			DataEvent[] out = L1Parser.getParsedMessage(iqFeedUpdate.getFeedType(), iqFeedUpdate.getTimeZone(), 
					iqFeedUpdate.getReceiveTime(), iqFeedUpdate.getMessage());
			ConnectionEvent event = (ConnectionEvent) out[0];
			event.write(serializer);
			serializer.flip();
			EventType eventType = EventType.fromByteId(serializer.getByte());
			ConnectionEvent testEvent = event.read(serializer, eventType);
			assertTrue(event.equals(testEvent));
			assertFalse(serializer.hasRemaining());
			serializer.clear();
		}
	}
	
	@Test
	public void testErrorMessageEvent() {
		String[] L1_ERROR_MESSAGES = {
				"E,ERROR_MESSAGE1",
				"E,ERROR_MESSAGE2"
		};

		TimeZone timeZone = TimeZone.getDefault();
		long receiveTime = System.currentTimeMillis();
		
		for(String message: L1_ERROR_MESSAGES)
		{
			iqFeedUpdate.notifyStringMessage(Feed.L1, timeZone, receiveTime, message);
			assertTrue(iqFeedUpdate.getFeedType() == Feed.L1);
			DataEvent[] out = L1Parser.getParsedMessage(iqFeedUpdate.getFeedType(), iqFeedUpdate.getTimeZone(), 
					iqFeedUpdate.getReceiveTime(), iqFeedUpdate.getMessage());
			ErrorEvent event = (ErrorEvent) out[0];
			event.write(serializer);
			serializer.flip();
			EventType eventType = EventType.fromByteId(serializer.getByte());
			ErrorEvent testEvent = event.read(serializer, eventType);
			assertTrue(event.equals(testEvent));
			assertFalse(serializer.hasRemaining());
			serializer.clear();

		}
	}
	@Test
	public void testFundamentalMessageEvent() {
		String[] L1_FUNDAMENTAL_MESSAGES = {
				"F,AAPL,5,13.8,11453000,575.1358,385.1000,575.1360,385.1000,2.2000,3.0500,12.2000,11/14/2013,11/06/2013,,,,17038304,,40.03,,0.49,09,,APPLE,AAPL AAPL7,62.9,0.63,,73286.0,43658.0,09/30/2013,16960.0,899738,334220,0.50 02/28/2005,0.50 06/21/2000,,0,14,4,3571,18.43,1,21,12/05/2013,04/19/2013,12/05/2013,04/19/2013,532.17,,,,,334220,,",
				"F,@ES#,22,,,1805.75,1511.50,,,,,,,,,,,,,,,,,,E-MINI S&P 500 MARCH 2014,,,,,,,,,,, , ,,0,12,2,,10.49,8,43,11/29/2013,03/18/2013,,,,,,03/21/2014,,,ES,"
		};
		
		TimeZone timeZone = TimeZone.getDefault();
		long receiveTime = System.currentTimeMillis();
		for(String message: L1_FUNDAMENTAL_MESSAGES)
		{
			iqFeedUpdate.notifyStringMessage(Feed.L1, timeZone, receiveTime, message);
			assertTrue(iqFeedUpdate.getFeedType() == Feed.L1);
			DataEvent[] out = L1Parser.getParsedMessage(iqFeedUpdate.getFeedType(), iqFeedUpdate.getTimeZone(),
					iqFeedUpdate.getReceiveTime(), iqFeedUpdate.getMessage());
			assertTrue(out != null && out.length == 1 && out[0] instanceof FundamentalEvent);
			FundamentalEvent event = (FundamentalEvent) out[0];
			event.write(serializer);
			serializer.flip();
			EventType eventType = EventType.fromByteId(serializer.getByte());
			FundamentalEvent testEvent = event.read(serializer, eventType);
			assertTrue(event.equals(testEvent));
			assertFalse(serializer.hasRemaining());
			serializer.clear();
		}
	}
	
	@Test
	public void testSymbolNotFoundEvent()	{
		String[] L1_SYMBOL_NOT_FOUND_MESSAGES = {
				"n,AAPL",
				"n,@ES#"
		};
		for(String message: L1_SYMBOL_NOT_FOUND_MESSAGES)
		{
			iqFeedUpdate.notifyStringMessage(Feed.L1, TimeZone.getDefault(), System.currentTimeMillis(), message);
			DataEvent[] out = L1Parser.getParsedMessage(iqFeedUpdate.getFeedType(), iqFeedUpdate.getTimeZone(), 
					iqFeedUpdate.getReceiveTime(), iqFeedUpdate.getMessage());
			SymbolNotFoundEvent event = (SymbolNotFoundEvent) out[0];
			event.write(serializer);
			serializer.flip();
			EventType eventType = EventType.fromByteId(serializer.getByte());
			SymbolNotFoundEvent testEvent = event.read(serializer, eventType);
			assertTrue(event.equals(testEvent));
			assertFalse(serializer.hasRemaining());
			serializer.clear();
		}
	}

	@Test
	public void testTOBEventAndLastEvent() throws ParseException {
		String[] L1_UPDATE_MESSAGES = {
				"Q,AAPL,ba,17,5,26,12/16/2013,09:02:56.764,557.2100,100,8511,,555.8517,26,09:03:05.963,557.2500,300,26,09:03:05.963,557.3800,500,",
				"Q,AAPL,E,17,5,19,12/16/2013,09:03:07.075,557.3800,250,8522,,555.8553,26,09:03:05.963,557.2500,300,26,09:03:05.963,557.3800,500,",
				"Q,AAPL,b,17,5,19,12/16/2013,09:03:07.075,557.3800,250,8522,,555.8553,5,09:03:10.099,557.3000,100,26,09:03:10.099,557.3800,500,",
				"Q,@ES#,a,01,22,43,12/16/2013,09:03:11.023,1778.00,1,1704475,1446253,,43,09:03:11.023,1777.75,232,43,09:03:11.366,1778.00,186,",
				"Q,AAPL,O,173D,5,5,12/16/2013,09:03:11.668,557.3000,100,8528,,555.8567,5,09:03:10.099,557.3000,100,26,09:03:10.099,557.3800,500,",
				"Q,AAPL,C,173D,5,5,12/16/2013,09:03:11.668,557.3000,100,8529,,555.8581,5,09:03:10.099,557.3000,100,26,09:03:10.099,557.3800,500,",
				"Q,AAPL,Cba,173D,5,5,12/16/2013,09:03:11.668,557.3000,100,8529,,555.8581,26,09:03:11.669,557.2500,300,26,09:03:11.669,557.3800,500,"
		};
		TimeZone timeZone = TimeZone.getDefault();
		long receiveTime = System.currentTimeMillis();
		for(String message: L1_UPDATE_MESSAGES)
		{
			String tokens[] = StringUtils.getCommaSplitString(message);
			TradeEvent last = null;
			TOBEvent bid = null;
			TOBEvent ask = null;
			if(tokens[2].contains("E") || tokens[2].contains("C") || tokens[2].contains("O"))
			{
				String tradeType = (tokens[2].contains("C")) ? "C" : (tokens[2].contains("E")) ? "E" : (tokens[2].contains("O")) ? "O" : ""; 
				last = new TradeEvent(timeZone, receiveTime, tokens[1], tradeType, tokens[3], tokens[4], tokens[5], tokens[6], tokens[7], 
						StringUtils.getDoubleFromString(tokens[8], Double.MIN_VALUE), StringUtils.getIntFromString(tokens[9],Integer.MIN_VALUE), 
						StringUtils.getIntFromString(tokens[10],Integer.MIN_VALUE), StringUtils.getIntFromString(tokens[11],Integer.MIN_VALUE), 
						StringUtils.getDoubleFromString(tokens[12], Double.MIN_VALUE));
				last.write(serializer);
			}
			if(tokens[2].contains("b"))
			{
				bid = new TOBEvent(timeZone, receiveTime, tokens[1], EventType.BID, tokens[4], tokens[13], tokens[14],
						StringUtils.getDoubleFromString(tokens[15], Double.MIN_VALUE), StringUtils.getIntFromString(tokens[16],Integer.MIN_VALUE));
				bid.write(serializer);
			}
			if(tokens[2].contains("a"))
			{
				ask = new TOBEvent(timeZone, receiveTime, tokens[1], EventType.ASK, tokens[4], tokens[17], tokens[18], 
						StringUtils.getDoubleFromString(tokens[19], Double.MIN_VALUE), StringUtils.getIntFromString(tokens[20],Integer.MIN_VALUE));
				ask.write(serializer);
			}
			serializer.flip();
			if(last != null)
			{
				EventType event = EventType.fromByteId(serializer.getByte());
				assertTrue(last.equals(last.read(serializer, event)));
			}
			if(bid != null)
			{
				EventType event = EventType.fromByteId(serializer.getByte());
				TOBEvent bidTest = bid.read(serializer, event);
				assertTrue(bid.equals(bidTest));
			}
			if(ask != null)
			{
				EventType event = EventType.fromByteId(serializer.getByte());
				assertTrue(ask.equals(ask.read(serializer, event)));
			}
			assertFalse(serializer.hasRemaining());
			serializer.clear();
		}

		
	}
	@Test
	public void testDepthEvent() {
		String[] L2_SUMMARY_MESSAGES = {
				"Z,@ES#,MD04,1777.,1778.75,561,599,09:03:07.827,2013-12-16,52,09:03:09.344,T,T,T,",
				"Z,@ES#,MD05,1776.75,1779.,905,677,09:03:11.085,2013-12-16,52,09:03:07.867,T,T,T,",
				"Z,@ES#,MD06,1776.5,1779.25,554,592,09:03:10.266,2013-12-16,52,09:03:10.334,T,T,T,",
				"Z,@ES#,MD10,1775.5,1780.25,844,719,09:03:08.430,2013-12-16,52,09:03:08.430,T,T,T,",
				"Z,@ES#,MD07,1776.25,1779.5,727,757,09:03:08.010,2013-12-16,52,09:03:08.010,T,T,T,",
				"Z,@ES#,MD01,1777.75,1778.,232,185,09:03:11.023,2013-12-16,52,09:03:11.023,T,T,T,",
				"Z,@ES#,MD02,1777.5,1778.25,832,974,09:03:08.429,2013-12-16,52,09:03:07.734,T,T,T,",
				"Z,@ES#,MD03,1777.25,1778.5,731,871,09:03:08.429,2013-12-16,52,09:03:07.810,T,T,T,",
				"Z,@ES#,MD08,1776.,1779.75,773,703,09:03:08.728,2013-12-16,52,09:03:08.100,T,T,T,",
				"Z,@ES#,MD09,1775.75,1780.,573,995,09:03:08.191,2013-12-16,52,09:03:08.191,T,T,T,"
		};
		
		TimeZone timeZone = TimeZone.getDefault();
		long receiveTime = System.currentTimeMillis();
		for(String message: L2_SUMMARY_MESSAGES)
		{
			iqFeedUpdate.notifyStringMessage(Feed.L2, timeZone, receiveTime, message);
			assertTrue(iqFeedUpdate.getFeedType() == Feed.L2);
			DataEvent[] out = L2Parser.getParsedMessage(iqFeedUpdate.getFeedType(), iqFeedUpdate.getTimeZone(),
					iqFeedUpdate.getReceiveTime(), iqFeedUpdate.getMessage());
			int count = 0;
			DepthEvent bid = null;
			DepthEvent ask = null;
			
			String tokens[] = StringUtils.getCommaSplitString(message);

			if(tokens[11].equals("T"))
			{
				assertTrue(out != null && out[count] instanceof DepthEvent);
				bid = (DepthEvent) out[count++];
				bid.write(serializer);

			}
			if(tokens[12].equals("T"))
			{
				assertTrue(out != null && out[count] instanceof DepthEvent);
				ask = (DepthEvent) out[count++];
				ask.write(serializer);
			}
			serializer.flip();
			if(bid != null) 
			{
				EventType eventType = EventType.fromByteId(serializer.getByte());
				DepthEvent testEvent = bid.read(serializer, eventType);
				assertTrue(bid.equals(testEvent));
			}
			if(ask != null)
			{
				EventType eventType = EventType.fromByteId(serializer.getByte());
				DepthEvent testEvent = ask.read(serializer, eventType);
				assertTrue(ask.equals(testEvent));
			}

			assertFalse(serializer.hasRemaining());
			serializer.clear();
		}
		
	}

}
