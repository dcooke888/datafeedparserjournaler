package test.JDatabase.DB;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import org.junit.*;

import com.JDatabase.DB.TicDBMS;
import com.JDatabase.DB.Index.DateStreamTypeSymbolIndex;
import com.JDatabase.DataSource.TicDataFeed;
import com.JDatabase.DataSource.TicFeedListener;
import com.JDatabase.Events.DataEvent;
import com.JDatabase.Events.DepthEvent;
import com.JDatabase.Events.TOBEvent;
import com.JDatabase.Events.TicType;
import com.JDatabase.Events.TradeEvent;
import com.JDatabase.IQFeed.Feed;
import com.JDatabase.IQFeed.L1Events.L1Parser;
import com.JDatabase.IQFeed.L2Events.L2Parser;
import com.JDatabase.Utils.FileUtilities;

public class TicDB_DateStreamTypeSymbolIndexTests {
	public static final String DEFAULT_TEST_DB_FOLDER = System.getProperty("user.dir") + "/testDB2/";
	public static final String[] symbols = { "@ES#", "AAPL", "GOOG", "+CL"};
	public static int NUM_EVENTS_PER_TYPE_SYMBOL = 2;
	public static TimeZone timeZone = TimeZone.getDefault();
	public static String L1Msg = "Q,AAPL,Cba,17,5,26,12/16/2013,09:02:56.764,557.2100,100,8511,,555.8517,26,09:03:05.963,557.2500,300,26,09:03:05.963,557.3800,500,";
	public static String L2Msg = "2,@ES#,MD05,1776.75,1779.,908,677,09:03:12.236,2013-12-16,52,09:03:07.867,T,T,T,";
	public static int routineCount = 0;
	
	private static TradeEvent tradeEvent;
	private static TOBEvent tobEvent;
	private static DepthEvent depthEvent;
	private static TicDataFeed[] dataFeeds = {new TicDataFeed()};
	private static volatile boolean streamComplete = false;
	
	static {
		DataEvent[] events = L1Parser.getParsedMessage(Feed.L1, timeZone, 0, L1Msg);
		for(DataEvent event: events) {
			if(event instanceof TradeEvent) tradeEvent = (TradeEvent) event;
			else if(event instanceof TOBEvent && tobEvent == null) tobEvent = (TOBEvent) event;
		}
		events = L2Parser.getParsedMessage(Feed.L2, timeZone, 0, L2Msg);
		for(DataEvent event: events) {
			if(event instanceof DepthEvent && depthEvent == null) depthEvent = (DepthEvent) event;
		}
	}
	
	private TicDBMS ticDB;
	private List<DataEvent> validEvents = new ArrayList<DataEvent>();
	private List<DataEvent> receivedEvents = new ArrayList<DataEvent>();
	
	@BeforeClass
	public static void onlyOnce() {
		FileUtilities.delete(DEFAULT_TEST_DB_FOLDER);
	}
	
	@Before
	public void setUp() {
		setupDatabase();
		receivedEvents.clear();
	}

	private void setupDatabase() {
		// Create DB first
		ticDB = new TicDBMS(new DateStreamTypeSymbolIndex(DEFAULT_TEST_DB_FOLDER));
			
		// Create multiple days of events and add to events to put in database;
		createEvents(0, NUM_EVENTS_PER_TYPE_SYMBOL, true, true, true);
		createEvents(24 * 60 * 60 * 1000 + 10, NUM_EVENTS_PER_TYPE_SYMBOL, true, true, true);
		createEvents((24 * 60 * 60 * 1000) * 2 + 10, NUM_EVENTS_PER_TYPE_SYMBOL, true, true, true);
		// Write events to DB
		for(DataEvent event: validEvents) 
			ticDB.write(event);
		
		// Close DB to allow reading
		ticDB.closeDBForWriting();
		
		dataFeeds[0].registerTicFeedListener(new TestTicListener());
	}

	private void createEvents(long startTime, int numEventsPerTypeSymbol, boolean trade, boolean tob, boolean depth) {
		for(int j = 0; j < numEventsPerTypeSymbol; j++) {
			for(int i = 0; i < symbols.length; i++) {
				if(trade) validEvents.add(getTradeEvent(startTime + j, symbols[i]));
				if(tob) validEvents.add(getTOBEvent(startTime + j, symbols[i]));
				if(depth) validEvents.add(getDepthEvent(startTime + j, symbols[i]));
			}
		}
	}

	private static TradeEvent getTradeEvent(long time, String symbol) {
		return new TradeEvent(tradeEvent.timeZone, time, symbol, tradeEvent.tradeType, tradeEvent.tradeConditions, tradeEvent.exchangeId, 
				tradeEvent.lastMktCenter, tradeEvent.feedTime, tradeEvent.lastPrice, tradeEvent.lastSize,
				tradeEvent.tickId, tradeEvent.openInterest, tradeEvent.vwap);
	}
	
	
	private static DataEvent getTOBEvent(long time, String symbol) {
		return  new TOBEvent(tobEvent.timeZone, time, symbol, tobEvent.eventType, 
				tobEvent.exchange, tobEvent.mktCenter, tobEvent.feedTime, tobEvent.price, tobEvent.size);
	}
	
	private static DepthEvent getDepthEvent(long time, String symbol) {
		return new DepthEvent(depthEvent.timeZone, time, symbol, depthEvent.eventType, 
				depthEvent.mmid, depthEvent.feedTime, depthEvent.price, depthEvent.size);
	}
	
	
	@Test
	public void shouldFireEventsInOrder() {
		streamComplete = false;
		ticDB.streamTradeTOBDepth(dataFeeds, 0, Long.MAX_VALUE, symbols);
		waitUntilStreamIsDone();
		DataEvent lastEvent = null;
		for(DataEvent event: receivedEvents) {
			if(lastEvent != null) {
				if(lastEvent.receiveTime > event.receiveTime) fail("Data is not in order");
			}
			lastEvent = event;
		}
	}
	
	@Test
	public void shouldEliminateDuplicateEvents() {
		streamComplete = false;
		// Write events to DB
		for(DataEvent event: validEvents) 
			ticDB.write(event);
		
		// Close DB to allow reading
		ticDB.closeDBForWriting();
		
		
		ticDB.streamTradeTOBDepth(dataFeeds, 0, Long.MAX_VALUE, symbols);
		waitUntilStreamIsDone();
		for(int i = 0; i < receivedEvents.size() - 1; i++) {
			int eventCount = 0;
			DataEvent event = receivedEvents.get(i);
			for(DataEvent testEvent: receivedEvents) {
				if(event.equals(testEvent)) 
					eventCount++;
			}
			if(eventCount > 1) fail("Duplicate events");
		}
	}
	
	@Test
	public void shouldFireTradeEvents() {
		streamComplete = false;
		ticDB.streamTrade(dataFeeds, 0, Long.MAX_VALUE, symbols);
		waitUntilStreamIsDone();
		for(DataEvent event: validEvents) {
			TicType ticType = TicType.getTicType(event.eventType);
			if(ticType == TicType.TRADE && !wasEventReceived(event))
				fail("TRADE shouldn't be filtered");
			else if(ticType == TicType.TOB && wasEventReceived(event))
				fail("TOB should be filtered");
			else if(ticType == TicType.DEPTH && wasEventReceived(event))
				fail("Depth should be filtered");
		}
	}
	
	@Test
	public void shouldFireTOBEvents() {
		streamComplete = false;
		ticDB.streamTOB(dataFeeds, 0, Long.MAX_VALUE, symbols);
		waitUntilStreamIsDone();
		for(DataEvent event: validEvents) {
			TicType ticType = TicType.getTicType(event.eventType);
			if(ticType == TicType.TRADE && wasEventReceived(event))
				fail("TRADE should be filtered");
			else if(ticType == TicType.TOB && !wasEventReceived(event))
				fail("TOB shouldn't be filtered");
			else if(ticType == TicType.DEPTH && wasEventReceived(event))
				fail("Depth should be filtered");
		}
	}
	
	@Test
	public void shouldFireDepthEvents() {
		streamComplete = false;
		ticDB.streamDepth(dataFeeds, 0, Long.MAX_VALUE, symbols);
		waitUntilStreamIsDone();
		for(DataEvent event: validEvents) {
			TicType ticType = TicType.getTicType(event.eventType);
			if(ticType == TicType.TRADE && wasEventReceived(event))
				fail("TRADE should be filtered");
			else if(ticType == TicType.TOB && wasEventReceived(event))
				fail("TOB should be filtered");
			else if(ticType == TicType.DEPTH && !wasEventReceived(event))
				fail("Depth shouldn't be filtered");
		}
	}
	
	@Test
	public void shouldFireTradeTOBEvents() {
		streamComplete = false;
		ticDB.streamTradeTOB(dataFeeds, 0, Long.MAX_VALUE, symbols);
		waitUntilStreamIsDone();
		for(DataEvent event: validEvents) {
			TicType ticType = TicType.getTicType(event.eventType);
			if(ticType == TicType.TRADE && !wasEventReceived(event))
				fail("TRADE shouldn't be filtered");
			else if(ticType == TicType.TOB && !wasEventReceived(event))
				fail("TOB shouldn't be filtered");
			else if(ticType == TicType.DEPTH && wasEventReceived(event))
				fail("Depth should be filtered");
		}
	}
	
	@Test
	public void shouldFireTOBDepthEvents() {
		streamComplete = false;
		ticDB.streamTOBDepth(dataFeeds, 0, Long.MAX_VALUE, symbols);
		waitUntilStreamIsDone();
		for(DataEvent event: validEvents) {
			TicType ticType = TicType.getTicType(event.eventType);
			if(ticType == TicType.TRADE && wasEventReceived(event))
				fail("TRADE should be filtered");
			else if(ticType == TicType.TOB && !wasEventReceived(event))
				fail("TOB shouldn't be filtered");
			else if(ticType == TicType.DEPTH && !wasEventReceived(event))
				fail("Depth shouldn't be filtered");
		}
	}
	
	@Test
	public void shouldFireTradeTOBDepthEvents() {
		streamComplete = false;
		ticDB.streamTradeTOBDepth(dataFeeds, 0, Long.MAX_VALUE, symbols);
		waitUntilStreamIsDone();
		for(DataEvent event: validEvents) {
			if(!wasEventReceived(event)) fail("Should have received event");
		}
	}
	
	private void waitUntilStreamIsDone() {
		while(!streamComplete) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private boolean wasEventReceived(DataEvent toCheck) {
		for(DataEvent event: receivedEvents) {
			if(toCheck.equals(event)) return true;
		}
		return false;
	}
	
	class TestTicListener extends TicFeedListener {

		public TestTicListener() {
			super(true, true, true);
		}

		@Override
		public void notify(TradeEvent event) {
			receivedEvents.add(event);
		}

		@Override
		public void notify(TOBEvent event) {
			receivedEvents.add(event);
		}

		@Override
		public void notify(DepthEvent event) {
			receivedEvents.add(event);
		}

		@Override
		public void notifyStreamCompleted(int streamReqId) {
			streamComplete = true;
			
		}
		
	}
	
}
