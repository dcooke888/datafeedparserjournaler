package test.JDatabase.Utils;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Test;

import com.JDatabase.Utils.DateUtils;

public class DateUtilsTest {

	int year = 2000;
	int month = 12;
	int day = 31;
	
	public static final String dateString = "2000_12_31";
	public static final String iqFeedDate = "";
	public static final String iqFeedESTTime = "";
	public static final String iqFeedCentralTime = "";
	public static final DateFormat df = new SimpleDateFormat("yyyy_MM_dd");
	public static final DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	
	
	@Test
	public void shouldParseDateProperly() throws ParseException {
		long timeVal = DateUtils.getStartESTDateValue(year, month, day);
		long parsedTime = df.parse(dateString).getTime();
		assertTrue(timeVal == parsedTime);
	}
	
	@Test
	public void shouldPrintDateProperly() throws ParseException {
		long parsedTime = df.parse(dateString).getTime();
		assertTrue(dateString.equals(DateUtils.getESTDateString(parsedTime)));
	}
	
	@Test
	public void rangesShouldOverlap() {
		long s1 = 0;
		long e1 = 2;
		long s2 = -1;
		long e2 = 1;
		assertTrue(DateUtils.doRangesOverlap(s1, e1, s2, e2));
	}
	
	@Test
	public void rangesShouldntOverlap() {
		long s1 = 0;
		long e1 = 2;
		long s2 = -1;
		long e2 = -1;
		assertFalse(DateUtils.doRangesOverlap(s1, e1, s2, e2));
	}
}
