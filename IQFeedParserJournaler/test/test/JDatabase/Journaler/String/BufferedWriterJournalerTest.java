package test.JDatabase.Journaler.String;

import java.io.File;
import java.util.TimeZone;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.JDatabase.IQFeed.Feed;
import com.JDatabase.Journaler.String.BufferedWriterJournaler;
import com.JDatabase.Journaler.String.StringJournaler;

public class BufferedWriterJournalerTest {
	StringJournaler j;
	
	@Before
	public void setUp() throws Exception {
		File temp = File.createTempFile("temp",".txt");
		temp.deleteOnExit();
		j = new BufferedWriterJournaler(temp.getAbsolutePath());
	}

	@After
	public void tearDown() throws Exception {
		j.close();
	}

	@Test
	public void testWrite() {
		String testString = "HELLO, I'm a String";
		j.write(Feed.L1, TimeZone.getDefault(), System.currentTimeMillis(), testString);
	}

}
