package test.JDatabase.Journaler.String;
import java.util.Arrays;

import com.JDatabase.Utils.StringUtils;


public class TestSplitFunction {

	public static void main(String[] args)
	{
		int ITERATIONS = 1000 * 1000;
		String toTest = "L1,F,AAPL,5,14.0,11462000,575.1358,385.1000,575.1360,385.1000,2.1700,3.0500,12.2000,11/14/2013,11/06/2013,,,,17038304,,40.03,,0.49,09,,APPLE,AAPL AAPL7,62.9,0.63,,73286.0,43658.0,09/30/2013,16960.0,899738,334220,0.50 02/28/2005,0.50 06/21/2000,,0,14,4,3571,18.50,1,21,12/05/2013,04/19/2013,12/05/2013,04/19/2013,532.17,,,,,334220,4,4";
		long[] splitPerf = new long[ITERATIONS];
		
		
//		String[] tokens = toTest.split(",");
//		for(String token: tokens)
//		{
//			System.out.print(token + "|");
//		}
//		System.out.println();
//		tokens = StringUtilities.getCommaSplitString(toTest);
//		for(String token: tokens)
//		{
//			System.out.print(token + "|");
//		}
		long start,end, overallStart, overallEnd;
		String[] tokens = null;
		for(int j = 0; j < 5; j++) 
		{
			overallStart = System.nanoTime();
			for(int i = 0; i < ITERATIONS; i++)
			{
//				start = System.nanoTime();
				tokens = toTest.split(",");
//				end = System.nanoTime();
//				splitPerf[i] = (end-start);
			}
			overallEnd = System.nanoTime();

//			printPerformance("Split\t\t", splitPerf, ITERATIONS-1);
			System.out.println("OverallTime: " + (overallEnd - overallStart));
			tokens = null;
			overallStart = System.nanoTime();
			for(int i = 0; i < ITERATIONS; i++)
			{
//				start = System.nanoTime();
				tokens = StringUtils.getCommaSplitString(toTest);
//				end = System.nanoTime();
//				splitPerf[i] = (end-start);
			}
			overallEnd = System.nanoTime();
//			printPerformance("StringUtilities\t", splitPerf, ITERATIONS-1);
			System.out.println("OverallTime: " + (overallEnd - overallStart));
		}
		if(tokens != null) System.out.println("tokens length from split" + tokens.length);
		
	}
	
	private static void printPerformance(String name, long[] times, int repeats)
	{
		System.out.printf("%s: ", name);
	    Arrays.sort(times);
	    for (double perc : new double[]{50, 90, 99, 99.9, 99.99}) {
	        System.out.printf("%s%% took %.1f �s, ", perc, times[((int) (repeats * perc / 100))] / 1000.0);
	    }
	    System.out.printf("worst took %d �s%n", times[times.length - 1] / 1000);
	}
}
