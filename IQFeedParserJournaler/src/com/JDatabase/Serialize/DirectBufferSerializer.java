package com.JDatabase.Serialize;

import java.nio.ByteBuffer;

public class DirectBufferSerializer implements Serializer{
	private ByteBuffer mem;
	
	public DirectBufferSerializer(int maxMsgSize)
	{
		mem = ByteBuffer.allocateDirect(maxMsgSize);
	}
	
	public DirectBufferSerializer(byte[] buffer)
	{
		mem = ByteBuffer.allocateDirect(buffer.length);
		mem.put(buffer);
		mem.flip();
	}
	
	public DirectBufferSerializer(byte[] buffer1, byte[] buffer2)
	{
		mem = ByteBuffer.allocateDirect(buffer1.length + buffer2.length);
		mem.put(buffer1);
		mem.put(buffer2);
		mem.flip();
	}
	
	@Override
	public void clearAndPutWrittenBytes(byte[] data) {
		mem.clear();
		if(data.length > mem.capacity()) {
			mem = ByteBuffer.allocateDirect(data.length);
		}
		mem.put(data);
		mem.flip();
	}

	@Override
	public void flip() {
		mem.flip();
	}

	@Override
	public void compact() {
	    if (mem.remaining() == 0)	mem.clear();
	    else mem.compact();
	}

	@Override
	public void clear() {
		mem.clear();
	}

	@Override
	public int remaining() {
		return mem.remaining();
	}

	@Override
	public boolean hasRemaining() {
		return mem.hasRemaining();
	}
	
	@Override
	public int capacity() {
		return mem.capacity();
	}

	@Override
	public void putBoolean(boolean value) {
	    mem.put((byte) (value ? 1 : 0));
		
	}

	@Override
	public boolean getBoolean() {
		return mem.get() == 1;
	}

	@Override
	public void putByte(byte value) {
		mem.put(value);
		
	}

	@Override
	public byte getByte() {
		return mem.get();
	}

	@Override
	public void putInt(int value) {
		mem.putInt(value);
		
	}

	@Override
	public int getInt() {
		return mem.getInt();
	}

	@Override
	public void putDouble(double value) {
		mem.putDouble(value);
	}

	@Override
	public double getDouble() {
		return mem.getDouble();
	}

	@Override
	public void putLong(long value) {
		mem.putLong(value);
	}

	@Override
	public long getLong() {
		return mem.getLong();
	}

	@Override
	public void putString(String value) {
		putCharArray(value.toCharArray());
	}

	@Override
	public String getString() {
		return new String(getCharArray());
	}
	@Override
    public final void putCharArray(final char[] values) {
    	byte[] toWrite = convertCharToByteArray(values);
    	mem.putShort((short) toWrite.length);
    	mem.put(toWrite);
    }

	@Override
	public char[] getCharArray() {
		short msgSize = mem.getShort();
		byte[] toConvert = new byte[msgSize];
		mem.get(toConvert);
		return convertByteToCharArray(toConvert);
	}

	@Override
	public void putLongArray(long[] values) {
		mem.putShort((short) values.length);
		for(long value: values)
			mem.putLong(value);
	}

	@Override
	public long[] getLongArray() {
		short length = mem.getShort();
		long[] out = new long[length];
		for(int i = 0; i < length; i++)
			out[i] = mem.getLong();
		return out;
	}

	@Override
	public void putDoubleArray(double[] values) {
		mem.putShort((short) values.length);
		for(double value: values)
			mem.putDouble(value);
	}

	@Override
	public double[] getDoubleArray() {
		short length = mem.getShort();
		double[] out = new double[length];
		for(int i = 0; i < length; i++)
			out[i] = mem.getDouble();
		return out;
	}
	
	private byte[] convertCharToByteArray(char[] toConvert)
	{
		byte[] bytes = new byte[toConvert.length*2];
		for(int i=0;i<toConvert.length;i++) {
		   bytes[i*2] = (byte) (toConvert[i] >> 8);
		   bytes[i*2+1] = (byte) toConvert[i];
		}
		return bytes;
	}
	private char[] convertByteToCharArray(byte[] toConvert)
	{
		char[] chars = new char[toConvert.length/2];
		for(int i=0;i<chars.length;i++) 
		{
		   chars[i] = (char) ((toConvert[i*2] << 8) + toConvert[i*2+1]);
		}
		return chars;
	}

	@Override
	public byte[] getWrittenBytes() {
		mem.flip();
		if(mem.remaining() == 0) return null;
		byte[] out = new byte[mem.remaining()];
		mem.get(out);
		mem.clear();
		return out;
	}

	@Override
	public void putByteArray(byte[] values) {
		mem.putShort((short) values.length);
		mem.put(values);
	}

	@Override
	public byte[] getByteArray() {
		short length = mem.getShort();
		byte[] out = new byte[length];
		mem.get(out);
		return out;
	}

	@Override
	public byte[] getByteArrayIfAvailable() {
		if(mem.remaining() < 2) return null;
		int startPos = mem.position();
		short length = mem.getShort();
		if(mem.remaining() >= length) {
			byte[] out = new byte[length];
			mem.get(out);
			return out;
		} else {
			mem.position(startPos);
			return null;
		}
	}

	@Override
	public Serializer getNewSerializerInstance(int bufferSize) {
		return new DirectBufferSerializer(bufferSize);
	}
	
	@Override
	public Serializer getNewSerializerInstance() {
		return new DirectBufferSerializer(mem.capacity());
	}

}
