package com.JDatabase.Serialize;

public interface Serializer {
	Serializer getNewSerializerInstance();
	Serializer getNewSerializerInstance(int bufferSize);
	void clearAndPutWrittenBytes(byte[] data);
	byte[] getWrittenBytes();
	void flip();
	void compact();
	void clear();
	int remaining();
	boolean hasRemaining();
	int capacity();
    void putBoolean(final boolean value);
    boolean getBoolean();
    void putByte(final byte value);
    byte getByte();
    void putInt(final int value);
    int getInt();
    void putDouble(final double value);
    double getDouble();
    void putLong(final long value);
    long getLong();
    void putByteArray(final byte[] value);
    byte[] getByteArray();
	byte[] getByteArrayIfAvailable();
    void putString(final String value);
    String getString();
    void putCharArray(final char[] values);
    char[] getCharArray();
    void putLongArray(final long[] values);
    long[] getLongArray();
    void putDoubleArray(final double[] values);
    double[] getDoubleArray();
}
