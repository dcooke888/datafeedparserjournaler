package com.JDatabase.Serialize;

import java.lang.reflect.Field;

import sun.misc.Unsafe;

public class UnsafeMemory implements Serializer
{
	private static final Unsafe unsafe;
    static
    {
        try
        {
            Field field = Unsafe.class.getDeclaredField("theUnsafe");
            field.setAccessible(true);
            unsafe = (Unsafe)field.get(null);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
 
    private static final long byteArrayOffset = unsafe.arrayBaseOffset(byte[].class);
    private static final long longArrayOffset = unsafe.arrayBaseOffset(long[].class);
    private static final long doubleArrayOffset = unsafe.arrayBaseOffset(double[].class);
    private static final long charArrayOffset = unsafe.arrayBaseOffset(char[].class);

 
    private static final int SIZE_OF_BOOLEAN = 1;
    private static final int SIZE_OF_BYTE = 1;
    private static final int SIZE_OF_SHORT = 2;
    private static final int SIZE_OF_INT = 4;
    private static final int SIZE_OF_LONG = 8;
    private static final int SIZE_OF_DOUBLE = 8;
 
    private int pos = 0;
    private int limit = 0;
    private byte[] buffer;
    
    public UnsafeMemory(int bufferSize)
    {
    	this(new byte[bufferSize], bufferSize);
    }
 
    public UnsafeMemory(final byte[] buffer)
    {
    	this(buffer, buffer.length);
    }
    public UnsafeMemory(final byte[] buffer, int bufferFill)
    {
        if (null == buffer || bufferFill <= 0)
        {
            throw new NullPointerException("buffer cannot be null");
        }
        byte[] tempBuffer = new byte[bufferFill];
    	unsafe.copyMemory(buffer, byteArrayOffset, tempBuffer, byteArrayOffset, bufferFill);
        this.buffer = tempBuffer;
        this.limit = tempBuffer.length - 1;
    }
    
    public UnsafeMemory(final byte[] remainingBuffer, final byte[] newBuffer)
    {
    	this(remainingBuffer, newBuffer, newBuffer.length);
    }
    public UnsafeMemory(final byte[] remainingBuffer, final byte[] newBuffer, int newBufferFill)
    {
    	if(remainingBuffer == null || newBuffer == null || newBufferFill <= 0)
        {
            throw new NullPointerException("Neither buffer 1 or 2 can be null, and newBufferSize cannot be zero");
        }
    	byte[] tempBuffer = new byte[remainingBuffer.length + newBufferFill];
    	unsafe.copyMemory(remainingBuffer, byteArrayOffset, tempBuffer, byteArrayOffset, remainingBuffer.length);
    	unsafe.copyMemory(newBuffer, byteArrayOffset, tempBuffer, byteArrayOffset + remainingBuffer.length, newBufferFill);
    	this.buffer = tempBuffer;
    	this.limit = tempBuffer.length - 1;
    }

    
    public byte[] getWrittenBuffer()
    {
    	if(pos == 0) return null;
    	byte[] values = new byte[pos];
    	unsafe.copyMemory(buffer, byteArrayOffset, values, byteArrayOffset, pos);
    	reset();
    	return values;
    }
    public byte[] getUnreadBuffer()
    {
    	byte[] tempBuffer = new byte[buffer.length - pos];
    	unsafe.copyMemory(buffer, byteArrayOffset + pos, tempBuffer, byteArrayOffset, buffer.length - pos);
    	return tempBuffer;
    }
 
    public void putBoolean(final boolean value)
    {
        unsafe.putBoolean(buffer, byteArrayOffset + pos, value);
        pos += SIZE_OF_BOOLEAN;
    }
 
    public boolean getBoolean()
    {
        boolean value = unsafe.getBoolean(buffer, byteArrayOffset + pos);
        pos += SIZE_OF_BOOLEAN;
 
        return value;
    }
    public void putByte(final byte value)
    {
        unsafe.putByte(buffer, byteArrayOffset + pos, value);
        pos += SIZE_OF_BYTE;
    }
 
    public byte getByte()
    {
        byte value = unsafe.getByte(buffer, byteArrayOffset + pos);
        pos += SIZE_OF_BOOLEAN;
 
        return value;
    }
    
    public void putShort(final short value)
    {
    	unsafe.putShort(buffer,  byteArrayOffset + pos, value);
    	pos += SIZE_OF_SHORT;
    }
    
    public short getShort()
    {
    	short value = unsafe.getShort(buffer, byteArrayOffset + pos);
    	pos += SIZE_OF_SHORT;
    	return value;
    }
    
 
    public void putInt(final int value)
    {
        unsafe.putInt(buffer, byteArrayOffset + pos, value);
        pos += SIZE_OF_INT;
    }
 
    public int getInt()
    {
        int value = unsafe.getInt(buffer, byteArrayOffset + pos);
        pos += SIZE_OF_INT;
 
        return value;
    }
    
    public void putDouble(final double value)
    {
        unsafe.putDouble(buffer, byteArrayOffset + pos, value);
        pos += SIZE_OF_DOUBLE;
    }
 
    public double getDouble()
    {
        double value = unsafe.getDouble(buffer, byteArrayOffset + pos);
        pos += SIZE_OF_DOUBLE;
 
        return value;
    }
    
 
    public void putLong(final long value)
    {
        unsafe.putLong(buffer, byteArrayOffset + pos, value);
        pos += SIZE_OF_LONG;
    }
 
    public long getLong()
    {
        long value = unsafe.getLong(buffer, byteArrayOffset + pos);
        pos += SIZE_OF_LONG;
 
        return value;
    }
    
    public void putString(final String value)
    {
    	putCharArray(value.toCharArray());
    }
    public String getString()
    {
    	char[] values = getCharArray();
        return new String(values);
    }
    
    public void putCharArray(final char[] values) {
        putShort((short) values.length);
        long bytesToCopy = values.length << 1;
        unsafe.copyMemory(values, charArrayOffset, buffer, byteArrayOffset + pos, bytesToCopy);
        pos += bytesToCopy;
      }


      public char[] getCharArray() {
        short arraySize = getShort();
        char[] values = new char[arraySize];
        long bytesToCopy = values.length << 1;
        unsafe.copyMemory(buffer, byteArrayOffset + pos, values, charArrayOffset, bytesToCopy);
        pos += bytesToCopy;
        return values;
      }
      
      @Override
      public void putByteArray(final byte[] values)
      {
          putInt(values.length);
   
//          long bytesToCopy = values.length << 1;
          long bytesToCopy = values.length;
          unsafe.copyMemory(values, byteArrayOffset,
                            buffer, byteArrayOffset + pos,
                            bytesToCopy);
          pos += bytesToCopy;
      }
   
      @Override
      public byte[] getByteArray()
      {
          int arraySize = getInt();
          byte[] values = new byte[arraySize];
   
//          long bytesToCopy = values.length << 1;
          long bytesToCopy = values.length;
          unsafe.copyMemory(buffer, byteArrayOffset + pos,
                            values, byteArrayOffset,
                            bytesToCopy);
          pos += bytesToCopy;
   
          return values;
      }
      
  	@Override
  	public byte[] getByteArrayIfAvailable() {
        int arraySize = unsafe.getInt(buffer, byteArrayOffset + pos);
        if(remaining() - SIZE_OF_INT >= arraySize)
        {
        	pos += SIZE_OF_INT;
            byte[] values = new byte[arraySize];
            
//            long bytesToCopy = values.length << 1;
            long bytesToCopy = values.length;
            unsafe.copyMemory(buffer, byteArrayOffset + pos,
                              values, byteArrayOffset,
                              bytesToCopy);
            pos += bytesToCopy;
     
            return values;
        }
        else
        {
        	return null;
        }

  	}
 
 
    public void putLongArray(final long[] values)
    {
        putInt(values.length);
 
        long bytesToCopy = values.length << 3;
        unsafe.copyMemory(values, longArrayOffset,
                          buffer, byteArrayOffset + pos,
                          bytesToCopy);
        pos += bytesToCopy;
    }
 
    public long[] getLongArray()
    {
        int arraySize = getInt();
        long[] values = new long[arraySize];
 
        long bytesToCopy = values.length << 3;
        unsafe.copyMemory(buffer, byteArrayOffset + pos,
                          values, longArrayOffset,
                          bytesToCopy);
        pos += bytesToCopy;
 
        return values;
    }
 
    public void putDoubleArray(final double[] values)
    {
        putInt(values.length);
 
        long bytesToCopy = values.length << 3;
        unsafe.copyMemory(values, doubleArrayOffset,
                          buffer, byteArrayOffset + pos,
                          bytesToCopy);
        pos += bytesToCopy;
    }
 
    public double[] getDoubleArray()
    {
        int arraySize = getInt();
        double[] values = new double[arraySize];
 
        long bytesToCopy = values.length << 3;
        unsafe.copyMemory(buffer, byteArrayOffset + pos,
                          values, doubleArrayOffset,
                          bytesToCopy);
        pos += bytesToCopy;
 
        return values;
    }
    @Override
    public void flip()
    {
		limit = pos;
		pos = 0;
    }
    
    public void reset()
    {
        this.pos = 0;
        this.limit = buffer.length - 1;
    }
    public int sizeRead() 	{ return pos;	}
    
	@Override
	public void compact() {
		if(hasRemaining())
		{
			byte[] copy = new byte[buffer.length];
	    	unsafe.copyMemory(buffer, byteArrayOffset, copy, byteArrayOffset, limit - pos);
	    	this.buffer = copy;
	    	pos = limit - pos;
	    	limit = buffer.length - 1;
		}
		else reset();
	}
	@Override
	public void clear() {
		pos = 0;
		limit = buffer.length - 1;
		
	}
	@Override
	public boolean hasRemaining() {
		return (limit - pos > 0);
	}
    @Override
    public int remaining() 	{ 
    	return  limit - pos;
	}
	@Override
	public int capacity() {
		return buffer.length;
	}

	@Override
	public byte[] getWrittenBytes() {
		return getWrittenBuffer();
	}

	@Override
	public void clearAndPutWrittenBytes(byte[] data) {
		clear();
		if(data.length > capacity()) {
	        buffer = new byte[data.length];
		}
    	unsafe.copyMemory(data, byteArrayOffset, buffer, byteArrayOffset, data.length);
        this.limit = data.length;
	}

	@Override
	public Serializer getNewSerializerInstance() {
		return new UnsafeMemory(buffer.length);
	}

	@Override
	public Serializer getNewSerializerInstance(int bufferSize) {
		return new UnsafeMemory(bufferSize);
	}
}