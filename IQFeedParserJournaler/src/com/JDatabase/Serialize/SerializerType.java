package com.JDatabase.Serialize;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum SerializerType {
	UNKNOWN((byte) 0),
	DirectBuffer((byte) 1),
	Unsafe((byte) 2);
	
	private byte val;

	private SerializerType(byte val) {
		this.val = val;
	}
	
	private static final Map<Byte,SerializerType> lookupByte = new HashMap<Byte,SerializerType>();

	static {
	    for(SerializerType s : EnumSet.allOf(SerializerType.class))
	    {
	        lookupByte.put(s.getByteId(), s);
	    }
	}
	
	public byte getByteId()
	{
		return this.val;
	}

	public static SerializerType fromByteId(byte b)
	{
		SerializerType type = lookupByte.get(b);
		return (type == null) ? UNKNOWN : type;
	}
	
	public static final Serializer getSerializer(SerializerType type, int bufferSize) {
		switch(type) {
		case DirectBuffer:
			return new DirectBufferSerializer(bufferSize);
		case Unsafe:
			return new UnsafeMemory(bufferSize);
		case UNKNOWN:
		default:
			return null;
		}
	}
}
