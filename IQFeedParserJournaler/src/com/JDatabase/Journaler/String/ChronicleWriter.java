package com.JDatabase.Journaler.String;

import java.io.IOException;
import java.util.TimeZone;

import com.JDatabase.IQFeed.Feed;
import com.JDatabase.Serialize.Serializer;
import com.JDatabase.Serialize.UnsafeMemory;
import com.higherfrequencytrading.chronicle.Excerpt;
import com.higherfrequencytrading.chronicle.impl.IndexedChronicle;

public class ChronicleWriter implements StringJournaler{
	IndexedChronicle chronicle;
	Excerpt excerpt;
	Serializer serializer = new UnsafeMemory(1024 * 100);
	
	public ChronicleWriter(String filename) throws IOException{
		chronicle = new IndexedChronicle(filename);
		excerpt = chronicle.createExcerpt();
	}

	@Override
	public void write(Feed feed, TimeZone t, long receiveTime, String message) {
		serializer.putByte(feed.getId());
		serializer.putString(t.getID());
		serializer.putLong(receiveTime);
		serializer.putString(message);
		byte[] out = serializer.getWrittenBytes();
		excerpt.startExcerpt(out.length);
		excerpt.write(out);
		excerpt.finish();
		
	}

	@Override
	public void close() {
		chronicle.close();
		
	}

}
