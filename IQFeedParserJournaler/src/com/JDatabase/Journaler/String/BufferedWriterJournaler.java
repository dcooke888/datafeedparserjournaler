package com.JDatabase.Journaler.String;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.TimeZone;

import com.JDatabase.IQFeed.Feed;

public class BufferedWriterJournaler implements StringJournaler{
	public static final String newLine = System.getProperty("line.separator");
	
	BufferedWriter w;

	public BufferedWriterJournaler(String filename) throws IOException {
		this.w = new BufferedWriter(new FileWriter(filename));
	}
	

	@Override
	public void write(Feed feed, TimeZone t, long receiveTime, String message) {
		String out = feed.toString().concat(",").concat(message).concat(",").concat(t.getID()).concat(",").concat(String.valueOf(receiveTime)).concat(newLine);
		try {
			w.write(out.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	@Override
	public void close() {
		try {
			if(w != null) w.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
