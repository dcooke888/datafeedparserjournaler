package com.JDatabase.Journaler.String;

import java.util.TimeZone;

import com.JDatabase.IQFeed.Feed;

public interface StringJournaler {
	void write(Feed feed, TimeZone t, long receiveTime, String message);
	void close();
}
