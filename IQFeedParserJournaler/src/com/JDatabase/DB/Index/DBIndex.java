package com.JDatabase.DB.Index;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.JDatabase.DB.File.FileQueue;
import com.JDatabase.DB.Write.DBWriter;
import com.JDatabase.Events.DataEvent;
import com.JDatabase.Events.DepthEvent;
import com.JDatabase.Events.TradeEvent;
import com.JDatabase.Events.TOBEvent;
import com.JDatabase.Events.TicType;
import com.JDatabase.Utils.FileUtilities;

public abstract class DBIndex {
	
	Set<String> symbols = new HashSet<String>();
	Set<String> fileStreamIds = new HashSet<String>();
	List<FileQueue> fileQueues = new ArrayList<FileQueue>();
	private final Map<String, DBWriter> openFiles = new ConcurrentHashMap<String, DBWriter>();
	protected final String dbBasePath;
	
	
	public DBIndex(String dbBasePath) {
		this.dbBasePath = dbBasePath;
		File file = new File(dbBasePath);
		boolean dirExists = file.exists()  && file.isDirectory();
		if(dirExists) {
			refreshIndex();
		} else {
			file.mkdirs();
		}
	}
	
	public void refreshIndex() {
		String[] filenames = FileUtilities.getAbsoluteFileNamesOfType(dbBasePath, ".tic");
		if(filenames != null) indexAllFiles(filenames);
	}
	
	public abstract String getFileStreamId(String filename);
	public abstract String getFileStreamId(String symbol, TicType type);
	public abstract void indexAllFiles(String[] allFilenames);
	public abstract FileQueue[] getAllFileQueues(long startTime, long endTime, boolean trade, boolean tob, boolean depth);
	
	public String getFileStreamId(DataEvent event) {
		switch(event.eventType) {
		case TRADE:
			return getFileStreamId(((TradeEvent) event).symbol, TicType.TRADE);
		case ASK:
		case BID:
		case REGIONAL_ASK:
		case REGIONAL_BID:
			return getFileStreamId(((TOBEvent) event).symbol, TicType.TOB);
		case ASK_DEPTH:
		case BID_DEPTH:
			return getFileStreamId(((DepthEvent) event).symbol, TicType.DEPTH);
		default:
			return null;
		}
	}
	
	public abstract StreamFilter getStreamFilterAllSymbols(long startTime, long endTime, boolean trade, boolean tob, boolean depth);
	public abstract StreamFilter getStreamFilter(long startTime, long endTime, String[] symbols,
			boolean trade, boolean tob, boolean depth);
	
	// File Queue and Stream Methods;
	public abstract FileQueue getFileQueue(String fileStreamId, long startTime, long endTime);
	
	public FileQueue[] getFileQueues(long startTime, long endTime, String[] symbols,
			boolean trade, boolean tob, boolean depth) {
		String[] fileStreamIds = getFileStreamIds(symbols, trade, tob, depth);
		return getFileQueues(fileStreamIds, startTime, endTime);
	}

	private String[] getFileStreamIds(String[] symbols, boolean trade, boolean tob, boolean depth) {
		fileStreamIds.clear();
		for(String symbol: symbols) {
			if(trade) addFileStreamId(getFileStreamId(symbol, TicType.TRADE));
			if(tob) addFileStreamId(getFileStreamId(symbol, TicType.TOB));
			if(depth) addFileStreamId(getFileStreamId(symbol, TicType.DEPTH));
		}
		return fileStreamIds.toArray(new String[fileStreamIds.size()]);
	}
	
	private void addFileStreamId(String fileStreamId) {
		if(fileStreamId != null) fileStreamIds.add(fileStreamId);
	}
	
	private FileQueue[] getFileQueues(String[] fileStreamIds, long startTime, long endTime) {
		fileQueues.clear();
		for(String fileStreamId: fileStreamIds) {
			FileQueue fileQueue = getFileQueue(fileStreamId, startTime, endTime);
			if(fileQueue != null) fileQueues.add(fileQueue);
		}
		return fileQueues.toArray(new FileQueue[fileQueues.size()]);
	}
	
	public void writeEvent(DataEvent event) throws IOException {
		TicType ticType = TicType.getTicType(event.eventType);
		if(ticType == TicType.UNKNOWN) return;
		String absoluteFilename = getAbsoluteFilename(getFilename(event));
		DBWriter holder = getWriter(absoluteFilename);
		holder.write(event);
		openFiles.put(absoluteFilename, holder);
	}
	
	abstract String getFilename(DataEvent event);
	
	private void closeOpenFiles(String fileStreamIdToMatch) throws IOException {
		for(String filename: openFiles.keySet()) {
			if(getFileStreamId(filename).equals(fileStreamIdToMatch)) {
				DBWriter toClose = openFiles.remove(filename);
				toClose.close();
			}
		}
	}
	
	private DBWriter getWriter(String filename) throws IOException {
		DBWriter holder = openFiles.get(filename);
		if(holder == null) {
			closeOpenFiles(getFileStreamId(filename));
			holder = new DBWriter(filename);
		}
		return holder;
	}
	
	protected String getAbsoluteFilename(String relativeFilename) {
		return dbBasePath.concat(relativeFilename);
	}
	
	public boolean isDatabaseInWriteMode() {
		return openFiles.size() > 0;
	}
	public void setDatabaseToRead() throws IOException {
		for(DBWriter holder: openFiles.values()) {
			holder.close();
		}
		openFiles.clear();
		refreshIndex();
	}
	
}
