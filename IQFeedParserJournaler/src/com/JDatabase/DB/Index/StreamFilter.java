package com.JDatabase.DB.Index;

import com.JDatabase.Events.DataEvent;

public interface StreamFilter {
	DataEvent filterEvent(DataEvent event);
}
