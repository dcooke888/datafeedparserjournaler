package com.JDatabase.DB.Index;

import java.util.HashMap;

import com.JDatabase.Byte.ReaderWriterType;
import com.JDatabase.DB.File.FileIndex;
import com.JDatabase.DB.File.FileQueue;
import com.JDatabase.DB.File.TicFile;
import com.JDatabase.Events.DataEvent;
import com.JDatabase.Events.TicType;
import com.JDatabase.Serialize.SerializerType;
import com.JDatabase.Utils.DateUtils;
import com.JDatabase.Utils.FileUtilities;
import com.JDatabase.Utils.StringUtils;

public class DateStreamTypeSymbolIndex extends DBIndex{
	private static final ReaderWriterType rwType = ReaderWriterType.LZ4;
	private static final SerializerType serializerType = SerializerType.Unsafe;
	private HashMap<String, FileIndex> fileStreamIndex;

	public DateStreamTypeSymbolIndex(String dbBasePath) {
		super(dbBasePath);
	}
	@Override
	public String getFileStreamId(String filename) {
		String relativeFilename = FileUtilities.getRelativeFilename(filename);
		int idx = relativeFilename.lastIndexOf("_");
		return (idx < 0) ? relativeFilename : relativeFilename.substring(idx + 1, relativeFilename.length() - 4);
	}

	@Override
	public String getFileStreamId(String symbol, TicType type) {
		return type.toString().concat(",").concat(symbol);
	}

	@Override
	public void indexAllFiles(String[] allFilenames) {
		FileIndex idx;
		if(fileStreamIndex == null) fileStreamIndex = new HashMap<String, FileIndex>();
		else fileStreamIndex.clear();
		for(String absoluteFilename: allFilenames) {
			String fileStreamId = getFileStreamId(absoluteFilename);
			if(fileStreamId == null) continue;
			idx = fileStreamIndex.get(fileStreamId);
			if(idx == null) idx = new FileIndex(fileStreamId);
			idx.addFileString(absoluteFilename, DateUtils.getFilenameStartTime(absoluteFilename));
			fileStreamIndex.put(fileStreamId, idx);
		}
	}

	@Override
	public StreamFilter getStreamFilter(long startTime, long endTime,
			String[] symbols, boolean trade, boolean tob, boolean depth) {
		return new DateFilter(startTime, endTime);
	}
	

	@Override
	public StreamFilter getStreamFilterAllSymbols(long startTime, long endTime,
			boolean trade, boolean tob, boolean depth) {
		return new DateFilter(startTime, endTime);
	}

	@Override
	public FileQueue getFileQueue(String fileStreamId, long startTime,
			long endTime) {
		if(fileStreamIndex == null) fileStreamIndex = new HashMap<String, FileIndex>();
		FileIndex idx = fileStreamIndex.get(fileStreamId);
		if(idx == null) return null;
		return new FileQueue(fileStreamId, idx.getOrderedFilenames(startTime, endTime),
				rwType, serializerType);
	}
	
	private boolean checkFileStreamId(String fileStreamId, boolean trade, boolean tob, boolean depth) {
		String[] tokens = StringUtils.getCommaSplitString(fileStreamId);
		TicType ticType = TicType.fromString(tokens[0]);
		if(trade && ticType == TicType.TRADE) return true;
		else if(tob && ticType == TicType.TOB) return true;
		else if(depth && ticType == TicType.DEPTH) return true;
		else return false;
	}

	@Override
	public FileQueue[] getAllFileQueues(long startTime, long endTime, boolean trade, boolean tob, boolean depth) {
		FileQueue[] out = new FileQueue[fileStreamIndex.size()];
		int i = 0;
		for(String fileStreamId: fileStreamIndex.keySet()) {
			if(!checkFileStreamId(fileStreamId, trade, tob, depth)) continue;
			FileIndex idx = fileStreamIndex.get(fileStreamId);
			TicFile[] files = idx.getOrderedFilenames(startTime, endTime);
			if(files != null) {
				out[i++] = new FileQueue(fileStreamId, idx.getOrderedFilenames(startTime, endTime),
						rwType, serializerType);
			}
		}
		return getSubset(out, i);
	}
	
	private FileQueue[] getSubset(FileQueue[] in, int lengthToUse) {
		if(lengthToUse == in.length) return in;
		FileQueue[] out = new FileQueue[lengthToUse];
		System.arraycopy(in, 0, out, 0, lengthToUse);
		return out;
	}

	@Override
	String getFilename(DataEvent event) {
		String date = DateUtils.getESTDateString(event.receiveTime);
		return date.concat("_").concat(getFileStreamId(event)).concat(".tic");
	}
	
	class DateFilter implements StreamFilter {
		private final long startTime;
		private final long endTime;
		
		public DateFilter(long startTime, long endTime) {
			this.startTime = startTime;
			this.endTime = endTime;
		}

		@Override
		public DataEvent filterEvent(DataEvent event) {
			if(event.receiveTime < startTime || event.receiveTime > endTime) return null;
			return event;
		}
	}
}
