package com.JDatabase.DB;

import java.io.IOException;

import com.JDatabase.DB.File.FileQueue;
import com.JDatabase.DB.Index.DBIndex;
import com.JDatabase.DB.Index.StreamFilter;
import com.JDatabase.DB.Read.DBReader;
import com.JDatabase.DB.Read.StreamRequest;
import com.JDatabase.DataSource.TicDataFeed;
import com.JDatabase.Events.DataEvent;

public class TicDBMS {
	protected final DBIndex index;
	private final DBReader reader;
	private int uniqueStreamRequestId = 0;
	
	public TicDBMS(DBIndex index) {
		this.index = index;
		this.reader = new DBReader(index);
	}
	
	// Write Methods
	public boolean write(DataEvent event) {
		try {
			index.writeEvent(event);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public void closeDBForWriting() {
		try {
			index.setDatabaseToRead();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// Read Methods
	public int stream(TicDataFeed[] dataFeeds, long startTime, long endTime, String[] symbols, boolean trade, boolean tob, boolean depth) {
		FileQueue[] fileQueues = index.getFileQueues(startTime, endTime, symbols, trade, tob, depth);
		StreamFilter streamFilter = index.getStreamFilter(startTime, endTime, symbols, trade, tob, depth);
		StreamRequest streamRequest = new StreamRequest(uniqueStreamRequestId++, fileQueues, streamFilter, dataFeeds);
		reader.streamFilteredFileQueues(streamRequest);
		return streamRequest.streamReqId;
	}
	
	public int streamAllSymbols(TicDataFeed[] dataFeeds, long startTime, long endTime, boolean trade, boolean tob, boolean depth) {
		FileQueue[] fileQueues = index.getAllFileQueues(startTime, endTime, trade,tob, depth);
		StreamFilter streamFilter = index.getStreamFilterAllSymbols(startTime, endTime, trade, tob, depth);
		StreamRequest streamRequest = new StreamRequest(uniqueStreamRequestId++, fileQueues, streamFilter, dataFeeds);
		reader.streamFilteredFileQueues(streamRequest);
		return streamRequest.streamReqId;
	}

	public int streamTrade(TicDataFeed[] dataFeeds, long startTime, long endTime, String[] symbols) {
		return stream(dataFeeds, startTime, endTime, symbols, true, false, false);
	}
	public int streamTOB(TicDataFeed[] dataFeeds, long startTime, long endTime, String[] symbols) {
		return stream(dataFeeds, startTime, endTime, symbols, false, true, false);
	}
	public int streamDepth(TicDataFeed[] dataFeeds, long startTime, long endTime, String[] symbols) {
		return stream(dataFeeds, startTime, endTime, symbols, false, false, true);
	}
	public int streamTradeTOB(TicDataFeed[] dataFeeds, long startTime, long endTime, String[] symbols) {
		return stream(dataFeeds, startTime, endTime, symbols, true, true, false);
	}
	public int streamTOBDepth(TicDataFeed[] dataFeeds, long startTime, long endTime, String[] symbols) {
		return stream(dataFeeds, startTime, endTime, symbols, false, true, true);
	}
	public int streamTradeTOBDepth(TicDataFeed[] dataFeeds, long startTime, long endTime, String[] symbols) {
		return stream(dataFeeds, startTime, endTime, symbols, true, true, true);
	}
	
}
