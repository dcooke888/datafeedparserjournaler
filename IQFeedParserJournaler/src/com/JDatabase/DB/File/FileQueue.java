package com.JDatabase.DB.File;

import java.io.IOException;
import java.util.PriorityQueue;
import java.util.Queue;

import com.JDatabase.Byte.ReaderWriterType;
import com.JDatabase.Byte.Reader.ByteReader;
import com.JDatabase.Events.DataEvent;
import com.JDatabase.Events.Serialize.EventDeserializer;
import com.JDatabase.Serialize.Serializer;
import com.JDatabase.Serialize.SerializerType;

public class FileQueue {
	public static final int DEFAULT_BUFFER_SIZE = 1024 * 100;
	
	private final ReaderWriterType rwType;
	private final Serializer serializer;
	private String fileStreamId;
	private Queue<TicFile> orderedFilenames = new PriorityQueue<TicFile>();
	private ByteReader reader = null;
			
	public FileQueue(String fileStreamId, String[] filenames,
			ReaderWriterType readerWriterType, SerializerType serializerType) {
		this.fileStreamId = fileStreamId;
		for(String filename: filenames){
			orderedFilenames.add(new TicFile(fileStreamId, filename));
		}
		this.rwType = readerWriterType;
		this.serializer = SerializerType.getSerializer(serializerType, DEFAULT_BUFFER_SIZE);
	}
	
	public FileQueue(String fileStreamId, TicFile[] files, ReaderWriterType readerWriterType, SerializerType serializerType) {
		this.fileStreamId = fileStreamId;
		for(TicFile file: files){
			orderedFilenames.add(file);
		}
		this.rwType = readerWriterType;
		this.serializer = SerializerType.getSerializer(serializerType, DEFAULT_BUFFER_SIZE);
	}

	public DataEvent getDataEvent() throws IOException {
		if(reader == null) {
			TicFile fileToRead = orderedFilenames.poll();
			if(fileToRead != null)
				reader = ReaderWriterType.getByteReader(rwType, fileToRead.absoluteFilename, DEFAULT_BUFFER_SIZE);
		}
		if(reader == null) return null;
		byte[] event = reader.readSingleEvent();
		if(event != null) {
			serializer.clearAndPutWrittenBytes(event);
			return EventDeserializer.deserializeEvent(serializer);
		} else {
			reader.close();
			reader = null;
			return getDataEvent();
		}
	}

	public String getFileStreamID() {
		return this.fileStreamId;
	}

}
