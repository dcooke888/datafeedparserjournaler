package com.JDatabase.DB.File;

import java.util.ArrayList;
import java.util.Collections;
import com.JDatabase.Utils.DateUtils;

public class FileIndex {
	
	public final String fileStreamId;
	private boolean sorted = false;
	private long minTime = Long.MAX_VALUE;
	private long maxTime = Long.MIN_VALUE;
	
	ArrayList<TicFile> files = new ArrayList<TicFile>();
	
	
	public FileIndex(String fileStreamId) {
		this.fileStreamId = fileStreamId;
	}
	
	public void addFileString(String relativePath, long startTime) {
		updateTimes(startTime);
		files.add(new TicFile(fileStreamId, relativePath, startTime));
		sorted = false;
	}
	

	private void updateTimes(long startTime) {
		if(startTime > maxTime) maxTime = startTime;
		if(startTime < minTime) minTime = startTime;
	}

	public TicFile[] getOrderedFilenames(long startTime, long endTime) {
		if(files.size() == 0) return null;
		checkSorted();
		ArrayList<TicFile> out = new ArrayList<TicFile>();
		for(TicFile file: files) {
			if(DateUtils.doRangesOverlap(startTime, endTime, file.startTime, file.startTime + DateUtils.millisPerDay))
				out.add(file);
		}
		return (out.size() > 0) ? out.toArray(new TicFile[out.size()]) : null;
	}

	private void checkSorted() {
		if(!sorted) {
			Collections.sort(files);
			sorted = true;
		}
	}
}
