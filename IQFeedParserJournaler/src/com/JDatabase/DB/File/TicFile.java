package com.JDatabase.DB.File;

import com.JDatabase.Utils.DateUtils;
import com.JDatabase.Utils.FileUtilities;

public class TicFile implements Comparable<TicFile>{

	public final String fileStreamId;
	public final String relativeFilename;
	public final String absoluteFilename;
	public final long startTime;
	
	public TicFile(String fileStreamId, String absoluteFilename) {
		this(fileStreamId, absoluteFilename, DateUtils.getFilenameStartTime(absoluteFilename));
	}
	
	public TicFile(String fileStreamId, String absoluteFilename, long startTime) {
		this.fileStreamId = fileStreamId;
		this.absoluteFilename = absoluteFilename;
		this.relativeFilename = FileUtilities.getRelativeFilename(absoluteFilename);
		this.startTime = startTime;
	}
	
	@Override
	public int compareTo(TicFile other) {
		if(this.startTime < other.startTime) return -1;
		else {
			return (this.startTime == other.startTime) ? 0 : 1;
		}
	}

}
