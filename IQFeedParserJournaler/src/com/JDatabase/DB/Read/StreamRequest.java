package com.JDatabase.DB.Read;

import com.JDatabase.DB.File.FileQueue;
import com.JDatabase.DB.Index.StreamFilter;
import com.JDatabase.DataSource.TicDataFeed;

public class StreamRequest {

	public final int streamReqId;
	public final FileQueue[] fileQueue;
	public final StreamFilter streamFilter;
	public final TicDataFeed[] dataFeeds;
	
	public StreamRequest(int streamReqId, FileQueue[] fileQueue, StreamFilter streamFilter, TicDataFeed[] dataFeeds) {
		this.streamReqId = streamReqId;
		this.fileQueue = fileQueue;
		this.streamFilter = streamFilter;
		this.dataFeeds = dataFeeds;
	}
}
