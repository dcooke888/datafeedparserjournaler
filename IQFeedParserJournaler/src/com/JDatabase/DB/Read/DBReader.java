package com.JDatabase.DB.Read;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.JDatabase.DB.File.FileQueue;
import com.JDatabase.DB.Index.DBIndex;
import com.JDatabase.DB.Index.StreamFilter;
import com.JDatabase.DataSource.TicDataFeed;
import com.JDatabase.Events.DataEvent;

public class DBReader implements Runnable{
	
	private final DBIndex index;
	private List<StreamRequest> streamRequests = new CopyOnWriteArrayList<StreamRequest>();
	private ExecutorService threadPool = Executors.newCachedThreadPool();

	public DBReader(DBIndex index) {
		this.index = index;
	}
	
	public void streamFilteredFileQueues(StreamRequest streamRequest) {
		if(streamRequest.fileQueue != null && streamRequest.streamFilter != null && streamRequest.dataFeeds != null) {
			streamRequests.add(streamRequest);
			threadPool.submit(this);
		}
	}
	
	@Override
	public void run() {
		synchronized(streamRequests) {
			while(streamRequests.size() > 0)  {
				StreamRequest req = streamRequests.remove(0);
				stream(req);
			}
		}
	}
	
	private void stream(StreamRequest streamRequest) {
		try {
			Queue<DataEvent> eventQueue = new PriorityQueue<DataEvent>();
			Map<String, FileQueue> fileQueueMap = new HashMap<String, FileQueue>();
			
			FileQueue[] fileQueues = streamRequest.fileQueue;
			StreamFilter streamFilter = streamRequest.streamFilter;
			TicDataFeed[] dataFeeds = streamRequest.dataFeeds;
			
			
			for(FileQueue fileQueue: fileQueues) {
				eventQueue.add(fileQueue.getDataEvent());
				fileQueueMap.put(fileQueue.getFileStreamID(), fileQueue);
			}
			while(eventQueue.size() > 0) {
				DataEvent toStream = eventQueue.poll();
				FileQueue fileQueue = getFileQueue(fileQueueMap, toStream);
				DataEvent nextEvent = eventQueue.peek();
				toStream = streamEventsUntilNextEvent(toStream, fileQueue, streamFilter, nextEvent, dataFeeds);
				if(toStream != null) eventQueue.add(toStream);
			}
			notifyEndOfStream(dataFeeds, streamRequest.streamReqId);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private FileQueue getFileQueue(Map<String, FileQueue> fileQueueMap, DataEvent event) {
		String fileStreamId = index.getFileStreamId(event);
		return (fileStreamId != null) ? fileQueueMap.get(fileStreamId) : null;
	}
	
	private DataEvent streamEventsUntilNextEvent(DataEvent toStream, FileQueue fileQueue, 
			StreamFilter filter, DataEvent nextEvent, TicDataFeed[] dataFeeds) throws IOException {
		if(fileQueue == null) {
			streamEvent(toStream, filter, dataFeeds);
			return null;
		}
		long nextEventTime = (nextEvent == null) ? Long.MAX_VALUE : nextEvent.receiveTime;
		while(toStream != null && toStream.receiveTime <= nextEventTime) {
			streamEvent(toStream, filter, dataFeeds);
			toStream = fileQueue.getDataEvent();
		}
		return toStream;
	}
	
	private void streamEvent(DataEvent event, StreamFilter filter, TicDataFeed[] dataFeeds) {
		DataEvent toStream = filter.filterEvent(event);
		if(toStream != null) streamFilteredEvent(toStream, dataFeeds);
	}

	private void streamFilteredEvent(DataEvent toStream,TicDataFeed[] dataFeeds) {
		for(TicDataFeed dataFeed: dataFeeds) {
			dataFeed.notify(toStream);
		}
	}
	
	private void notifyEndOfStream(TicDataFeed[] dataFeeds, int streamReqId) {
		for(TicDataFeed dataFeed: dataFeeds) {
			dataFeed.notifyStreamCompleted(streamReqId);
		}
	}

	
}
