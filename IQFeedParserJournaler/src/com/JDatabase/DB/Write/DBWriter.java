package com.JDatabase.DB.Write;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.JDatabase.Byte.ReaderWriterType;
import com.JDatabase.Byte.Reader.ByteReader;
import com.JDatabase.Byte.Writer.ByteWriter;
import com.JDatabase.Events.DataEvent;
import com.JDatabase.Events.Serialize.EventDeserializer;
import com.JDatabase.Serialize.Serializer;
import com.JDatabase.Serialize.SerializerType;
import com.JDatabase.Utils.FileUtilities;

public class DBWriter {
	public static final int DEFAULT_BUFFER_SIZE = 1024 * 1000;
	public static final ReaderWriterType DEFAULT_READER_WRITER_TYPE = ReaderWriterType.LZ4;
	public static final SerializerType DEFAULT_SERIALIZER = SerializerType.Unsafe;
	
	private final String absoluteFilename;
	private final ReaderWriterType rwType;
	private final SerializerType serializerType;
	private final Serializer deSerializer;
	private final int bufferSize;
	private ByteReader reader = null;
	private ByteWriter writer = null;
	private List<DataEvent> equalTimeEvents = new ArrayList<DataEvent>();
	private DataEvent lastReadEvent = null;
	private DataEvent lastWrittenEvent = null;
	
	public DBWriter(String absoluteFilename)	{
		this(absoluteFilename, DEFAULT_READER_WRITER_TYPE, DEFAULT_SERIALIZER,
				DEFAULT_BUFFER_SIZE);
	}
	public DBWriter(String absoluteFilename, ReaderWriterType rwType)	{
		this(absoluteFilename, rwType, DEFAULT_SERIALIZER, DEFAULT_BUFFER_SIZE);
	}
	public DBWriter(String absoluteFilename, ReaderWriterType rwType, 
			SerializerType serializerType)	{
		this(absoluteFilename, rwType, serializerType, DEFAULT_BUFFER_SIZE);
	}
	
	public DBWriter(String absoluteFilename, ReaderWriterType rwType, 
			SerializerType serializerType, int bufferSize)	{
		this.absoluteFilename = absoluteFilename;
		this.rwType = rwType;
		this.serializerType = serializerType;
		this.bufferSize = bufferSize;
		this.deSerializer = SerializerType.getSerializer(serializerType, 1024 * 10);
	}
	
	public void write(DataEvent event) throws IOException {
		if(writer == null) setReaderWriter();
		if(reader != null) readEventsToWriter(event);
		if(!isDuplicateEvent(event)) {
			writeEvent(event);
		}
	}
	
	public void close() throws IOException {
		if(writer != null) {
			if(reader != null) {
				byte[] readEvent = null;
				while((readEvent = reader.readSingleEvent()) != null)
				{
					writer.write(readEvent);
				}
				reader.close();
				reader = null;
				lastReadEvent = null;
			}
			writer.close();
			writer = null;
			equalTimeEvents.clear();
			lastWrittenEvent = null;
			renameTempFile();
		}
	}

	
	private void writeEvent(DataEvent event) throws IOException {
		if(lastWrittenEvent != null && event.receiveTime < lastWrittenEvent.receiveTime) {
			close();
			write(event);
		} else {
			lastWrittenEvent = event;
			writer.write(event);
		}
	}
	
	private boolean isDuplicateEvent(DataEvent event) {
		for(DataEvent otherEvent: equalTimeEvents) {
			if(otherEvent.receiveTime != event.receiveTime){
				equalTimeEvents.clear();
				equalTimeEvents.add(event);
				return false;
			}
			else if(otherEvent.equals(event)) return true;
		}
		equalTimeEvents.add(event);
		return false;
	}
	
	private void readEventsToWriter(DataEvent event) throws IOException {
		lastReadEvent = (lastReadEvent != null) ? lastReadEvent :  readEvent();
		while(lastReadEvent != null && lastReadEvent.receiveTime < event.receiveTime) {
			writer.write(lastReadEvent);
			lastWrittenEvent = lastReadEvent;
			lastReadEvent = readEvent();
		}
		if(lastReadEvent != null) {
			while(lastReadEvent != null && lastReadEvent.receiveTime == event.receiveTime) {
				addEqualTimeEventIfNotDuplicate(lastReadEvent);
				writer.write(lastReadEvent);
				lastWrittenEvent = lastReadEvent;
				lastReadEvent = readEvent();
			}
		}
		if(lastReadEvent == null) {
			reader.close();
			reader = null;
			FileUtilities.delete(absoluteFilename);
		}
	}
	
	private void addEqualTimeEventIfNotDuplicate(DataEvent event) {
		DataEvent timeCheck = (equalTimeEvents.size() > 0) ? equalTimeEvents.get(0): null;
		if(timeCheck == null) equalTimeEvents.add(event);
		else if(timeCheck.receiveTime != event.receiveTime) {
			equalTimeEvents.clear();
			equalTimeEvents.add(event);
		} else if(!equalTimeEvents.contains(lastReadEvent)) equalTimeEvents.add(event);
	}
	
	private DataEvent readEvent() throws IOException {
		byte[] readData = reader.readSingleEvent();
		if(readData != null) {
			deSerializer.clearAndPutWrittenBytes(readData);
			return EventDeserializer.deserializeEvent(deSerializer);
		} else {
			return null;
		}
	}
	private void setReaderWriter() throws IOException {
		if(fileExists(absoluteFilename) && reader == null) {
			reader = ReaderWriterType.getByteReader(rwType, absoluteFilename,
					bufferSize);
		}
		writer = ReaderWriterType.getByteWriter(rwType, getTempFilename(absoluteFilename),
				serializerType, bufferSize);
	}

	private boolean fileExists(String filename) {
		return (new File(filename)).exists();
	}
	
	private String getTempFilename(String filename) {
		int lastSlash = filename.lastIndexOf("/");
		String basePath = filename.substring(0, lastSlash + 1);
		String relativeFilename = filename.substring(lastSlash + 1);
		String out =  basePath.concat("TEMP").concat(relativeFilename);
		return out;
	}
	
	private void renameTempFile() throws IOException {
		String tempName = getTempFilename(absoluteFilename);
		File srcFile = new File(tempName);
	    boolean bSucceeded = false;
	    try {
	        File destFile = new File(absoluteFilename);
	        if (destFile.exists()) {
	            if (!destFile.delete()) {
	                throw new IOException(tempName + " was not successfully renamed to " + absoluteFilename); 
	            }
	        }
	        if (!srcFile.renameTo(destFile))        {
	            throw new IOException(tempName + " was not successfully renamed to " + absoluteFilename);
	        } else {
	                bSucceeded = true;
	        }
	    } finally {
	          if (bSucceeded) {
	                srcFile.delete();
	          }
	    }
	}
	
}
