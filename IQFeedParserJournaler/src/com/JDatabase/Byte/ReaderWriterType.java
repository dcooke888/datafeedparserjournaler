package com.JDatabase.Byte;

import java.io.IOException;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import com.JDatabase.Byte.Reader.ByteReader;
import com.JDatabase.Byte.Reader.FileStreamReader;
import com.JDatabase.Byte.Reader.LZ4StreamReader;
import com.JDatabase.Byte.Reader.SnappyStreamReader;
import com.JDatabase.Byte.Writer.ByteWriter;
import com.JDatabase.Byte.Writer.FileStreamWriter;
import com.JDatabase.Byte.Writer.LZ4StreamWriter;
import com.JDatabase.Byte.Writer.SnappyStreamWriter;
import com.JDatabase.Serialize.SerializerType;

public enum ReaderWriterType {
	UNKNOWN((byte) 0),
	FILESTREAM((byte) 1),
	LZ4((byte) 2),
	SNAPPY((byte) 3);
	
	private byte val;

	private ReaderWriterType(byte val) {
		this.val = val;
	}
	
	private static final Map<Byte,ReaderWriterType> lookupByte = new HashMap<Byte,ReaderWriterType>();

	static {
	    for(ReaderWriterType s : EnumSet.allOf(ReaderWriterType.class))
	    {
	        lookupByte.put(s.getByteId(), s);
	    }
	}
	
	public byte getByteId()
	{
		return this.val;
	}

	public static ReaderWriterType fromByteId(byte b)
	{
		ReaderWriterType type = lookupByte.get(b);
		return (type == null) ? UNKNOWN : type;
	}
	
	public static final ByteReader getByteReader(ReaderWriterType rwType, String filename, int bufferSize) throws IOException {
		switch(rwType) {
		case FILESTREAM:
			return new FileStreamReader(filename, bufferSize);
		case LZ4:
			return new LZ4StreamReader(filename, bufferSize);
		case SNAPPY:
			return new SnappyStreamReader(filename, bufferSize);
		default:
			return null;
		}
	}
	
	public static final ByteWriter getByteWriter(ReaderWriterType rwType, String filename, SerializerType serializerType, int bufferSize) {
		switch(rwType) {
		case FILESTREAM:
			return new FileStreamWriter(filename, SerializerType.getSerializer(serializerType, bufferSize));
		case LZ4:
			return new LZ4StreamWriter(filename, SerializerType.getSerializer(serializerType, bufferSize));
		case SNAPPY:
			return new SnappyStreamWriter(filename, SerializerType.getSerializer(serializerType, bufferSize));
		case UNKNOWN:
		default:
			return null;
		}
	}

}
