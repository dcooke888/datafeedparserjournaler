package com.JDatabase.Byte.Reader;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class FileStreamReader extends ByteReader{

	public FileStreamReader(String filename, int bufferSize) {
		super(filename, bufferSize);
	}

	@Override
	void setInputStream() throws IOException {
		reader = new BufferedInputStream(new FileInputStream(getFilename()));
	}

}
