package com.JDatabase.Byte.Reader;

import java.io.IOException;
import java.io.InputStream;

public abstract class ByteReader {
	private final String filename;
	private final byte[] buffer;
	protected InputStream reader;
	
	public ByteReader(String filename, int bufferSize){
		this.filename = filename;
		this.buffer = new byte[bufferSize];
	}
	
	protected String getFilename() {
		return filename;
	}
	
	abstract void setInputStream() throws IOException;
	
	public int getBufferSize() {
		return buffer.length;
	}
	
	public boolean hasRemaining() throws IOException {
		return remaining() > 0;
	}

	public int remaining() throws IOException {
		checkReader();
		return reader.available();
	}

	private void checkReader() throws IOException {
		if(reader == null) setInputStream();
	}
	
	public byte[] readSingleEvent() throws IOException {
		checkReader();
		int length = reader.read();
		if(length == -1) return null;
		byte[] eventBytes = new byte[length];
		byte[] readBytes = read(eventBytes);
		if(readBytes.length == length) {
			return readBytes;
		} else {
			return null;
		}
	}
	
	private byte[] read(byte[] toRead) throws IOException {
		int totalRead = 0;
		int lastRead = 0;
		
		while(lastRead >= 0 && totalRead < toRead.length) {
			lastRead = reader.read(toRead, totalRead, toRead.length - totalRead);
			if(lastRead > 0) totalRead += lastRead;
		} 
		
		if(totalRead <= 0) {
			return null;
		} else if(totalRead == toRead.length) {
			return toRead;
		} else {
			byte[] out = new byte[totalRead];
			System.arraycopy(toRead, 0, out, 0, totalRead);
			return out;
		}
	}

	public byte[] readToBuffer() throws IOException {
		checkReader();
		return read(buffer);
	}
	
	public void close() throws IOException {
		if(reader != null) reader.close();
		reader = null;
	}
}
