package com.JDatabase.Byte.Reader;

import java.io.FileInputStream;
import java.io.IOException;

import org.xerial.snappy.SnappyInputStream;


public class SnappyStreamReader extends ByteReader{

	public SnappyStreamReader(String filename, int bufferSize){
		super(filename, bufferSize);
	}

	@Override
	void setInputStream() throws IOException {
		reader = new SnappyInputStream(new FileInputStream(getFilename()));
	}

}
