package com.JDatabase.Byte.Reader;

import java.io.FileInputStream;
import java.io.IOException;

import net.jpountz.lz4.LZ4BlockInputStream;

public class LZ4StreamReader extends ByteReader{

	public LZ4StreamReader(String filename, int bufferSize){
		super(filename, bufferSize);
	}

	@Override
	void setInputStream() throws IOException {
		reader = new LZ4BlockInputStream(new FileInputStream(getFilename()));
	}

}
