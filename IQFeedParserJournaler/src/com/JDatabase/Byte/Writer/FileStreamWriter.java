package com.JDatabase.Byte.Writer;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import com.JDatabase.Serialize.Serializer;

public class FileStreamWriter extends ByteWriter{

	public  FileStreamWriter(String filename, Serializer serializer)
	{
		super(filename, serializer);
	}

	@Override
	void setOutputStream() throws IOException {
		stream = new BufferedOutputStream(new FileOutputStream(getFilename()));
	}
}
