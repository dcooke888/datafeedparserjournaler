package com.JDatabase.Byte.Writer;

import java.io.IOException;
import java.io.OutputStream;
import java.util.TimeZone;

import com.JDatabase.Events.DataEvent;
import com.JDatabase.IQFeed.Feed;
import com.JDatabase.Serialize.Serializer;

public abstract class ByteWriter {
	
	private final String filename;
	private final Serializer serializer;
	
	protected OutputStream stream;

	public ByteWriter(String filename, Serializer serializer)
	{
		this.filename = filename;
		this.serializer = serializer;
	}
	
	public String getFilename() {
		return filename;
	}
	
	abstract void setOutputStream() throws IOException;
	
	public void write(Feed feed, TimeZone t, long receiveTime, byte[] message) throws IOException {
		serializer.putByte(feed.getId());
		serializer.putString(t.getID());
		serializer.putLong(receiveTime);
		serializer.putByteArray(message);
		write(serializer.getWrittenBytes());
	}

	public int write(DataEvent event) throws IOException {
		event.write(serializer);
		byte[] out = serializer.getWrittenBytes();
		write(out);
		return out.length;
	}
	private void checkStream() throws IOException {
		if(stream == null) setOutputStream();
	}
	
	public void write(byte[] toWrite) throws IOException {
		checkStream();
		stream.write(toWrite.length);
		stream.write(toWrite);
//		serializer.putByteArray(toWrite);
//		byte[] out = serializer.getWrittenBytes();
//		stream.write(out);
	}
	
	public void writeBytesToSerializer(byte[] toWrite) throws  IOException {
		checkStream();
		serializer.putByteArray(toWrite);
		byte[] out = serializer.getWrittenBytes();
		stream.write(out);
	}
	
	public void flush() throws IOException {
		checkStream();
		stream.flush();
		
	}

	public void close() throws IOException {
		checkStream();
		stream.close();
		stream = null;
	}


}
