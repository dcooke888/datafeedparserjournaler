package com.JDatabase.Byte.Writer;

import java.io.FileOutputStream;
import java.io.IOException;
import org.xerial.snappy.SnappyOutputStream;

import com.JDatabase.Serialize.Serializer;

public class SnappyStreamWriter extends ByteWriter{

	public  SnappyStreamWriter(String filename, Serializer serializer)
	{
		super(filename, serializer);
	}

	@Override
	void setOutputStream() throws IOException {
		stream = new SnappyOutputStream(new FileOutputStream(getFilename()));
	}

}
