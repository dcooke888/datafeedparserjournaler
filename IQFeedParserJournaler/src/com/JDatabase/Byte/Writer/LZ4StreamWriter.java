package com.JDatabase.Byte.Writer;

import java.io.FileOutputStream;
import java.io.IOException;
import net.jpountz.lz4.LZ4BlockOutputStream;

import com.JDatabase.Serialize.Serializer;

public class LZ4StreamWriter extends ByteWriter{

	public  LZ4StreamWriter(String filename, Serializer serializer)
	{
		super(filename, serializer);
	}

	@Override
	void setOutputStream() throws IOException {
		stream = new LZ4BlockOutputStream(new FileOutputStream(getFilename()));
	}

}
