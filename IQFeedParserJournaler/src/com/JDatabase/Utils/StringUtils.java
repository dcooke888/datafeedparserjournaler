package com.JDatabase.Utils;

public class StringUtils {
	public static int MAX_DELIMETERS = 100;
	
	public static String getFirstCommaSplitString(String s)
	{
		int pos = s.indexOf(',');
		return (pos < 0) ? s : s.substring(0, pos);
	}
	
	public static String[] getCommaSplitString(String s) {
		return getCharSplitString(s, ',');
	}
	
	public static String[] getCharSplitString(String s, char delimiter) {
		if(s == null) return null;
		int count = 0;
		int[] positions = new int[MAX_DELIMETERS];
		int pos = s.indexOf(delimiter);
		while(pos >= 0) {
			count++;
			positions = (count > positions.length) ? doubleIntArraySize(positions) : positions;
			positions[count - 1 ] = pos;
			pos = s.indexOf(delimiter, pos + 1); 
		}
		return getStringSubstrings(s, positions, count);
	}

	private static String[] getStringSubstrings(String s, int[] positions, int count) {
		if(count == 0){
			String[] out = new String[1];
			out[0] = s;
			return out;
		} else {
			String[] out;
			boolean lastValue;
			if(positions[count - 1] + 1 >= s.length())
			{
				out = new String[count]; 
				lastValue = false;
			} 
			else 
			{
				out = new String[count + 1];
				lastValue = true;
			}
			int last = 0;
			for(int i = 0; i < count; i++)
			{
				out[i] = s.substring(last,positions[i]);
				last = positions[i]+1;
			}
			if(lastValue) out[count] = s.substring(last);
			return out;
		}

	}

	private static int[] doubleIntArraySize(int[] positions) {
		int[] out = new int[positions.length * 2];
		System.arraycopy(positions, 0, out, 0, positions.length);
		return out;
	}
	
	public static double getDoubleFromString(String in, double defaultValue)
	{
		if(in.length() == 0) return defaultValue;
		try
		{
			return Double.valueOf(in);
		} 
		catch(NumberFormatException e)
		{
			return defaultValue;
		}
	}
	
	public static int getIntFromString(String in, int defaultValue)
	{
		if(in.length() == 0) return defaultValue;
		try
		{
			return Integer.valueOf(in);
		} 
		catch(NumberFormatException e)
		{
			return defaultValue;
		}
	}
}
