package com.JDatabase.Utils;

import java.util.TimeZone;

public class EqualityCheck {

	public static final boolean isDoubleEqual(double num1, double num2, int numDigits)
	{
		int multiplier = (int) Math.pow(10, numDigits);
		int num1Round = (int) Math.round(num1 * multiplier);
		int num2Round = (int) Math.round(num2 * multiplier);
		return num1Round == num2Round;
	}
	
	public static final boolean isTimeZoneEqual(TimeZone tz1, TimeZone tz2)
	{
		long dateTest = 0;
		return tz2.getOffset(dateTest) == tz2.getOffset(dateTest);
	}
}
