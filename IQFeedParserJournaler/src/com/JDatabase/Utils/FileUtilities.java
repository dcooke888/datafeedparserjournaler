package com.JDatabase.Utils;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

public class FileUtilities {

	public static String[] getAbsoluteFileNamesInFolder(String folder)
	{
	  	File[] files = new File(folder).listFiles();
	  	return convertFilestoFilenames(files);
	}
	
	public static String[] getAbsoluteFileNamesOfType(String folder, final String fileType)
	{
		
	  	  File[] files = new File(folder).listFiles(new FilenameFilter() {
	  	    public boolean accept(File dir, String name) {
	  	        return name.toLowerCase().endsWith(fileType);
	  	    }
	  	});
	  	return convertFilestoFilenames(files);
	}
	
	private static String[] convertFilestoFilenames(File[] files) {
		if(files == null) return null;
		String[] fileNames = new String[files.length];
		for (int i = 0, n = files.length; i < n; i++) fileNames[i] = files[i].getAbsolutePath();
		return fileNames;
	}
	
	public static String getRelativeFilename(String absoluteFilename) {
		int index = absoluteFilename.lastIndexOf("/");
		return (index == -1 || (absoluteFilename.length() == index + 1)) ? absoluteFilename : absoluteFilename.substring(index + 1);
	}
	
	public static void delete(String pathToDelete) {
		File directory = new File(pathToDelete);
		if(directory.exists()) {
           try{
               delete(directory);
           } catch(IOException e){
               e.printStackTrace();
           }
		}
	}
	
	public static void delete(File file) throws IOException {
	 
	    	if(file.isDirectory()){
	 
	    		//directory is empty, then delete it
	    		if(file.list().length==0) {
	    			file.delete();
	    		} else {
	 
	    		   //list all the directory contents
	        	   String files[] = file.list();
	 
	        	   for (String temp : files) {
	        	      //construct the file structure
	        	      File fileDelete = new File(file, temp);
	 
	        	      //recursive delete
	        	     delete(fileDelete);
	        	   }
	 
	        	   //check the directory again, if empty then delete it
	        	   if(file.list().length==0){
	           	     file.delete();
	        	   }
	    		}
	 
	    	}else{
	    		//if file, then delete it
	    		file.delete();
	    	}
	    }
	
}
