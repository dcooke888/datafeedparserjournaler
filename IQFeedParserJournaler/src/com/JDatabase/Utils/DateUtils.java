package com.JDatabase.Utils;

import java.text.ParseException;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateUtils {
	public static final long millisPerDay = 24 * 60 * 60 * 1000;
	public static final DateTimeZone est = DateTimeZone.forID("America/New_York");
	public static final DateTimeZone centralTime = DateTimeZone.forID("America/Chicago");
	public static final DateTimeFormatter dateFmt = DateTimeFormat.forPattern("MM/dd/yyyy").withZone(est);
	public static final DateTimeFormatter L1DateTimeFmt = DateTimeFormat.forPattern("MM/dd/yyyyHH:mm:ss.SSS").withZone(est);
	public static final DateTimeFormatter L2DateTimeFmt = DateTimeFormat.forPattern("yyyy-MM-ddHH:mm:ss.SSS").withZone(est);
	public static final DateTimeFormatter OLD_L2DateTimeFmt = DateTimeFormat.forPattern("yyyy-MM-ddHH:mm:ss").withZone(est);
	
	
	public static long getStartESTDateValue(int year, int month, int day) {
		return getESTDateValue(year, month, day, 0, 0, 0, 0);
	}
	public static long getEndESTDateValue(int year, int month, int day) {
		return getESTDateValue(year, month, day, 23, 59, 59, 999);
	}
	public static long getStartESTDateValue(int year, int month, int day, int hr, int min, int sec) {
		return getESTDateValue(year, month, day, hr, min, sec, 0);
	}
	public static long getEndESTDateValue(int year, int month, int day, int hr, int min, int sec) {
		return getESTDateValue(year, month, day, hr, min, sec, 999);
	}
	public static long getESTDateValue(int year, int month, int day, int hr, int min, int sec, int millis){
		return getDateValue(est, year, month, day, hr, min, sec, millis);
	}
	public static long getDateValue(DateTimeZone timeZone, int year, int month, int day, int hr, int min, int sec, int millis){
		DateTime out = new DateTime(year, month, day, hr, min, sec, millis, timeZone);
		return out.getMillis();
	}
	
	public static String getESTDateString(long time) {
		return getDateString(est, time);
	}
	
	public static String getDateString(DateTimeZone timeZone, long time) {
		DateTime dateTime = new DateTime(time, timeZone);
		StringBuilder out = new StringBuilder();
		out.append(dateTime.getYear()).append("_");
		out.append(dateTime.getMonthOfYear()).append("_");
		out.append(dateTime.getDayOfMonth());
		return out.toString();
	}
	
	public static long getFilenameStartTime(String filename) {
		String relativeFilename = FileUtilities.getRelativeFilename(filename);
		String[] dateBreakdown = StringUtils.getCharSplitString(relativeFilename, '_');
		if(dateBreakdown.length >= 3){
			int yr = Integer.valueOf(dateBreakdown[0]);
			int mo = Integer.valueOf(dateBreakdown[1]);
			int day = Integer.valueOf(dateBreakdown[2]);
			return DateUtils.getStartESTDateValue(yr, mo, day);
		} else return Long.MIN_VALUE;
	}

	public static long getFileNameEndTime(String filename) {
		String relativeFilename = FileUtilities.getRelativeFilename(filename);
		String[] dateBreakdown = StringUtils.getCharSplitString(relativeFilename, '_');
		if(dateBreakdown.length >= 3){
			int yr = Integer.valueOf(dateBreakdown[0]);
			int mo = Integer.valueOf(dateBreakdown[1]);
			int day = Integer.valueOf(dateBreakdown[2]);
			return DateUtils.getEndESTDateValue(yr, mo, day);
		} else return Long.MIN_VALUE;
	}
	
	public static long getL1EstimatedUTCTime(long receiveTime, String iqFeedDate, String iqFeedTime) throws ParseException {
		if(iqFeedTime.equals("")) throw new ParseException("IQFeedDate or Time incorrectly formatted",1);
		if(iqFeedDate.equals("")) return getL1EstimatedUTCTime(receiveTime, iqFeedTime);
		DateTime parsedDateTime = L1DateTimeFmt.parseDateTime(iqFeedDate.concat(iqFeedTime));
		return parsedDateTime.getMillis();
	}
	
	public static long getL1EstimatedUTCTime(long receiveTime, String iqFeedTime) throws ParseException {
		if(iqFeedTime.equals("")) throw new ParseException("IQFeedTime Incorrectly formatted",1);
		DateTime receiveDateTime = new DateTime(receiveTime, est);
		DateTime parseDateTime = L1DateTimeFmt.parseDateTime(receiveDateTime.toString(dateFmt).concat(iqFeedTime));
		long correctedDay = getCorrectDay(receiveTime, parseDateTime);
		return correctedDay;
	}
	
	public static long getL2EstimatedUTCTime(long receiveTime, String iqFeedDate, String iqFeedTime){
		DateTime parsedDateTime = L2DateTimeFmt.parseDateTime(iqFeedDate.concat(iqFeedTime));
		return parsedDateTime.getMillis();
	}
	
	public static long getOLDL2EstimatedUTCTime(long receiveTime, String iqFeedDate, String iqFeedTime){
		DateTime parsedDateTime = OLD_L2DateTimeFmt.parseDateTime(iqFeedDate.concat(iqFeedTime));
		return parsedDateTime.getMillis();
	}
	
//	private static long getCorrectTimeZone(long receiveTime, long correctedDay) throws ParseException {
//		double hrDiff = ((double)receiveTime - correctedDay) / (1000 * 60 * 60);
//		if(hrDiff >= -0.5 && hrDiff <= 0.5) return correctedDay;
//		else if(hrDiff > -1.5 && hrDiff < -0.5) return new DateTime(correctedDay).minusHours(1).getMillis();
//		else throw new ParseException("IQFeed Date Offset is incorrect", (int) hrDiff * 10);
//	}
//	
	private static long getCorrectDay(long receiveTime, DateTime parseDateTime) {
		int hrDiff = (int) (Math.abs(receiveTime - parseDateTime.getMillis()) / (1000 * 60 * 60));
		return (hrDiff > 3) ? parseDateTime.minusDays(1).getMillis() : parseDateTime.getMillis();
	}
	
	public static boolean doRangesOverlap(long start1, long end1, long start2, long end2) {
		return (start1 > end2 || start2 > end1) ? false : true;
	}
}
