package com.JDatabase.Events;

import java.text.ParseException;
import java.util.TimeZone;

import com.JDatabase.Serialize.Serializer;
import com.JDatabase.Utils.DateUtils;
import com.JDatabase.Utils.EqualityCheck;

public class TradeEvent extends DataEvent{

	public final String symbol;
	public final String tradeType;
	public final String tradeConditions;
	public final String exchangeId;
	public final String lastMktCenter;
	public final long feedTime;
	public final double lastPrice;
	public final int lastSize;
	public final int tickId;
	public final int openInterest;
	public final double vwap;
	
	public TradeEvent(TimeZone timeZone, long receiveTime, String symbol, String tradeType, String tradeConditions, String exchangeId, 
			String lastMktCenter, String lastUpdateDate, String lastUpdateTime, double lastPrice, int lastSize,
			int tickId, int openInterest, double vwap) throws ParseException
	{
		this(timeZone, receiveTime, symbol, tradeType, tradeConditions, exchangeId, lastMktCenter,
				DateUtils.getL1EstimatedUTCTime(receiveTime, lastUpdateDate, lastUpdateTime), lastPrice, 
				lastSize, tickId, openInterest, vwap);
	}

	public TradeEvent(TimeZone timeZone, long receiveTime, String symbol, String tradeType, String tradeConditions, String exchangeId, 
			String lastMktCenter, long feedTime, double lastPrice, int lastSize,
			int tickId, int openInterest, double vwap) {
		super(timeZone, receiveTime, EventType.TRADE);
		this.symbol = symbol;
		this.tradeType = tradeType;
		this.tradeConditions = tradeConditions;
		this.exchangeId = exchangeId;
		this.lastMktCenter = lastMktCenter;
		this.feedTime = feedTime;
		this.lastPrice = lastPrice;
		this.lastSize = lastSize;
		this.tickId = tickId;
		this.openInterest = openInterest;
		this.vwap = vwap;
	}
	
	@Override
	public String toString() {
		StringBuilder out = new StringBuilder();
		out.append(String.format("%s,%d,%s",eventType.toString(), receiveTime, timeZone.getID()));
		out.append(String.format(",%s,%s,%s,%s,%s",symbol, tradeType, tradeConditions,exchangeId,lastMktCenter));
		out.append(String.format(",%d,%.2f,%d,%d,%d,%.2f",feedTime, lastPrice, lastSize, tickId, openInterest, vwap));
		return out.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public TradeEvent read(Serializer serializer, EventType eventType)
	{
		TimeZone timeZone = TimeZone.getTimeZone(serializer.getString());
		long receiveTime = serializer.getLong();
		String symbol = serializer.getString();
		String tradeType = serializer.getString();
		String tradeConditions = serializer.getString();
		String exchangeId = serializer.getString();
		String lastMktCenter = serializer.getString();
		long feedTime = serializer.getLong();
		double lastPrice = serializer.getDouble();
		int lastSize = serializer.getInt();
		int tickId = serializer.getInt();
		int openInterest = serializer.getInt();
		double vwap = serializer.getDouble();
		return new TradeEvent(timeZone, receiveTime, symbol, tradeType, tradeConditions,
				exchangeId, lastMktCenter, feedTime, lastPrice, lastSize, tickId, openInterest, vwap);
	}
	
	@Override
	public Serializer write(Serializer serializer)
	{
		serializer.putByte(eventType.getByteId());
		serializer.putString(timeZone.getID());
		serializer.putLong(receiveTime);
		serializer.putString(symbol);
		serializer.putString(tradeType);
		serializer.putString(tradeConditions);
		serializer.putString(exchangeId);
		serializer.putString(lastMktCenter);
		serializer.putLong(feedTime);
		serializer.putDouble(lastPrice);
		serializer.putInt(lastSize);
		serializer.putInt(tickId);
		serializer.putInt(openInterest);
		serializer.putDouble(vwap);
		return serializer;
	}
	
	
	@Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
 
        final TradeEvent that = (TradeEvent) o;
        
        if(!EqualityCheck.isTimeZoneEqual(timeZone, that.timeZone))
        {
        	return false;
        }
        if (receiveTime != that.receiveTime)
        {
            return false;
        }
        if (!symbol.equals(that.symbol))
        {
            return false;
        }
        if(!tradeType.equals(that.tradeType))
        {
        	return false;
        }
    	if(!tradeConditions.equals(that.tradeConditions))
    	{
    		return false;
    	}
    	if(!exchangeId.equals(that.exchangeId))
    	{
    		return false;
    	}
    	if(!lastMktCenter.equals(that.lastMktCenter))
    	{
    		return false;
    	}
    	if(feedTime != that.feedTime)
    	{
    		return false;
    	}
    	if(!EqualityCheck.isDoubleEqual(lastPrice, that.lastPrice, 6))
    	{
    		return false;
    	}
    	if(lastSize != that.lastSize)
    	{
    		return false;
    	}
    	if(tickId != that.tickId)
    	{
    		return false;
    	}
    	if(openInterest != that.openInterest)
    	{
    		return false;
    	}
    	if(!EqualityCheck.isDoubleEqual(vwap, that.vwap, 6))
    	{
    		return false;
    	}
    	return true;
    }
}
