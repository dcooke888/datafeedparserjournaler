package com.JDatabase.Events;

import java.util.TimeZone;

import com.JDatabase.IQFeed.Feed;
import com.JDatabase.Serialize.Serializer;
import com.JDatabase.Utils.EqualityCheck;
import com.JDatabase.Utils.StringUtils;

public class SymbolNotFoundEvent extends DataEvent{
	
	public final Feed feed;
	public final String symbol; 
	
	public SymbolNotFoundEvent(Feed feed, TimeZone timeZone, long receiveTime, String symbol)
	{
		super(timeZone, receiveTime, EventType.SYMBOL_NOT_FOUND);
		this.feed = feed;
		this.symbol = symbol;
	}
	
	public static DataEvent[] getSymbolNotFoundEvent(Feed feed, TimeZone timeZone, long receiveTime, String message)
	{
		String[] tokens = StringUtils.getCommaSplitString(message);
		if(tokens.length == 2)
		{
			DataEvent[] out = new DataEvent[1];
			out[0] = new SymbolNotFoundEvent(feed, timeZone, receiveTime, tokens[1]);
			return out;
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public SymbolNotFoundEvent read(Serializer serializer, EventType eventType) {
		TimeZone timeZone = TimeZone.getTimeZone(serializer.getString());
		long receiveTime = serializer.getLong();
		Feed feed = Feed.fromByteId(serializer.getByte());
		String symbol = serializer.getString();
		return new SymbolNotFoundEvent(feed, timeZone, receiveTime, symbol);
	}
	
	@Override
	public Serializer write(Serializer serializer) {
		serializer.putByte(eventType.getByteId());
		serializer.putString(timeZone.getID());
		serializer.putLong(receiveTime);
		serializer.putByte(feed.getId());
		serializer.putString(symbol);
		return serializer;
	}
	@Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
 
        final SymbolNotFoundEvent that = (SymbolNotFoundEvent) o;
        
        if(!EqualityCheck.isTimeZoneEqual(timeZone, that.timeZone))
        {
        	return false;
        }
        if (receiveTime != that.receiveTime)
        {
            return false;
        }
        if (eventType != that.eventType)
        {
            return false;
        }
        if (!symbol.equals(that.symbol))
        {
            return false;
        }
        if(feed != that.feed)
        {
        	return false;
        }
    	return true;
    }

}
