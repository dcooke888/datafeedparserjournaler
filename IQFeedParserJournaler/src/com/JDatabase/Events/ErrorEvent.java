package com.JDatabase.Events;

import java.util.TimeZone;

import com.JDatabase.Serialize.Serializer;
import com.JDatabase.Utils.EqualityCheck;

public class ErrorEvent extends DataEvent{

	public final String errorMsg;
	
	public ErrorEvent(TimeZone timeZone, long receiveTime, String errorMsg)
	{
		super(timeZone, receiveTime, EventType.ERROR);
		this.errorMsg = errorMsg;
	}
	
	public static DataEvent[] getErrorMessage(TimeZone timeZone, long receiveTime, String message)
	{
		int commaIndex = message.indexOf(",");
		if(commaIndex > 0 && message.length() > commaIndex + 2)
		{
			DataEvent[] out = new DataEvent[1];
			out[0] = new ErrorEvent(timeZone, receiveTime, message.substring(commaIndex + 1));
			return out;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ErrorEvent read(Serializer serializer, EventType eventType) {
			TimeZone timeZone = TimeZone.getTimeZone(serializer.getString());
			long receiveTime = serializer.getLong();
			String message = serializer.getString();
			return new ErrorEvent(timeZone, receiveTime, message);
	}

	@Override
	public Serializer write(Serializer serializer) {
		serializer.putByte(eventType.getByteId());
		serializer.putString(timeZone.getID());
		serializer.putLong(receiveTime);
		serializer.putString(errorMsg);
		return serializer;
	}
	
	@Override
	public boolean equals(Object o)
	{
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
 
        final ErrorEvent that = (ErrorEvent)o;
        
        if(!EqualityCheck.isTimeZoneEqual(timeZone, that.timeZone))
        {
        	return false;
        }
        if (receiveTime != that.receiveTime)
        {
            return false;
        }
        if (!errorMsg.equals(that.errorMsg))
        {
            return false;
        }
    	return true;
    }

}
