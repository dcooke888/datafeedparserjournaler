package com.JDatabase.Events;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum TicType {
	UNKNOWN((byte) 0, "UNKNOWN"),
	TRADE((byte) 1, "TRADE"),
	TOB((byte) 2, "TOB"),
	DEPTH((byte) 3, "DEPTH"),
	NEWS((byte) 4, "NEWS");
	
	private TicType(byte val, String descriptor)
	{
		this.val = val;
		this.descriptor = descriptor;
	}
	
	private final byte val;
	private final String descriptor;
	
	@Override
	public String toString() {
		return descriptor;
	}
	
	public static final TicType getTicType(EventType eventType) {
		switch(eventType) {
		case TRADE:
			return TicType.TRADE;
		case ASK:
		case BID:
		case REGIONAL_ASK:
		case REGIONAL_BID:
			return TicType.TOB;
		case ASK_DEPTH:
		case BID_DEPTH:
			return TicType.DEPTH;
		case NEWS:
			return TicType.NEWS;
		case CONNECTION_STATUS:
		case ERROR:
		case FUNDAMENTAL:
		case SYMBOL_NOT_FOUND:
		case SYSTEM_MESSAGE:
		case UNKNOWN:
		default:
			return TicType.UNKNOWN;
		}
	}
	
	private static final Map<Byte,TicType> lookupByte = new HashMap<Byte,TicType>();
	private static final Map<String,TicType> lookupString = new HashMap<String,TicType>();

	static {
	    for(TicType s : EnumSet.allOf(TicType.class))
	    {
	        lookupByte.put(s.getByteId(), s);
	        lookupString.put(s.descriptor, s);
	    }
	}
	
	public byte getByteId()
	{
		return this.val;
	}

	public static TicType fromByteId(byte b)
	{
		TicType type = lookupByte.get(b);
		return (type == null) ? UNKNOWN : type;
	}

	public static TicType fromString(String string) {
		TicType type = lookupString.get(string);
		return (type == null) ? UNKNOWN : type;
	}
}
