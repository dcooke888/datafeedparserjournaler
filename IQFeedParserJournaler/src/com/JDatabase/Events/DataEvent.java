package com.JDatabase.Events;

import java.util.TimeZone;

import com.JDatabase.Serialize.Serializer;

public abstract class DataEvent implements Comparable<DataEvent>{
	
	public final EventType  eventType;
	public final TimeZone timeZone;
	public final long receiveTime;
	
	public DataEvent(TimeZone timeZone, long receiveTime, EventType eventType) {
		this.eventType = eventType;
		this.timeZone = timeZone;
		this.receiveTime = receiveTime;
	}
	
	public abstract <T extends DataEvent> T read(Serializer serializer, EventType eventType);
	public abstract Serializer write(Serializer serializer);
	
	@Override
	public String toString(){
		return String.format("%s,%d,%s",eventType.toString(), receiveTime, timeZone.toString());
	}
	
	@Override
	public int compareTo(DataEvent other) {
		if(this.receiveTime < other.receiveTime) return -1;
		else if(this.receiveTime > other.receiveTime) return 1;
		return 0;
	}

}
