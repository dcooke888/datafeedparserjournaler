package com.JDatabase.Events;

import java.util.TimeZone;

import com.JDatabase.Events.DataEvent;
import com.JDatabase.Events.EventType;
import com.JDatabase.Serialize.Serializer;
import com.JDatabase.Utils.EqualityCheck;

public class NewsEvent extends DataEvent{
	public final String distributorCode;
	public final String storyId;
	public final String symbolList;
	public final String dateTime;
	public final String headline;
	
	public NewsEvent(TimeZone timeZone, long receiveTime, String distributorCode, String storyId, String symbolList, String dateTime, String headLine)
	{
		super(timeZone, receiveTime, EventType.NEWS);
		this.distributorCode = distributorCode;
		this.storyId = storyId;
		this.symbolList = symbolList;
		this.dateTime = dateTime;
		this.headline = headLine;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public NewsEvent read(Serializer serializer, EventType eventType) {
		TimeZone timeZone = TimeZone.getTimeZone(serializer.getString());
		long receiveTime = serializer.getLong();
		String distributorCode = serializer.getString();
		String storyId = serializer.getString();
		String symbolList = serializer.getString();
		String dateTime = serializer.getString();
		String headline = serializer.getString();
		return new NewsEvent(timeZone, receiveTime, distributorCode, storyId, symbolList, dateTime, headline);
	}
	
	@Override
	public Serializer write(Serializer serializer) {
		serializer.putByte(eventType.getByteId());
		serializer.putString(timeZone.getID());
		serializer.putLong(receiveTime);
		serializer.putString(distributorCode);
		serializer.putString(storyId);
		serializer.putString(symbolList);
		serializer.putString(dateTime);
		serializer.putString(headline);
		return serializer;
	}
	
	
	@Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
 
        final NewsEvent that = (NewsEvent) o;
        
        if(!EqualityCheck.isTimeZoneEqual(timeZone, that.timeZone))
        {
        	return false;
        }
        if (receiveTime != that.receiveTime)
        {
            return false;
        }
        if (eventType != that.eventType)
        {
            return false;
        }
        if (!distributorCode.equals(that.distributorCode))
        {
            return false;
        }
        if(!storyId.equals(that.storyId))
        {
        	return false;
        }
    	if(!symbolList.equals(that.symbolList))
    	{
    		return false;
    	}
    	if(!dateTime.equals(that.dateTime))
    	{
    		return false;
    	}
    	if(!headline.equals(that.headline))
    	{
    		return false;
    	}
    	return true;
    }
}
