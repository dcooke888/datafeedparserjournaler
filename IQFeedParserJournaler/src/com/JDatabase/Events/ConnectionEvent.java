package com.JDatabase.Events;

import java.util.TimeZone;

import com.JDatabase.IQFeed.Feed;
import com.JDatabase.Serialize.Serializer;
import com.JDatabase.Utils.EqualityCheck;

public class ConnectionEvent extends DataEvent{

	public final boolean isConnected;
	public final Feed feed;
	
	public ConnectionEvent(Feed feed, TimeZone timeZone, long receiveTime, boolean isConnected)
	{
		super(timeZone, receiveTime, EventType.CONNECTION_STATUS);
		this.isConnected = isConnected;
		this.feed = feed;
	}
	
	public static DataEvent[] getConnectionEvent(Feed feed, TimeZone timeZone, long receiveTime, boolean isConnected)
	{
		DataEvent[] out = new DataEvent[1];
		out[0] = new ConnectionEvent(feed, timeZone, receiveTime, isConnected);
		return out;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ConnectionEvent read(Serializer serializer, EventType eventType) {
		TimeZone timeZone = TimeZone.getTimeZone(serializer.getString());
		long receiveTime = serializer.getLong();
		boolean isConnected = serializer.getBoolean();
		Feed feed = Feed.fromByteId(serializer.getByte());
		return new ConnectionEvent(feed, timeZone, receiveTime, isConnected);
	}

	@Override
	public Serializer write(Serializer serializer) {
		serializer.putByte(eventType.getByteId());
		serializer.putString(timeZone.getID());
		serializer.putLong(receiveTime);
		serializer.putBoolean(isConnected);
		serializer.putByte(feed.getId());
		return serializer;
	}
	
	@Override
	public boolean equals(Object o)
	{
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
 
        final ConnectionEvent that = (ConnectionEvent)o;
        
        if(!EqualityCheck.isTimeZoneEqual(timeZone, that.timeZone))
        {
        	return false;
        }
        if (receiveTime != that.receiveTime)
        {
            return false;
        }
        if (!feed.equals(that.feed))
        {
            return false;
        }
    	if(isConnected != that.isConnected)
    	{
    		return false;
    	}
    	return true;
    }
}
