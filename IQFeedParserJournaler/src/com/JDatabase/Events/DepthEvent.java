package com.JDatabase.Events;

import java.text.ParseException;
import java.util.TimeZone;

import com.JDatabase.Serialize.Serializer;
import com.JDatabase.Utils.DateUtils;
import com.JDatabase.Utils.EqualityCheck;

public class DepthEvent extends DataEvent{
	public final String symbol;
	public final String mmid;
	public final long feedTime;
	public final double price;
	public final int size;
	
	
	public DepthEvent(TimeZone timeZone, long receiveTime, String symbol, EventType eventType, 
			String mmid, String date, String updateTime, double price, int size) throws ParseException
	{
		this(timeZone, receiveTime, symbol, eventType, mmid, DateUtils.getL2EstimatedUTCTime(receiveTime, date, updateTime),
				price, size);
	}
	
	public DepthEvent(TimeZone timeZone, long receiveTime, String symbol, EventType eventType, 
			String mmid, long feedTime, double price, int size)
	{
		super(timeZone, receiveTime, eventType);
		this.symbol = symbol;
		this.mmid = mmid;
		this.feedTime = feedTime;
		this.price = price;
		this.size = size;
	}

	@SuppressWarnings("unchecked")
	@Override
	public DepthEvent read(Serializer serializer, EventType eventType) {
		TimeZone timeZone = TimeZone.getTimeZone(serializer.getString());
		long receiveTime = serializer.getLong();
		String symbol = serializer.getString();
		String mmid = serializer.getString();
		long feedTime = serializer.getLong();
		double price = serializer.getDouble();
		int size = serializer.getInt();
		return new DepthEvent(timeZone, receiveTime, symbol, eventType, mmid, feedTime, price, size);
	}

	@Override
	public Serializer write(Serializer serializer)
	{
		serializer.putByte(eventType.getByteId());
		serializer.putString(timeZone.getID());
		serializer.putLong(receiveTime);
		serializer.putString(symbol);
		serializer.putString(mmid);
		serializer.putLong(feedTime);
		serializer.putDouble(price);
		serializer.putInt(size);
		return serializer;
	}
	
	@Override
	public boolean equals(Object o)
	{
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
 
        final DepthEvent that = (DepthEvent)o;
        
        if(!EqualityCheck.isTimeZoneEqual(timeZone, that.timeZone))
        {
        	return false;
        }
        if (receiveTime != that.receiveTime)
        {
            return false;
        }
        if (!symbol.equals(that.symbol))
        {
            return false;
        }
    	if(!mmid.equals(that.mmid))
    	{
    		return false;
    	}
    	if(feedTime != that.feedTime)
    	{
    		return false;
    	}
    	if(!EqualityCheck.isDoubleEqual(price, that.price, 6))
    	{
    		return false;
    	}
    	if(size != that.size)
    	{
    		return false;
    	}
    	return true;
    }
}
