package com.JDatabase.Events.Serialize;

import java.text.ParseException;
import java.util.TimeZone;

import com.JDatabase.Events.ConnectionEvent;
import com.JDatabase.Events.DataEvent;
import com.JDatabase.Events.DepthEvent;
import com.JDatabase.Events.ErrorEvent;
import com.JDatabase.Events.EventType;
import com.JDatabase.Events.FundamentalEvent;
import com.JDatabase.Events.TradeEvent;
import com.JDatabase.Events.NewsEvent;
import com.JDatabase.Events.SymbolNotFoundEvent;
import com.JDatabase.Events.TOBEvent;
import com.JDatabase.IQFeed.Feed;
import com.JDatabase.Serialize.Serializer;

public class EventDeserializer {
	
	public static final DataEvent deserializeEvent(Serializer serializer) {
		EventType eventType = EventType.fromByteId(serializer.getByte());
		switch(eventType) {
		case ASK:
		case BID:
		case REGIONAL_ASK:
		case REGIONAL_BID:
			return readTOBEvent(serializer, eventType);
		case ASK_DEPTH:
		case BID_DEPTH:
			return readDepthEvent(serializer, eventType);
		case TRADE:
			return readLastEvent(serializer, eventType);
		case FUNDAMENTAL:
			return readFundamentalEvent(serializer, eventType);
		case CONNECTION_STATUS:
			return readConnectionEvent(serializer, eventType);
		case ERROR:
			return readErrorEvent(serializer, eventType);
		case NEWS:
			return readNewsEvent(serializer, eventType);
		case SYMBOL_NOT_FOUND:
			return readSymbolNotFoundEvent(serializer, eventType);
		case SYSTEM_MESSAGE:
			return null;
		case UNKNOWN:
			return null;
		}
		return null;
	}
	
	public static final ConnectionEvent readConnectionEvent(Serializer serializer, EventType eventType) {
		TimeZone timeZone = TimeZone.getTimeZone(serializer.getString());
		long receiveTime = serializer.getLong();
		boolean isConnected = serializer.getBoolean();
		Feed feed = Feed.fromByteId(serializer.getByte());
		return new ConnectionEvent(feed, timeZone, receiveTime, isConnected);
	}
	
	public static final DepthEvent readDepthEvent(Serializer serializer, EventType eventType) {
		TimeZone timeZone = TimeZone.getTimeZone(serializer.getString());
		long receiveTime = serializer.getLong();
		String symbol = serializer.getString();
		String mmid = serializer.getString();
		long feedTime = serializer.getLong();
		double price = serializer.getDouble();
		int size = serializer.getInt();
		return new DepthEvent(timeZone, receiveTime, symbol, eventType, mmid, feedTime, price, size);
	}
	
	public static final ErrorEvent readErrorEvent(Serializer serializer, EventType eventType) {
		TimeZone timeZone = TimeZone.getTimeZone(serializer.getString());
		long receiveTime = serializer.getLong();
		String message = serializer.getString();
		return new ErrorEvent(timeZone, receiveTime, message);
	}
	
	public static final FundamentalEvent readFundamentalEvent(Serializer serializer, EventType eventType) {
		TimeZone timeZone = TimeZone.getTimeZone(serializer.getString());
		long receiveTime = serializer.getLong();
		String symbol = serializer.getString();
		String exchangeId = serializer.getString();
		double PERatio = serializer.getDouble();
		int 	avgVolume = serializer.getInt();
		double price_52WkHigh = serializer.getDouble();
		double price_52WkLow = serializer.getDouble();
		String date52WkHigh = serializer.getString();
		String date52WkLow = serializer.getString();
		double priceCalendarYearHigh = serializer.getDouble();
		double priceCalendarYearLow = serializer.getDouble();
		String calendarYearHighDate = serializer.getString();
		String calendarYearLowDate = serializer.getString();
		double dividendYield = serializer.getDouble();
		double dividendAmount = serializer.getDouble();
		double dividendRate = serializer.getDouble();
		String payDate = serializer.getString();
		String exDividendDate = serializer.getString();
		int 	shortInterest = serializer.getInt();
		double currentYearEPS = serializer.getDouble();
		double nextYearEPS = serializer.getDouble();
		double fiveYearGrowthPercentage = serializer.getDouble();
		int 	fiscalYearEnd = serializer.getInt();
		String companyName = serializer.getString();
		String rootOptionSymbol = serializer.getString();
		double percentHeldByInstitutions = serializer.getDouble();
		double beta = serializer.getDouble();
		String leaps = serializer.getString();
		double currentAssets = serializer.getDouble();
		double currentLiabilities = serializer.getDouble();
		String balanceSheetDate = serializer.getString();
		double longTermDebt = serializer.getDouble();
		double commonSharesOutstanding = serializer.getDouble();
		String splitFactor1 = serializer.getString();
		String splitFactor2 = serializer.getString();
		String formatCode = serializer.getString();
		int 	precision = serializer.getInt();
		int 	SIC = serializer.getInt();
		double historicalVolatility = serializer.getDouble();
		String securityType = serializer.getString();
		String listedMarket = serializer.getString();
		double yearEndClosePrice = serializer.getDouble();
		String maturityDate = serializer.getString();
		double couponRate = serializer.getDouble();
		String expirationDate = serializer.getString();
		double strikePrice = serializer.getDouble();
		int 	NAICS = serializer.getInt();
		String exchangeRoot = serializer.getString();
		return new FundamentalEvent(timeZone, receiveTime, symbol, exchangeId, PERatio, avgVolume, 
				price_52WkHigh, price_52WkLow, 
				date52WkHigh, date52WkLow, priceCalendarYearHigh, priceCalendarYearLow, 
				calendarYearHighDate, calendarYearLowDate, dividendYield, dividendAmount, 
				dividendRate, payDate, exDividendDate, shortInterest, currentYearEPS, 
				nextYearEPS, fiveYearGrowthPercentage, fiscalYearEnd, companyName, 
				rootOptionSymbol, percentHeldByInstitutions, beta, leaps, currentAssets, 
				currentLiabilities, balanceSheetDate, longTermDebt, commonSharesOutstanding,
				splitFactor1, splitFactor2, formatCode, precision, SIC, historicalVolatility,
				securityType, listedMarket, yearEndClosePrice, maturityDate, couponRate,
				expirationDate, strikePrice, NAICS, exchangeRoot);
	}
	
	public static final TradeEvent readLastEvent(Serializer serializer, EventType eventType) {
		TimeZone timeZone = TimeZone.getTimeZone(serializer.getString());
		long receiveTime = serializer.getLong();
		String symbol = serializer.getString();
		String tradeType = serializer.getString();
		String tradeConditions = serializer.getString();
		String exchangeId = serializer.getString();
		String lastMktCenter = serializer.getString();
		long feedTime = serializer.getLong();
		double lastPrice = serializer.getDouble();
		int lastSize = serializer.getInt();
		int tickId = serializer.getInt();
		int openInterest = serializer.getInt();
		double vwap = serializer.getDouble();
		return new TradeEvent(timeZone, receiveTime, symbol, tradeType, tradeConditions,
					exchangeId, lastMktCenter, feedTime, lastPrice, lastSize, tickId,
					openInterest, vwap);
	}
	
	public static final NewsEvent readNewsEvent(Serializer serializer, EventType eventType) {
		TimeZone timeZone = TimeZone.getTimeZone(serializer.getString());
		long receiveTime = serializer.getLong();
		String distributorCode = serializer.getString();
		String storyId = serializer.getString();
		String symbolList = serializer.getString();
		String dateTime = serializer.getString();
		String headline = serializer.getString();
		return new NewsEvent(timeZone, receiveTime, distributorCode, storyId, symbolList, dateTime, headline);
	}
	
	public static final SymbolNotFoundEvent readSymbolNotFoundEvent(Serializer serializer, EventType eventType) {
		TimeZone timeZone = TimeZone.getTimeZone(serializer.getString());
		long receiveTime = serializer.getLong();
		Feed feed = Feed.fromByteId(serializer.getByte());
		String symbol = serializer.getString();
		return new SymbolNotFoundEvent(feed, timeZone, receiveTime, symbol);
	}
	
	public static final TOBEvent readTOBEvent(Serializer serializer, EventType eventType) {
		TimeZone timeZone = TimeZone.getTimeZone(serializer.getString());
		long receiveTime = serializer.getLong();
		String symbol = serializer.getString();
		String exchange = serializer.getString();
		String mktCenter = serializer.getString();
		long feedTime = serializer.getLong();
		double price = serializer.getDouble();
		int size = serializer.getInt();
		return new TOBEvent(timeZone, receiveTime, symbol, eventType, exchange, mktCenter, feedTime, price, size);
	}
}
