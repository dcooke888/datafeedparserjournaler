package com.JDatabase.Events;

import java.util.TimeZone;

import com.JDatabase.Serialize.Serializer;
import com.JDatabase.Utils.EqualityCheck;

public class FundamentalEvent extends DataEvent{
	public final String symbol;
	public final String exchangeId;
	public final double PERatio;
	public final int 	avgVolume;
	public final double price_52WkHigh;
	public final double price_52WkLow;
	public final String date52WkHigh;
	public final String date52WkLow;
	public final double priceCalendarYearHigh;
	public final double priceCalendarYearLow;
	public final String calendarYearHighDate;
	public final String calendarYearLowDate;
	public final double dividendYield;
	public final double dividendAmount;
	public final double dividendRate;
	public final String payDate;
	public final String exDividendDate;
	public final int 	shortInterest;
	public final double currentYearEPS;
	public final double nextYearEPS;
	public final double fiveYearGrowthPercentage;
	public final int 	fiscalYearEnd;
	public final String companyName;
	public final String rootOptionSymbol;
	public final double percentHeldByInstitutions;
	public final double beta;
	public final String leaps;
	public final double currentAssets;
	public final double currentLiabilities;
	public final String balanceSheetDate;
	public final double longTermDebt;
	public final double commonSharesOutstanding;
	public final String splitFactor1;
	public final String splitFactor2;
	public final String formatCode;
	public final int 	precision;
	public final int 	SIC;
	public final double historicalVolatility;
	public final String securityType;
	public final String listedMarket;
	public final double yearEndClosePrice;
	public final String maturityDate;
	public final double couponRate;
	public final String expirationDate;
	public final double strikePrice;
	public final int 	NAICS;
	public final String exchangeRoot;
	
	public FundamentalEvent(TimeZone timeZone, long receiveTime, String symbol, String exchangeId, double PERatio, int avgVolume, double price_52WkHigh, double price_52WkLow, 
			String date52WkHigh, String date52WkLow, double priceCalendarYearHigh, double priceCalendarYearLow, 
			String calendarYearHighDate, String calendarYearLowDate, double dividendYield, double dividendAmount, 
			double dividendRate, String payDate, String exDividendDate, int shortInterest, double currentYearEPS, 
			double nextYearEPS, double fiveYearGrowthPercentage, int fiscalYearEnd, String companyName, 
			String rootOptionSymbol, double percentHeldByInstitutions, double beta, String leaps, double currentAssets, 
			double currentLiabilities, String balanceSheetDate, double longTermDebt, double commonSharesOutstanding,
			String splitFactor1, String splitFactor2, String formatCode, int precision, int SIC, double historicalVolatility,
			String securityType, String listedMarket, double yearEndClosePrice, String maturityDate, double couponRate,
			String expirationDate, double strikePrice, int NAICS, String exchangeRoot)
	{
		super(timeZone, receiveTime, EventType.FUNDAMENTAL);
		this.symbol = symbol;
		this.exchangeId = exchangeId;
		this.PERatio = PERatio;
		this.avgVolume = avgVolume;
		this.price_52WkHigh = price_52WkHigh;
		this.price_52WkLow = price_52WkLow;
		this.date52WkHigh = date52WkHigh;
		this.date52WkLow = date52WkLow;
		this.priceCalendarYearHigh = priceCalendarYearHigh;
		this.priceCalendarYearLow = priceCalendarYearLow;
		this.calendarYearHighDate = calendarYearHighDate;
		this.calendarYearLowDate = calendarYearLowDate;
		this.dividendYield = dividendYield;
		this.dividendAmount = dividendAmount;
		this.dividendRate = dividendRate;
		this.payDate = payDate;
		this.exDividendDate = exDividendDate;
		this.shortInterest = shortInterest;
		this.currentYearEPS = currentYearEPS;
		this.nextYearEPS = nextYearEPS;
		this.fiveYearGrowthPercentage = fiveYearGrowthPercentage;
		this.fiscalYearEnd = fiscalYearEnd;
		this.companyName = companyName;
		this.rootOptionSymbol = rootOptionSymbol;
		this.percentHeldByInstitutions = percentHeldByInstitutions;
		this.beta = beta;
		this.leaps = leaps;
		this.currentAssets = currentAssets;
		this.currentLiabilities = currentLiabilities;
		this.balanceSheetDate = balanceSheetDate;
		this.longTermDebt = longTermDebt;
		this.commonSharesOutstanding = commonSharesOutstanding;
		this.splitFactor1 = splitFactor1;
		this.splitFactor2 = splitFactor2;
		this.formatCode = formatCode;
		this.precision = precision;
		this.SIC = SIC;
		this.historicalVolatility = historicalVolatility;
		this.securityType = securityType;
		this.listedMarket = listedMarket;
		this.yearEndClosePrice = yearEndClosePrice;
		this.maturityDate = maturityDate;
		this.couponRate = couponRate;
		this.expirationDate = expirationDate;
		this.strikePrice = strikePrice;
		this.NAICS = NAICS;
		this.exchangeRoot = exchangeRoot;
	}

	@SuppressWarnings("unchecked")
	@Override
	public FundamentalEvent read(Serializer serializer, EventType eventType)
	{
		TimeZone timeZone = TimeZone.getTimeZone(serializer.getString());
		long receiveTime = serializer.getLong();
		String symbol = serializer.getString();
		String exchangeId = serializer.getString();
		double PERatio = serializer.getDouble();
		int 	avgVolume = serializer.getInt();
		double price_52WkHigh = serializer.getDouble();
		double price_52WkLow = serializer.getDouble();
		String date52WkHigh = serializer.getString();
		String date52WkLow = serializer.getString();
		double priceCalendarYearHigh = serializer.getDouble();
		double priceCalendarYearLow = serializer.getDouble();
		String calendarYearHighDate = serializer.getString();
		String calendarYearLowDate = serializer.getString();
		double dividendYield = serializer.getDouble();
		double dividendAmount = serializer.getDouble();
		double dividendRate = serializer.getDouble();
		String payDate = serializer.getString();
		String exDividendDate = serializer.getString();
		int 	shortInterest = serializer.getInt();
		double currentYearEPS = serializer.getDouble();
		double nextYearEPS = serializer.getDouble();
		double fiveYearGrowthPercentage = serializer.getDouble();
		int 	fiscalYearEnd = serializer.getInt();
		String companyName = serializer.getString();
		String rootOptionSymbol = serializer.getString();
		double percentHeldByInstitutions = serializer.getDouble();
		double beta = serializer.getDouble();
		String leaps = serializer.getString();
		double currentAssets = serializer.getDouble();
		double currentLiabilities = serializer.getDouble();
		String balanceSheetDate = serializer.getString();
		double longTermDebt = serializer.getDouble();
		double commonSharesOutstanding = serializer.getDouble();
		String splitFactor1 = serializer.getString();
		String splitFactor2 = serializer.getString();
		String formatCode = serializer.getString();
		int 	precision = serializer.getInt();
		int 	SIC = serializer.getInt();
		double historicalVolatility = serializer.getDouble();
		String securityType = serializer.getString();
		String listedMarket = serializer.getString();
		double yearEndClosePrice = serializer.getDouble();
		String maturityDate = serializer.getString();
		double couponRate = serializer.getDouble();
		String expirationDate = serializer.getString();
		double strikePrice = serializer.getDouble();
		int 	NAICS = serializer.getInt();
		String exchangeRoot = serializer.getString();
		return new FundamentalEvent(timeZone, receiveTime, symbol, exchangeId, PERatio, avgVolume, 
				price_52WkHigh, price_52WkLow, 
				date52WkHigh, date52WkLow, priceCalendarYearHigh, priceCalendarYearLow, 
				calendarYearHighDate, calendarYearLowDate, dividendYield, dividendAmount, 
				dividendRate, payDate, exDividendDate, shortInterest, currentYearEPS, 
				nextYearEPS, fiveYearGrowthPercentage, fiscalYearEnd, companyName, 
				rootOptionSymbol, percentHeldByInstitutions, beta, leaps, currentAssets, 
				currentLiabilities, balanceSheetDate, longTermDebt, commonSharesOutstanding,
				splitFactor1, splitFactor2, formatCode, precision, SIC, historicalVolatility,
				securityType, listedMarket, yearEndClosePrice, maturityDate, couponRate,
				expirationDate, strikePrice, NAICS, exchangeRoot);
	}
	
	@Override
	public Serializer write(Serializer serializer)
	{
		serializer.putByte(EventType.FUNDAMENTAL.getByteId());
		serializer.putString(timeZone.getID());
		serializer.putLong(receiveTime);
		serializer.putString(symbol);
		serializer.putString(exchangeId);
		serializer.putDouble(PERatio);
		serializer.putInt(avgVolume);
		serializer.putDouble(price_52WkHigh);
		serializer.putDouble(price_52WkLow);
		serializer.putString(date52WkHigh);
		serializer.putString(date52WkLow);
		serializer.putDouble(priceCalendarYearHigh);
		serializer.putDouble(priceCalendarYearLow);
		serializer.putString(calendarYearHighDate);
		serializer.putString(calendarYearLowDate);
		serializer.putDouble(dividendYield);
		serializer.putDouble(dividendAmount);
		serializer.putDouble(dividendRate);
		serializer.putString(payDate);
		serializer.putString(exDividendDate);
		serializer.putInt(shortInterest);
		serializer.putDouble(currentYearEPS);
		serializer.putDouble(nextYearEPS);
		serializer.putDouble(fiveYearGrowthPercentage);
		serializer.putInt(fiscalYearEnd);
		serializer.putString(companyName);
		serializer.putString(rootOptionSymbol);
		serializer.putDouble(percentHeldByInstitutions);
		serializer.putDouble(beta);
		serializer.putString(leaps);
		serializer.putDouble(currentAssets);
		serializer.putDouble(currentLiabilities);
		serializer.putString(balanceSheetDate);
		serializer.putDouble(longTermDebt);
		serializer.putDouble(commonSharesOutstanding);
		serializer.putString(splitFactor1);
		serializer.putString(splitFactor2);
		serializer.putString(formatCode);
		serializer.putInt(precision);
		serializer.putInt(SIC);
		serializer.putDouble(historicalVolatility);
		serializer.putString(securityType);
		serializer.putString(listedMarket);
		serializer.putDouble(yearEndClosePrice);
		serializer.putString(maturityDate);
		serializer.putDouble(couponRate);
		serializer.putString(expirationDate);
		serializer.putDouble(strikePrice);
		serializer.putInt(NAICS);
		serializer.putString(exchangeRoot);
		return serializer;
	}
	


@Override
public boolean equals(Object o)
{
    if (this == o)
    {
        return true;
    }
    if (o == null || getClass() != o.getClass())
    {
        return false;
    }

    final FundamentalEvent that = (FundamentalEvent)o;
    
    if(!EqualityCheck.isTimeZoneEqual(timeZone, that.timeZone)) return false;
    if (receiveTime != that.receiveTime) return false;
    if (eventType != that.eventType) return false;
    if (!symbol.equals(that.symbol)) return false;
    if (!exchangeId.equals(that.exchangeId)) return false;
    if (!EqualityCheck.isDoubleEqual(PERatio, that.PERatio, 8)) return false;
    if (avgVolume != that.avgVolume) return false;
    if (!EqualityCheck.isDoubleEqual(price_52WkHigh, that.price_52WkHigh, 8)) return false;
    if (!EqualityCheck.isDoubleEqual(price_52WkLow, that.price_52WkLow, 8)) return false;
    if (!EqualityCheck.isDoubleEqual(priceCalendarYearHigh, that.priceCalendarYearHigh, 8)) return false;
    if (!EqualityCheck.isDoubleEqual(priceCalendarYearLow, that.priceCalendarYearLow, 8)) return false;
    if (!EqualityCheck.isDoubleEqual(dividendYield, that.dividendYield, 8)) return false;
    if (!EqualityCheck.isDoubleEqual(dividendAmount, that.dividendAmount, 8)) return false;
    if (!EqualityCheck.isDoubleEqual(dividendRate, that.dividendRate, 8)) return false;
    if (!payDate.equals(that.payDate)) return false;
    if (!exDividendDate.equals(that.exDividendDate)) return false;
    if (shortInterest != that.shortInterest) return false;
    if (!EqualityCheck.isDoubleEqual(currentYearEPS, that.currentYearEPS, 8)) return false;
    if (!EqualityCheck.isDoubleEqual(nextYearEPS, that.nextYearEPS, 8)) return false;
    if (!EqualityCheck.isDoubleEqual(fiveYearGrowthPercentage, that.fiveYearGrowthPercentage, 8)) return false;
    if (fiscalYearEnd != that.fiscalYearEnd) return false;
    if (!companyName.equals(that.companyName)) return false;
    if (!rootOptionSymbol.equals(that.rootOptionSymbol)) return false;
    if (!EqualityCheck.isDoubleEqual(percentHeldByInstitutions, that.percentHeldByInstitutions, 8)) return false;
    if (!EqualityCheck.isDoubleEqual(beta, that.beta, 8)) return false;
    if (!leaps.equals(that.leaps)) return false;
    
    if (!EqualityCheck.isDoubleEqual(currentAssets, that.currentAssets, 8)) return false;
    if (!EqualityCheck.isDoubleEqual(currentLiabilities, that.currentLiabilities, 8)) return false;
    if (!balanceSheetDate.equals(that.balanceSheetDate)) return false;
    if (!EqualityCheck.isDoubleEqual(longTermDebt, that.longTermDebt, 8)) return false;
    if (!EqualityCheck.isDoubleEqual(commonSharesOutstanding, that.commonSharesOutstanding, 8)) return false;
    if (!splitFactor1.equals(that.splitFactor1)) return false;
    if (!splitFactor2.equals(that.splitFactor2)) return false;
    if (!formatCode.equals(that.formatCode)) return false;
    if (precision != that.precision) return false;
    if (SIC != that.SIC) return false;
    if (!EqualityCheck.isDoubleEqual(historicalVolatility, that.historicalVolatility, 8)) return false;
    if (!securityType.equals(that.securityType)) return false;
    if (!listedMarket.equals(that.listedMarket)) return false;
    if (!date52WkHigh.equals(that.date52WkHigh)) return false;
    if (!date52WkLow.equals(that.date52WkLow)) return false;
    if (!calendarYearHighDate.equals(that.calendarYearHighDate)) return false;
    if (!calendarYearLowDate.equals(that.calendarYearLowDate)) return false;
    if (!EqualityCheck.isDoubleEqual(yearEndClosePrice, that.yearEndClosePrice, 8)) return false;
    if (!maturityDate.equals(that.maturityDate)) return false;
    if (!EqualityCheck.isDoubleEqual(couponRate, that.couponRate, 8)) return false;
    if (!expirationDate.equals(that.expirationDate)) return false;
    if (!EqualityCheck.isDoubleEqual(strikePrice, that.strikePrice, 8)) return false;
    if (NAICS != that.NAICS) return false;
    if (!exchangeRoot.equals(that.exchangeRoot)) return false;
	return true;
}
}
