package com.JDatabase.Events;

import java.text.ParseException;
import java.util.TimeZone;

import com.JDatabase.Serialize.Serializer;
import com.JDatabase.Utils.DateUtils;
import com.JDatabase.Utils.EqualityCheck;

public class TOBEvent extends DataEvent{
	public final String symbol;
	public final String exchange;
	public final String mktCenter;
	public final long feedTime;
	public final double price;
	public final int size;
	
	public TOBEvent(TimeZone timeZone, long receiveTime, String symbol, EventType eventType, 
			String exchange, String mktCenter, long feedTime, double price, int size) {
		super(timeZone, receiveTime, eventType);
		this.symbol = symbol;
		this.exchange = exchange;
		this.mktCenter = mktCenter;
		this.feedTime = feedTime;
		this.price = price;
		this.size = size;
	}
	
	public TOBEvent(TimeZone timeZone, long receiveTime, String symbol, EventType eventType, 
			String exchange, String mktCenter, String updateTime, double price, int size) throws ParseException
	{
		this(timeZone, receiveTime, symbol, eventType, exchange, mktCenter,
				DateUtils.getL1EstimatedUTCTime(receiveTime, updateTime), price, size);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public TOBEvent read(Serializer serializer, EventType eventType)
	{
		TimeZone timeZone = TimeZone.getTimeZone(serializer.getString());
		long receiveTime = serializer.getLong();
		String symbol = serializer.getString();
		String exchange = serializer.getString();
		String mktCenter = serializer.getString();
		long feedTime = serializer.getLong();
		double price = serializer.getDouble();
		int size = serializer.getInt();
		return new TOBEvent(timeZone, receiveTime, symbol, eventType, exchange, mktCenter, feedTime, price, size);
	}
	
	@Override
	public Serializer write(Serializer serializer)
	{
		serializer.putByte(eventType.getByteId());
		serializer.putString(timeZone.getID());
		serializer.putLong(receiveTime);
		serializer.putString(symbol);
		serializer.putString(exchange);
		serializer.putString(mktCenter);
		serializer.putLong(feedTime);
		serializer.putDouble(price);
		serializer.putInt(size);
		return serializer;
	}
	
	@Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
 
        final TOBEvent that = (TOBEvent)o;
        
        if(!EqualityCheck.isTimeZoneEqual(timeZone, that.timeZone))
        {
        	return false;
        }
        if (receiveTime != that.receiveTime)
        {
            return false;
        }
        if (!symbol.equals(that.symbol))
        {
            return false;
        }
    	if(!exchange.equals(that.exchange))
    	{
    		return false;
    	}
    	if(!mktCenter.equals(that.mktCenter))
    	{
    		return false;
    	}
    	if(feedTime != that.feedTime)
    	{
    		return false;
    	}
    	if(!EqualityCheck.isDoubleEqual(price, that.price, 6))
    	{
    		return false;
    	}
    	if(size != that.size)
    	{
    		return false;
    	}
    	return true;
    }

	
}
