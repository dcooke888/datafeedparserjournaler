package com.JDatabase.Events;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;


public enum EventType {
	UNKNOWN((byte) 0),
	BID((byte) 1),
	ASK((byte) 2),
	TRADE((byte) 3),
	BID_DEPTH((byte) 4),
	ASK_DEPTH((byte) 5),
	FUNDAMENTAL((byte) 6),
	REGIONAL_BID((byte) 7),
	REGIONAL_ASK((byte) 8),
	NEWS((byte) 9),
	SYSTEM_MESSAGE((byte) 10),
	SYMBOL_NOT_FOUND((byte) 11),
	CONNECTION_STATUS((byte) 12),
	ERROR((byte) 13),
	BAR((byte) 14);

	private EventType(byte val)
	{
		this.val = val;
	}
	
	private final byte val;
	
	private static final Map<Byte,EventType> lookupByte = new HashMap<Byte,EventType>();

	static {
	    for(EventType s : EnumSet.allOf(EventType.class))
	    {
	        lookupByte.put(s.getByteId(), s);
	    }
	}
	
	public byte getByteId()
	{
		return this.val;
	}

	public static EventType fromByteId(byte b)
	{
		EventType type = lookupByte.get(b);
		return (type == null) ? UNKNOWN : type;
	}
}
