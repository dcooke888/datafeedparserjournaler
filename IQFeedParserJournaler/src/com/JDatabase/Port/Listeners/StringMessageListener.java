package com.JDatabase.Port.Listeners;

import java.util.TimeZone;

import com.JDatabase.IQFeed.Feed;

public interface StringMessageListener {
	void notifyStringMessage(Feed feed, TimeZone timeZone, long receiveTime, String message);
}
