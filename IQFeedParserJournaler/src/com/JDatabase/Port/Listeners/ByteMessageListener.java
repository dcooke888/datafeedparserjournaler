package com.JDatabase.Port.Listeners;

import java.util.TimeZone;

import com.JDatabase.IQFeed.Feed;

public interface ByteMessageListener {
	void notifyByteMessage(Feed feed, TimeZone t, long receiveTime, byte[] message);
}
