package com.JDatabase.Port.ReaderWriter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import com.JDatabase.IQFeed.Feed;
import com.JDatabase.Port.Listeners.StringMessageListener;


public class StringReaderWriter extends PortReaderWriter{
	
	Socket s;
	BufferedReader r;
	BufferedWriter w;
	StringMessageListener toNotify;
	
	public StringReaderWriter(Feed feed, StringMessageListener toNotify) throws UnknownHostException, IOException {
		super(feed);
		this.s = new Socket(InetAddress.getLocalHost(),feed.getPort());
		this.r = new BufferedReader(new InputStreamReader(s.getInputStream()));
		this.w = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
		this.toNotify = toNotify;
	}

	@Override
	public void run() {
		try {
			String line;
			while((line = r.readLine()) != null) {
				toNotify.notifyStringMessage(feed, t, System.currentTimeMillis(), line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeToSocket(String toWrite) {
		try {
			w.write(toWrite);
			w.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
