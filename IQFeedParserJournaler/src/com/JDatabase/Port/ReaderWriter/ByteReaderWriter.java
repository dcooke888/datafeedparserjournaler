package com.JDatabase.Port.ReaderWriter;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.TimeZone;

import com.JDatabase.IQFeed.Feed;
import com.JDatabase.Port.Listeners.ByteMessageListener;


public class ByteReaderWriter extends PortReaderWriter {
	public static int BUFFER_SIZE = 1024 * 100; // Create a 100MB buffer
	private SocketChannel s;
	private ByteMessageListener toNotify;
	private ByteBuffer buffer;

	public ByteReaderWriter(Feed feed, ByteMessageListener toNotify) throws UnknownHostException, IOException
	{
		super(feed);
		s = SocketChannel.open();
		s.connect(new InetSocketAddress(InetAddress.getLocalHost(), feed.getPort()));
		s.configureBlocking(false);
		this.toNotify = toNotify;
		this.buffer = ByteBuffer.allocateDirect(BUFFER_SIZE);
	}

	@Override
	public void run()
	{
		// read from the socket as data becomes available
		int bytesRead = 0;
		try {
			do
			{
				int totalBytes = 0;
				do
				{
					bytesRead = s.read(buffer);
					if(bytesRead >= 0) totalBytes += bytesRead;
					
				} while(bytesRead>0 && buffer.hasRemaining());
				
				if(totalBytes > 0) fireReadBytes(t, System.currentTimeMillis());
				
			} while(bytesRead >= 0);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void fireReadBytes(TimeZone t, long receiveTime)
	{
		buffer.flip();
		int end;
		for(end = buffer.limit()-1; end >= 0; end--)
		{
			byte b = buffer.get(end);
			if(b == '\n' || b== '\r')
			{
				break;
			}
		}
		if(end > 0)
		{
			byte[] bytes = new byte[end + 1];
			buffer.get(bytes);
			toNotify.notifyByteMessage(feed, t, receiveTime, bytes);
			compact(buffer);
		} 
		
	}
	
	public static void compact(ByteBuffer bb) {
	    if (bb.remaining() == 0)
	        bb.clear();
	    else
	        bb.compact();
	}

}
