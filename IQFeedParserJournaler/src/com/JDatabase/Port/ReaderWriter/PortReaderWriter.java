package com.JDatabase.Port.ReaderWriter;

import java.util.TimeZone;

import com.JDatabase.IQFeed.Feed;

public abstract class PortReaderWriter implements Runnable{
	public static final TimeZone t = TimeZone.getDefault();
	
	public final Feed feed;

	public PortReaderWriter(Feed feed) {
		this.feed = feed;
	}
}
