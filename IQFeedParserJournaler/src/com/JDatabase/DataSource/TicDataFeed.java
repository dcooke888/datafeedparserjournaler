package com.JDatabase.DataSource;

import java.util.ArrayList;
import java.util.List;

import com.JDatabase.Events.DataEvent;
import com.JDatabase.Events.DepthEvent;
import com.JDatabase.Events.TOBEvent;
import com.JDatabase.Events.TradeEvent;

public class TicDataFeed {

	List<TicFeedListener> listeners = new ArrayList<TicFeedListener>();
	
	public void registerTicFeedListener(TicFeedListener listener) {
		if(!listeners.contains(listener)) listeners.add(listener);
	}
	
	public void unregisterTicFeedListner(TicFeedListener listener) {
		listeners.remove(listener);
	}
	
	public void notify(DataEvent event) {
		switch(event.eventType) {
		case TRADE:
			notify((TradeEvent) event);
			break;
		case ASK:
		case BID:
		case REGIONAL_ASK:
		case REGIONAL_BID:
			notify((TOBEvent) event);
			break;
		case ASK_DEPTH:
		case BID_DEPTH:
			notify((DepthEvent) event);
			break;
		default:
				break;
		}
	}
	
	public void notify(TradeEvent event) {
		for(TicFeedListener listener: listeners) 
			if(listener.tradeListener()) 
				listener.notify(event);
	}
	public void notify(TOBEvent event) {
		for(TicFeedListener listener: listeners) 
			if(listener.tobListener()) 
				listener.notify(event);
	}
	public void notify(DepthEvent event) {
		for(TicFeedListener listener: listeners) 
			if(listener.depthListener()) 
				listener.notify(event);
	}
	
	public void notifyStreamCompleted(int streamReqId) {
		for(TicFeedListener listener: listeners)
			listener.notifyStreamCompleted(streamReqId);
	}
}
