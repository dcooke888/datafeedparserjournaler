package com.JDatabase.DataSource;

import com.JDatabase.Events.DepthEvent;
import com.JDatabase.Events.TOBEvent;
import com.JDatabase.Events.TradeEvent;

public abstract class TicFeedListener {
	private boolean trade;
	private boolean tob;
	private boolean depth;
	
	public TicFeedListener(boolean trade, boolean tob, boolean depth) {
		this.trade = trade;
		this.tob = tob;
		this.depth = depth;
	}
	public boolean tradeListener() { return trade;	}
	public boolean tobListener() { return tob;	}
	public boolean depthListener() { return depth;	}
	
	public abstract void notify(TradeEvent event);
	public abstract void notify(TOBEvent event);
	public abstract void notify(DepthEvent event);
	public abstract void notifyStreamCompleted(int streamReqId);
}
