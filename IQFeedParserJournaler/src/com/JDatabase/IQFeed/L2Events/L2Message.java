package com.JDatabase.IQFeed.L2Events;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import com.JDatabase.Events.DataEvent;
import com.JDatabase.Events.DepthEvent;
import com.JDatabase.Events.EventType;
import com.JDatabase.Utils.DateUtils;
import com.JDatabase.Utils.StringUtils;

public class L2Message {
	
	public static DataEvent[] getL2Message(TimeZone timeZone, long receiveTime, String message)
	{
		String[] tokens = StringUtils.getCommaSplitString(message);
		if(tokens.length == 14)
		{
			
			boolean bidValid = tokens[11].indexOf("T") >= 0;
			boolean askValid = tokens[12].indexOf("T") >= 0;
//			boolean endOfMessageGroup = tokens[13].indexOf("T") >= 0;
			
			List<DataEvent> out = new ArrayList<DataEvent>();
			
			String symbol = tokens[1];
			String mmid =	tokens[2];
//			String conditionCode = tokens[9];
			
			if(bidValid)
			{
				String bidDate = tokens[8];
				String bidTime = tokens[7];
				double bidPrice = Double.valueOf(tokens[3]);
				int bidSize = Integer.valueOf(tokens[5]);
				try {
					out.add(new DepthEvent(timeZone, receiveTime, symbol, EventType.BID_DEPTH, mmid, bidDate, bidTime, bidPrice, bidSize));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			
			if(askValid)
			{
				String askDate = tokens[8];
				String askTime = tokens[10];
				double askPrice = Double.valueOf(tokens[4]);
				int askSize = Integer.valueOf(tokens[6]);
				try {
					out.add(new DepthEvent(timeZone, receiveTime, symbol, EventType.ASK_DEPTH, mmid, askDate, askTime, askPrice, askSize));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			return (out.size() > 0) ? out.toArray(new DataEvent[out.size()]) : null;
		} 
		return null;
	}

	public static DataEvent[] getOldL2Message(TimeZone timeZone,
			long receiveTime, String message) {
		// example string "U,	@ES#,	MD01,	1784.25,	1784.5,	211,	35,	21:18:01,	2013-11-19,	20,	52,	43,	21:17:49,	T,	T,";
		//					0,	1,		2,		3,			4,		5,		6,	7,			8,			9,	10,	11,	12,			13,	14;
		String[] tokens = StringUtils.getCommaSplitString(message);
		if(tokens.length == 15)
		{
			
			boolean bidValid = tokens[13].indexOf("T") >= 0;
			boolean askValid = tokens[14].indexOf("T") >= 0;
//			boolean endOfMessageGroup = tokens[13].indexOf("T") >= 0;
			
			List<DataEvent> out = new ArrayList<DataEvent>();
			
			String symbol = tokens[1];
			String mmid =	tokens[2];
//			String conditionCode = tokens[9];
			
			if(bidValid)
			{
				String bidDate = tokens[8];
				String bidTime = tokens[7];
				double bidPrice = Double.valueOf(tokens[3]);
				int bidSize = Integer.valueOf(tokens[5]);

				long time = DateUtils.getOLDL2EstimatedUTCTime(receiveTime, bidDate, bidTime);
				out.add(new DepthEvent(timeZone, receiveTime, symbol, EventType.BID_DEPTH, mmid, time, bidPrice, bidSize));
			}
			
			if(askValid)
			{
				String askDate = tokens[8];
				String askTime = tokens[12];
				double askPrice = Double.valueOf(tokens[4]);
				int askSize = Integer.valueOf(tokens[6]);
				long time = DateUtils.getOLDL2EstimatedUTCTime(receiveTime, askDate, askTime);
				out.add(new DepthEvent(timeZone, receiveTime, symbol, EventType.ASK_DEPTH, mmid, time, askPrice, askSize));
			}
			return (out.size() > 0) ? out.toArray(new DataEvent[out.size()]) : null;
		} 
		return null;
	}
}
