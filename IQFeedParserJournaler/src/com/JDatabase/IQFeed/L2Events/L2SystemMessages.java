package com.JDatabase.IQFeed.L2Events;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum L2SystemMessages {
		UNKNOWN((byte) 0, "UNKNOWN"),
		L2_SERVER_DISCONNECTED((byte) 1,"SERVER DISCONNECTED"),
		L2_SERVER_CONNECTED((byte) 2,"SERVER CONNECTED"),
		L2_RECONNECT_FAILED((byte) 3,"SERVER RECONNECT FAILED"),
		L2_CURRENT_PROTOCOL((byte) 4, "CURRENT PROTOCOL");

		
		private L2SystemMessages(byte idNum, String id)	{
			this.idNum = idNum;
			this.id = id;
		}
		private final byte idNum;
		private final String id;
		
		private static final Map<Byte,L2SystemMessages> lookupByte = new HashMap<Byte,L2SystemMessages>();
		private static final Map<String,L2SystemMessages> lookupFeedId= new HashMap<String,L2SystemMessages>();

		static {
		    for(L2SystemMessages s : EnumSet.allOf(L2SystemMessages.class))
		        lookupByte.put(s.getByteId(), s);
		    for(L2SystemMessages s : EnumSet.allOf(L2SystemMessages.class))
		    	lookupFeedId.put(s.id, s);
		}
		
		public byte getByteId() {
			return idNum;
		}
		
		public String getFeedId(){
			return id;
		}
		
		public static L2SystemMessages fromByteId(byte val) {
			return lookupByte.get(val);
		}
		
		public static L2SystemMessages fromFeedId(String s) {
			return lookupFeedId.get(s);
		}
}
