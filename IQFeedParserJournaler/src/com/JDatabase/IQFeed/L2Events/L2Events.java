package com.JDatabase.IQFeed.L2Events;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum L2Events {
	UNKNOWN((byte) 0, "UNKNOWN"),
	L2_SUMMARY((byte) 1,"Z"),
	L2_UPDATE((byte) 2,"2"),
	L2_SYSTEM((byte) 3,"S"),
	L2_SYMBOL_NOT_FOUND((byte) 4, "n"),
	L2_MARKET_MAKER((byte) 5,"M"),
	L2_ERROR((byte) 6, "E"),
	L2_TIMESTAMP((byte) 7,"T"),
	L2_OLD_SUMMARY((byte) 8, "Q"),
	L2_OLD_UPDATE((byte) 9, "U");

	
	private L2Events(byte idNum, String id)	{
		this.idNum = idNum;
		this.id = id;
	}
	private final byte idNum;
	private final String id;
	
	private static final Map<Byte,L2Events> lookupByte = new HashMap<Byte,L2Events>();
	private static final Map<String,L2Events> lookupFeedId= new HashMap<String,L2Events>();

	static {
	    for(L2Events s : EnumSet.allOf(L2Events.class))
	        lookupByte.put(s.getByteId(), s);
	    for(L2Events s : EnumSet.allOf(L2Events.class))
	    	lookupFeedId.put(s.id, s);
	}
	
	public byte getByteId() {
		return idNum;
	}
	
	public String getFeedId(){
		return id;
	}
	
	public static L2Events fromByteId(byte val) {
		L2Events event = lookupByte.get(val);
		return (event == null) ? UNKNOWN : event;
	}
	
	public static L2Events fromFeedId(String s) {
		L2Events event = lookupFeedId.get(s);
		return (event == null) ? UNKNOWN : event;
	}
}
