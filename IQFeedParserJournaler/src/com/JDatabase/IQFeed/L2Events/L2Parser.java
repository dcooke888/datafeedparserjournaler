package com.JDatabase.IQFeed.L2Events;

import java.util.TimeZone;

import com.JDatabase.Events.DataEvent;
import com.JDatabase.Events.ErrorEvent;
import com.JDatabase.Events.SymbolNotFoundEvent;
import com.JDatabase.IQFeed.Feed;
import com.JDatabase.Utils.StringUtils;

public class L2Parser{
	
	public static DataEvent[] getParsedMessage(Feed feed, TimeZone timeZone, long receiveTime, String message)
	{
			String splitVal = StringUtils.getFirstCommaSplitString(message);
			L2Events L2Event = L2Events.fromFeedId(splitVal);
			switch(L2Event)
			{
			case L2_ERROR:
				return ErrorEvent.getErrorMessage(timeZone, receiveTime, message);
			case L2_MARKET_MAKER:
				break;
			case L2_SUMMARY:
			case L2_UPDATE:
				return L2Message.getL2Message(timeZone, receiveTime, message);
			case L2_SYMBOL_NOT_FOUND:
				return SymbolNotFoundEvent.getSymbolNotFoundEvent(feed, timeZone, receiveTime, message);
			case L2_SYSTEM:
				return L2SystemMessage.getSystemMessage(timeZone, receiveTime, message);
			case L2_TIMESTAMP:
				break;
			case L2_OLD_SUMMARY:
			case L2_OLD_UPDATE:
				return L2Message.getOldL2Message(timeZone, receiveTime, message);
			case UNKNOWN:
				break;
			default:
				break;
			
			}
			return null;
	}
}
