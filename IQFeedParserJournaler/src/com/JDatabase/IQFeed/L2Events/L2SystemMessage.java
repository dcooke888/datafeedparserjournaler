package com.JDatabase.IQFeed.L2Events;

import java.util.TimeZone;

import com.JDatabase.Events.ConnectionEvent;
import com.JDatabase.Events.DataEvent;
import com.JDatabase.IQFeed.Feed;
import com.JDatabase.Utils.StringUtils;

public class L2SystemMessage{
	
	public static DataEvent[] getSystemMessage(TimeZone timeZone, long receiveTime, String line)
	{
		String[] tokens = StringUtils.getCommaSplitString(line);
		if(tokens.length >= 2)
		{
			L2SystemMessages msgType = L2SystemMessages.fromFeedId(tokens[1]);
			switch(msgType)
			{
			case L2_CURRENT_PROTOCOL:
				break;
			case L2_RECONNECT_FAILED:
				break;
			case L2_SERVER_CONNECTED:
				return ConnectionEvent.getConnectionEvent(Feed.L2, timeZone, receiveTime, true);
			case L2_SERVER_DISCONNECTED:
				return ConnectionEvent.getConnectionEvent(Feed.L2, timeZone, receiveTime, false);
			case UNKNOWN:
				break;
			default:
				break;
			
			
			}
		}
		return null;
	}
	
}
