package com.JDatabase.IQFeed;

import com.JDatabase.Events.DataEvent;
import com.JDatabase.Serialize.Serializer;
import com.JDatabase.Serialize.UnsafeMemory;
import com.lmax.disruptor.EventHandler;

public class UnsafeSerializedEventHandler implements EventHandler<IQFeedUpdateEvent>{
	Serializer serializer;
	
	public UnsafeSerializedEventHandler(int bufferSize)
	{
		serializer = new UnsafeMemory(bufferSize);
	}
	
	@Override
	public void onEvent(IQFeedUpdateEvent event, long sequence, boolean endOfBatch) throws Exception {
		DataEvent[] events = event.getEvents();
		for(DataEvent data: events)
		{
			data.write(serializer);
		}
		event.setSerializedBytes(serializer.getWrittenBytes());

		times[(int) count--] = System.nanoTime() - event.getReceiveTime();
	}
	
	private long[] times;
    private long count = 0;
	
    public void reset(final long expectedCount)
    {
        times = new long[(int) expectedCount + 1];
        count = expectedCount;
    }
	
	public long[] getTimes(){
		return times;
	}
}
