package com.JDatabase.IQFeed;

import com.JDatabase.IQFeed.L1Events.L1Parser;
import com.JDatabase.IQFeed.L2Events.L2Parser;
import com.lmax.disruptor.EventHandler;

public class ParserEventHandler implements EventHandler<IQFeedUpdateEvent>{

	@Override
	public void onEvent(IQFeedUpdateEvent event, long sequence, boolean endOfBatch) throws Exception {

			switch(event.getFeedType())
			{
			case L1:
				event.setEvents(L1Parser.getParsedMessage(event.getFeedType(), event.getTimeZone(),
						event.getReceiveTime(), event.getMessage()));
				break;
			case L2:
				event.setEvents(L2Parser.getParsedMessage(event.getFeedType(), event.getTimeZone(),
						event.getReceiveTime(), event.getMessage()));
				break;
			case ADMIN: 
				break;
			case LOOKUP:
				break;
			default:
				break;
			
			}
			times[(int) count--] = System.nanoTime() - event.getReceiveTime();
		}
		
		private long[] times;
	    private long count = 0;
		
	    public void reset(final long expectedCount)
	    {
	        times = new long[(int) expectedCount + 1];
	        count = expectedCount;
	    }
		
		public long[] getTimes(){
			return times;
		}
}
