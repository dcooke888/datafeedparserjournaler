package com.JDatabase.IQFeed;

import java.util.concurrent.CountDownLatch;

import com.lmax.disruptor.EventHandler;

public class CountDownEventHandler implements EventHandler<IQFeedUpdateEvent>{
	private long[] times;
    private CountDownLatch latch;
    private long count = 0;
	
    public void reset(final CountDownLatch latch, final long expectedCount)
    {
        this.latch = latch;
        times = new long[(int) expectedCount + 1];
        count = expectedCount;
    }
    
	@Override
	public void onEvent(IQFeedUpdateEvent event, long sequence,
			boolean endOfBatch) throws Exception {
		times[(int) count--] = System.nanoTime() - event.getReceiveTime();
		if(count <= 0) latch.countDown();
		
	}
	
	public long[] getTimes(){
		return times;
	}

}
