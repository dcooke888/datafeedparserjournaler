package com.JDatabase.IQFeed;

import java.util.TimeZone;

import com.JDatabase.Events.DataEvent;
import com.JDatabase.IQFeed.L1Events.L1Parser;
import com.JDatabase.IQFeed.L2Events.L2Parser;

public class Parser {
	
	public static DataEvent[] getDataEvent(Feed feed, TimeZone timeZone, long receiveTime, String message)
	{
		switch(feed)
		{
		case L1:
			return L1Parser.getParsedMessage(feed, timeZone, receiveTime, message);
		case L2:
			return L2Parser.getParsedMessage(feed, timeZone, receiveTime, message);
		case ADMIN: 
			break;
		case LOOKUP:
			break;
		default:
			break;
		
		}
		return null;
	}
}
