package com.JDatabase.IQFeed;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum Feed {
	UNKNOWN((byte) 0, "UNKOWN", 0),
	L1((byte) 1, "L1", 5009),
	L2((byte) 2, "L2", 9200),
	LOOKUP((byte) 3, "LOOKUP", 9100),
	ADMIN((byte) 4, "ADMIN", 9300);
	
	private Feed(byte id, String stringId, int port)
	{
		this.id = id;
		this.stringId = stringId;
		this.port = port;
	}
	private final byte id;
	private final String stringId;
	private final int port;
	
	private static final Map<Byte, Feed> feedIds = new HashMap<Byte, Feed>();
	private static final Map<Integer,Feed> feedPorts= new HashMap<Integer,Feed>();
	private static final Map<String, Feed> feedStringIds = new HashMap<String, Feed>();

	static {
	    for(Feed s : EnumSet.allOf(Feed.class))
	    {
	    	feedIds.put(s.id, s);
	    	feedStringIds.put(s.stringId, s);
	    	feedPorts.put(s.port, s);
	    }
	}
	
	@Override
	public String toString()
	{
		return stringId;
	}
	
	public byte getId()
	{
		return this.id;
	}
	
	public int getPort()
	{
		return this.port;
	}
	
	public static Feed fromPort(int port)
	{
		Feed feed = feedPorts.get(port);
		return (feed == null) ? UNKNOWN : feed;
	}
	
	public static Feed fromByteId(byte id)
	{
		Feed feed = feedIds.get(id);
		return (feed == null) ? UNKNOWN : feed;
	}


}
