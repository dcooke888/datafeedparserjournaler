package com.JDatabase.IQFeed.L1Events;

public enum L1DynamicFieldNames {
	SYMBOL("Symbol"),
	EXCHANGE_ID("Exchange ID"),
	LAST("Last"),
	CHANGE("Change"),
	PERCENT_CHANGE("Percent Change"),
	TOTAL_VOLUME("Total Volume"),
	HIGH("High"),
	LOW("Low"),
	BID("Bid"),
	ASK("Ask"),
	BID_SIZE("Bid Size"),
	ASK_SIZE("Ask Size"),
	TICK("Tick"),
	RANGE("Range"),
	OPEN_INTEREST("Open Interest"),
	OPEN("Open"),
	CLOSE("Close"),
	SPREAD("Spread"),
	STRIKE("Strike"),
	SETTLE("Settle"),
	DELAY("Delay"),
	RESTRICTED_CODE("Restricted Code"),
	NET_ASSET_VALUE("Net Asset Value"),
	AVG_MATURITY("Average Maturity"),
	SEVEN_DAY_YIEL("7 Day Yield"),
	NET_ASSET_VALUE_2("Net Asset Value 2"),
	EXTENDED_TRADING_CHANGE("Extended Trading Change"),
	EXTENDED_TRADING_DIFFERENCE("Extended Trading Difference"),
	PE_RATIO("Price-Earnings Ratio"),
	PERCENT_OFF_AVG_VOLUME("Percent Off Average Volume"),
	BID_CHANGE("Bid Change"),
	ASK_CHANGE("Ask Change"),
	CHANGE_FROM_OPEN("Change From Open"),
	MARKET_OPEN("Market Open"),
	VOLATILITY("Volatility"),
	MARKET_CAP("Market Capitalization"),
	FRACTION_DISPLAY_CODE("Fraction Display Code"),
	DECIMAL_PRECISION("Decimal Precision"),
	DAYS_TO_EXPIRATION("Days to Expiration"),
	PREVIOUS_DAY_VOLUME("Previous Day Volume"),
	OPEN_RANGE_ONE("Open Range 1"),
	CLOSE_RANGE_ONE("Close Range 1"),
	OPEN_RANGE_TWO("Open Range 2"),
	CLOSE_RANGE_TWO("Close Range 2"),
	NUMBER_OF_TRADES_TODAY("Number of Trades Today"),
	VWAP("VWAP"),
	TICK_ID("TickID"),
	FINANCIAL_STATUS_INDICATOR("Financial Status Indicator"),
	SETTLEMENT_DATE("Settlement Date"),
	BID_MARKET_CENTER("Bid Market Center"),
	ASK_MARKET_CENTER("Ask Market Center"),
	AVAILABLE_REGIONS("Available Regions"),
	LAST_SIZE("Last Size"),
	LAST_TIME("Last TimeMS"),
	LAST_MARKET_CENTER("Last Market Center"),
	MOST_RECENT_TRADE("Most Recent Trade"),
	MOST_RECENT_TRADE_SIZE("Most Recent Trade Size"),
	MOST_RECENT_TRADE_TIME("Most Recent Trade TimeMS"),
	MOST_RECENT_TRADE_CONDITIONS("Most Recent Trade Conditions"),
	MOST_RECENT_TRADE_MARKET_CENTER("Most Recent Trade Market Center"),
	EXTENDED_TRADE("Extended Trade"),
	EXTENDED_TRADE_SIZE("Extended Trade Size"),
	EXTENDEND_TRADE_TIME("Extended Trade TimeMS"),
	EXTENDED_TRADE_MARKET_CENTER("Extended Trade Market Center"),
	MESSAGE_CONTENTS("Message Contents"),
	ASK_TIME("Ask TimeMS"),
	BID_TIME("Bid TimeMS"),
	LAST_DATE("Last Date"),
	EXTENDED_TRADE_DATE("Extended Trade Date"),
	MOST_RECENT_TRADE_DATE("Most Recent Trade Date");

	private L1DynamicFieldNames(String iqFeedValue)
	{
		this.iqFeedValue = iqFeedValue; 
	}
	private String iqFeedValue;
	
	
	@Override
	public String toString()
	{
		return this.iqFeedValue;
	}

}
