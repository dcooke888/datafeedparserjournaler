package com.JDatabase.IQFeed.L1Events;

import java.util.TimeZone;

import com.JDatabase.Events.DataEvent;
import com.JDatabase.Events.ErrorEvent;
import com.JDatabase.Events.SymbolNotFoundEvent;
import com.JDatabase.IQFeed.Feed;

public class L1Parser {	
	public static DataEvent[] getParsedMessage(Feed feed, TimeZone timeZone, long receiveTime,
			String message) {
			int firstComma = message.indexOf(",");
			if(firstComma > 0)
			{
				String msgType = message.substring(0, firstComma).trim();
				L1Events eventType = L1Events.fromFeedId(msgType);
				switch(eventType)
				{
				case L1_FUNDAMENTAL:
					return L1FundamentalMessage.getFundamentalMessage(timeZone, receiveTime, message);
				case L1_REGIONAL:
					return L1RegionalMessage.getRegionalMessage(timeZone, receiveTime, message);
				case L1_SUMMARY:
					return null;//L1SummaryUpdateMessage.getL1Events(timeZone, receiveTime, message);
				case L1_SYSTEM:
					return L1SystemMessage.notifySystemMessage(timeZone, receiveTime, message);
				case L1_UPDATE:
					return L1SummaryUpdateMessage.getL1Events(timeZone, receiveTime, message);
				case UNKNOWN:
					return null;
				case L1_ERROR:
					return ErrorEvent.getErrorMessage(timeZone, receiveTime, message);
				case L1_SYMBOL_NOT_FOUND:
					return SymbolNotFoundEvent.getSymbolNotFoundEvent(feed, timeZone, receiveTime, message);
				case L1_NEWS:
					return L1NewsMessage.getNewsMessage(timeZone, receiveTime, message);
				case L1_TIMESTAMP:
					return null;
				default:
					return null;
				}
			}
			return null;
		
	}
}
