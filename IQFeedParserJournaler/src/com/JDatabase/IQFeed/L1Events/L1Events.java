package com.JDatabase.IQFeed.L1Events;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum L1Events {
	UNKNOWN((byte) 0, "UNKNOWN"),
	L1_FUNDAMENTAL((byte) 1,"F"),
	L1_SUMMARY((byte) 2,"P"),
	L1_UPDATE((byte) 3,"Q"),
	L1_SYSTEM((byte) 4,"S"),
	L1_REGIONAL((byte) 5,"R"),
	L1_NEWS((byte) 6,"N"),
	L1_TIMESTAMP((byte) 7,"T"),
	L1_SYMBOL_NOT_FOUND((byte) 8, "n"),
	L1_ERROR((byte) 9, "E");

	
	private L1Events(byte idNum, String id)	{
		this.idNum = idNum;
		this.id = id;
	}
	private final byte idNum;
	private final String id;
	
	private static final Map<Byte,L1Events> lookupByte = new HashMap<Byte,L1Events>();
	private static final Map<String,L1Events> lookupFeedId= new HashMap<String,L1Events>();

	static {
	    for(L1Events s : EnumSet.allOf(L1Events.class))
	        lookupByte.put(s.getByteId(), s);
	    for(L1Events s : EnumSet.allOf(L1Events.class))
	    	lookupFeedId.put(s.id, s);
	}
	
	public byte getByteId() {
		return idNum;
	}
	
	public String getFeedId(){
		return id;
	}
	
	public static L1Events fromByteId(byte val) {
		L1Events event = lookupByte.get(val);
		return (event == null) ? UNKNOWN : event;
	}
	
	public static L1Events fromFeedId(String s) {
		L1Events event = lookupFeedId.get(s);
		return (event == null) ? UNKNOWN : event;
	}
}
