package com.JDatabase.IQFeed.L1Events;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum L1SystemMessages {
		UNKNOWN((byte) 0, "UNKNOWN"),
		L1_SERVER_DISCONNECTED((byte) 1,"SERVER DISCONNECTED"),
		L1_SERVER_CONNECTED((byte) 2,"SERVER CONNECTED"),
		L1_KEY((byte) 3,"KEY"),
		L1_KEY_OK((byte) 4,"KEYOK"),
		L1_RECONNECT_FAILED((byte) 5,"SERVER RECONNECT FAILED"),
		L1_SYMBOL_LIMIT_REACHED((byte) 6,"SYMBOL LIMIT REACHED"),
		L1_USED_IPs((byte) 7,"IP"),
		L1_CUSTOMER((byte) 8, "CUST"),
		L1_STATS((byte) 9, "STATS"),
		L1_FUNDAMENTAL_FIELDNAMES((byte) 10, "FUNDAMENTAL FIELDNAMES"),
		L1_UPDATE_FIELDNAMES((byte) 11, "UPDATE FIELDNAMES"),
		L1_CURRENT_UPDATE_FIELDNAMES((byte) 12, "CURRENT UPDATE FIELDNAMES"),
		L1_CURRENT_LOG_LEVELS((byte) 13, "CURRENT LOG LEVELS"),
		L1_WATCHES((byte) 14, "WATCHES"),
		L1_CURRENT_PROTOCOL((byte) 15, "CURRENT PROTOCOL");

		
		private L1SystemMessages(byte idNum, String id)	{
			this.idNum = idNum;
			this.id = id;
		}
		private final byte idNum;
		private final String id;
		
		private static final Map<Byte,L1SystemMessages> lookupByte = new HashMap<Byte,L1SystemMessages>();
		private static final Map<String,L1SystemMessages> lookupFeedId= new HashMap<String,L1SystemMessages>();

		static {
		    for(L1SystemMessages s : EnumSet.allOf(L1SystemMessages.class))
		        lookupByte.put(s.getByteId(), s);
		    for(L1SystemMessages s : EnumSet.allOf(L1SystemMessages.class))
		    	lookupFeedId.put(s.id, s);
		}
		
		public byte getByteId() {
			return idNum;
		}
		
		public String getFeedId(){
			return id;
		}
		
		public static L1SystemMessages fromByteId(byte val) {
			return lookupByte.get(val);
		}
		
		public static L1SystemMessages fromFeedId(String s) {
			return lookupFeedId.get(s);
		}
}
