package com.JDatabase.IQFeed.L1Events;

import java.util.TimeZone;

import com.JDatabase.Events.DataEvent;
import com.JDatabase.Events.FundamentalEvent;
import com.JDatabase.Utils.StringUtils;

public class L1FundamentalMessage {
	
	public static final DataEvent[] getFundamentalMessage(TimeZone timeZone, long receiveTime, String line)
	{
		String[] tokens = StringUtils.getCommaSplitString(line);
		if(tokens.length == 56)
		{
			String symbol = tokens[1]; 
			String exchangeId = tokens[2]; 
			double PERatio = StringUtils.getDoubleFromString(tokens[3], Double.MIN_VALUE);
			int avgVolume = StringUtils.getIntFromString(tokens[4], Integer.MIN_VALUE);
			double price_52WkHigh = StringUtils.getDoubleFromString(tokens[5], Double.MIN_VALUE);
			double price_52WkLow = StringUtils.getDoubleFromString(tokens[6], Double.MIN_VALUE);
			double priceCalendarYearHigh = StringUtils.getDoubleFromString(tokens[7], Double.MIN_VALUE);
			double priceCalendarYearLow = StringUtils.getDoubleFromString(tokens[8], Double.MIN_VALUE);
			double dividendYield = StringUtils.getDoubleFromString(tokens[9], Double.MIN_VALUE);
			double dividendAmount = StringUtils.getDoubleFromString(tokens[10], Double.MIN_VALUE);
			double dividendRate = StringUtils.getDoubleFromString(tokens[11], Double.MIN_VALUE);
			String payDate = tokens[12]; 
			String exDividendDate = tokens[13]; 
			int shortInterest = StringUtils.getIntFromString(tokens[17], Integer.MIN_VALUE);
			double currentYearEPS = StringUtils.getDoubleFromString(tokens[19], Double.MIN_VALUE);
			double nextYearEPS = StringUtils.getDoubleFromString(tokens[20], Double.MIN_VALUE);
			double fiveYearGrowthPercentage = StringUtils.getDoubleFromString(tokens[21], Double.MIN_VALUE);
			int fiscalYearEnd = StringUtils.getIntFromString(tokens[22], Integer.MIN_VALUE);
			String companyName = tokens[24]; 
			String rootOptionSymbol = tokens[25]; 
			double percentHeldByInstitutions = StringUtils.getDoubleFromString(tokens[26], Double.MIN_VALUE);
			double beta = StringUtils.getDoubleFromString(tokens[27], Double.MIN_VALUE);
			String leaps = tokens[28]; 
			double currentAssets = StringUtils.getDoubleFromString(tokens[29], Double.MIN_VALUE);
			double currentLiabilities = StringUtils.getDoubleFromString(tokens[30], Double.MIN_VALUE);
			String balanceSheetDate = tokens[31]; 
			double longTermDebt = StringUtils.getDoubleFromString(tokens[32], Double.MIN_VALUE);
			double commonSharesOutstanding = StringUtils.getDoubleFromString(tokens[33], Double.MIN_VALUE);
			String splitFactor1 = tokens[35]; 
			String splitFactor2 = tokens[36]; 
			String formatCode = tokens[39]; 
			int precision = StringUtils.getIntFromString(tokens[40], Integer.MIN_VALUE);
			int SIC = StringUtils.getIntFromString(tokens[41], Integer.MIN_VALUE);
			double historicalVolatility = StringUtils.getDoubleFromString(tokens[42], Double.MIN_VALUE);
			String securityType = tokens[43]; 
			String listedMarket = tokens[44]; 
			String date52WkHigh = tokens[45]; 
			String date52WkLow = tokens[46]; 
			String calendarYearHighDate = tokens[47]; 
			String calendarYearLowDate = tokens[48]; 
			double yearEndClosePrice = StringUtils.getDoubleFromString(tokens[49], Double.MIN_VALUE);
			String maturityDate = tokens[50]; 
			double couponRate = StringUtils.getDoubleFromString(tokens[51], Double.MIN_VALUE);
			String expirationDate = tokens[52]; 
			double strikePrice = StringUtils.getDoubleFromString(tokens[53], Double.MIN_VALUE);
			int NAICS = StringUtils.getIntFromString(tokens[54], Integer.MIN_VALUE);
			String exchangeRoot = tokens[55]; 
			
			DataEvent[] out = new DataEvent[1];
			out[0] = new FundamentalEvent(timeZone, receiveTime,  symbol,  exchangeId,  PERatio, avgVolume,  price_52WkHigh,  price_52WkLow, 
					 date52WkHigh,  date52WkLow,  priceCalendarYearHigh,  priceCalendarYearLow, 
					 calendarYearHighDate,  calendarYearLowDate,  dividendYield,  dividendAmount, 
					 dividendRate,  payDate,  exDividendDate, shortInterest,  currentYearEPS, 
					 nextYearEPS,  fiveYearGrowthPercentage, fiscalYearEnd,  companyName, 
					 rootOptionSymbol,  percentHeldByInstitutions,  beta,  leaps,  currentAssets, 
					 currentLiabilities,  balanceSheetDate,  longTermDebt,  commonSharesOutstanding,
					 splitFactor1,  splitFactor2,  formatCode, precision, SIC,  historicalVolatility,
					 securityType,  listedMarket,  yearEndClosePrice,  maturityDate,  couponRate,
					 expirationDate,  strikePrice, NAICS,  exchangeRoot);
			return out;
		} return null;
	}
}
