package com.JDatabase.IQFeed.L1Events;

import java.util.TimeZone;

import com.JDatabase.Events.DataEvent;
import com.JDatabase.Events.NewsEvent;
import com.JDatabase.Utils.StringUtils;

public class L1NewsMessage{
	
	public static DataEvent[] getNewsMessage(TimeZone timeZone, long receiveTime, String line){
		String[] tokens = StringUtils.getCommaSplitString(line);
		if(tokens.length == 6)
		{
			DataEvent[] out = new DataEvent[1];
			String distributorCode = tokens[1];
			String storyId = tokens[2];
			String symbolList = tokens[3];
			String dateTime = tokens[4];
			String headLine = tokens[5];
			out[0] = new NewsEvent(timeZone, receiveTime, distributorCode, storyId, symbolList, dateTime, headLine);
			return out;
		}
		return null;
	}
}
