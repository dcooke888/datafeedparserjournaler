package com.JDatabase.IQFeed.L1Events;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import com.JDatabase.Events.DataEvent;
import com.JDatabase.Events.EventType;
import com.JDatabase.Events.TradeEvent;
import com.JDatabase.Events.TOBEvent;
import com.JDatabase.Utils.StringUtils;

public class L1SummaryUpdateMessage{
	
	public static DataEvent[] getL1Events(TimeZone timeZone, long receiveTime, String line)
	{
		String[] tokens = StringUtils.getCommaSplitString(line);
		if(tokens.length == 21)
		{
			String symbol = tokens[1];
			String exchangeId = tokens[4];
			
			// determine which updates are available
			String messageContents = tokens[2];
			boolean newLastUpdate = messageContents.indexOf("C") >= 0;
			boolean newExtendedLastUpdate = messageContents.indexOf("E") >= 0;
			boolean newOtherLastUpdate = messageContents.indexOf("O") >= 0;
			int newBidUpdate = messageContents.indexOf("b");
			int newAskUpdate = messageContents.indexOf("a");
			
			List<DataEvent> out = new ArrayList<DataEvent>();
			String lastUpdateDate = tokens[6];
			
			if(newLastUpdate || newExtendedLastUpdate || newOtherLastUpdate)
			{
				String tradeType = (newLastUpdate) ? "C" : (newExtendedLastUpdate) ? "E" : (newOtherLastUpdate) ? "O" : "";
				String tradeConditions = tokens[3];
				String lastMktCenter = tokens[5];
				String lastUpdateTime = tokens[7];
				double lastPrice = StringUtils.getDoubleFromString(tokens[8], Double.MIN_VALUE);
				int lastSize = StringUtils.getIntFromString(tokens[9], Integer.MIN_VALUE);
				int tickId = StringUtils.getIntFromString(tokens[10], Integer.MIN_VALUE);
				int openInterest = StringUtils.getIntFromString(tokens[11], Integer.MIN_VALUE);
				double vwap = StringUtils.getDoubleFromString(tokens[12], Double.MIN_VALUE);
				try {
					out.add(new TradeEvent(timeZone, receiveTime, symbol, tradeType, tradeConditions, exchangeId, lastMktCenter,
							lastUpdateDate, lastUpdateTime, lastPrice, lastSize, tickId, openInterest, vwap));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			
			if(newBidUpdate >= 0)
			{
				String bidMktCenter = tokens[13];
				String bidUpdateTime = tokens[14];
				double bidPrice = StringUtils.getDoubleFromString(tokens[15], Double.MIN_VALUE);
				int bidSize = StringUtils.getIntFromString(tokens[16], Integer.MIN_VALUE);
				try {
					out.add(new TOBEvent(timeZone, receiveTime, symbol, EventType.BID, exchangeId, bidMktCenter, bidUpdateTime, bidPrice, bidSize));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			
			if(newAskUpdate >= 0)
			{
				String askMktCenter = tokens[17];
				String askUpdateTime = tokens[18];
				double askPrice = StringUtils.getDoubleFromString(tokens[19], Double.MIN_VALUE);
				int askSize = StringUtils.getIntFromString(tokens[20], Integer.MIN_VALUE);
				try {
					out.add(new TOBEvent(timeZone, receiveTime, symbol, EventType.ASK, exchangeId, askMktCenter, askUpdateTime, askPrice, askSize));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			return out.toArray(new DataEvent[out.size()]);
		} else if (tokens.length == 17)  {
			// Older Data format
			// String oldExample = "P,	SPLS,	15.2000,1000,	17:18:53.001,	26,	15970171,	0,     	0,	15.3400,100,15.4700,15.6600,15.2800,15.4850,Cbaohlc,17,"
			// 						0, 	1,  	2,		3,  	4, 				5,	6,  	 	7,     	8,	9,		10, 11,		12, 	13,		14,		15,		16,";
			// 					   "Q,	@ES#,	1784.50,3,  	21:17:35.225,	43, 31927,  	1784.25,211,1784.50,10,	1785.50,1790.50,1784.25,1785.25,b,		01,";
			String symbol = tokens[1];
			String exchangeId = tokens[5];
			
			// determine which updates are available
			String messageContents = tokens[15];
			boolean newLastUpdate = messageContents.indexOf("C") >= 0;
			boolean newExtendedLastUpdate = messageContents.indexOf("E") >= 0;
			boolean newOtherLastUpdate = messageContents.indexOf("O") >= 0;
			int newBidUpdate = messageContents.indexOf("b");
			int newAskUpdate = messageContents.indexOf("a");
			
			List<DataEvent> out = new ArrayList<DataEvent>();
			String lastUpdateDate = "";
			
			if(newLastUpdate || newExtendedLastUpdate || newOtherLastUpdate)
			{
				String tradeType = (newLastUpdate) ? "C" : (newExtendedLastUpdate) ? "E" : (newOtherLastUpdate) ? "O" : "";
				String tradeConditions = tokens[16];
				String lastMktCenter = tokens[5];
				String lastUpdateTime = tokens[4];
				double lastPrice = StringUtils.getDoubleFromString(tokens[2], Double.MIN_VALUE);
				int lastSize = StringUtils.getIntFromString(tokens[3], Integer.MIN_VALUE);
				int tickId = StringUtils.getIntFromString(tokens[6], Integer.MIN_VALUE);
				int openInterest = -1;
				double vwap = -1;
				try {
					out.add(new TradeEvent(timeZone, receiveTime, symbol, tradeType, tradeConditions, exchangeId, lastMktCenter,
							lastUpdateDate, lastUpdateTime, lastPrice, lastSize, tickId, openInterest, vwap));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			
			if(newBidUpdate >= 0)
			{
				String bidMktCenter = "";
				String bidUpdateTime = "";
				double bidPrice = StringUtils.getDoubleFromString(tokens[7], Double.MIN_VALUE);
				int bidSize = StringUtils.getIntFromString(tokens[8], Integer.MIN_VALUE);
				out.add(new TOBEvent(timeZone, receiveTime, symbol, EventType.BID, exchangeId, bidMktCenter, receiveTime, bidPrice, bidSize));
			}
			
			if(newAskUpdate >= 0)
			{
				String askMktCenter = "";
				String askUpdateTime = "";
				double askPrice = StringUtils.getDoubleFromString(tokens[9], Double.MIN_VALUE);
				int askSize = StringUtils.getIntFromString(tokens[10], Integer.MIN_VALUE);
				out.add(new TOBEvent(timeZone, receiveTime, symbol, EventType.ASK, exchangeId, askMktCenter, receiveTime, askPrice, askSize));
			}
			return out.toArray(new DataEvent[out.size()]);
		}
		return null;
	}
	
	
	public static String getSelectionString()
	{
		StringBuilder out = new StringBuilder();
		out.append("S,SELECT UPDATE FIELDS,");
		// FEED TYPE is Automatically added															// 0
		out.append(L1DynamicFieldNames.SYMBOL.toString()).append(","); 								// 1
		out.append(L1DynamicFieldNames.MESSAGE_CONTENTS.toString()).append(",");					// 2
		out.append(L1DynamicFieldNames.MOST_RECENT_TRADE_CONDITIONS.toString()).append(",");		// 3
		out.append(L1DynamicFieldNames.EXCHANGE_ID.toString()).append(",");							// 4
		
		out.append(L1DynamicFieldNames.MOST_RECENT_TRADE_MARKET_CENTER.toString()).append(",");		// 5
		out.append(L1DynamicFieldNames.MOST_RECENT_TRADE_DATE.toString()).append(",");				// 6
		out.append(L1DynamicFieldNames.MOST_RECENT_TRADE_TIME.toString()).append(",");				// 7
		out.append(L1DynamicFieldNames.MOST_RECENT_TRADE.toString()).append(",");					// 8
		out.append(L1DynamicFieldNames.MOST_RECENT_TRADE_SIZE.toString()).append(",");				// 9
		out.append(L1DynamicFieldNames.TICK_ID.toString()).append(",");								// 10
		out.append(L1DynamicFieldNames.OPEN_INTEREST.toString()).append(",");						// 11
		out.append(L1DynamicFieldNames.VWAP.toString()).append(",");								// 12
		
		out.append(L1DynamicFieldNames.BID_MARKET_CENTER.toString()).append(",");					// 13
		out.append(L1DynamicFieldNames.BID_TIME.toString()).append(",");							// 14
		out.append(L1DynamicFieldNames.BID.toString()).append(",");									// 15	
		out.append(L1DynamicFieldNames.BID_SIZE.toString()).append(",");							// 16
		
		out.append(L1DynamicFieldNames.ASK_MARKET_CENTER.toString()).append(",");					// 17
		out.append(L1DynamicFieldNames.ASK_TIME.toString()).append(",");							// 18
		out.append(L1DynamicFieldNames.ASK.toString()).append(",");									// 19
		out.append(L1DynamicFieldNames.ASK_SIZE.toString());										// 20
		out.append("\r\n");
		return out.toString();
	}

}