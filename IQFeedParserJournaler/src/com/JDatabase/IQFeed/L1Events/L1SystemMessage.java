package com.JDatabase.IQFeed.L1Events;

import java.util.TimeZone;

import com.JDatabase.Events.ConnectionEvent;
import com.JDatabase.Events.DataEvent;
import com.JDatabase.IQFeed.Feed;
import com.JDatabase.Utils.StringUtils;

public class L1SystemMessage{
	
	public static DataEvent[] notifySystemMessage(TimeZone timeZone, long receiveTime, String line)
	{
		String[] tokens = StringUtils.getCommaSplitString(line);
		if(tokens.length >= 2)
		{
			L1SystemMessages msgType = L1SystemMessages.fromFeedId(tokens[1]);
			switch(msgType)
			{
			case L1_CURRENT_LOG_LEVELS:
				break;
			case L1_CURRENT_PROTOCOL:
				break;
			case L1_CURRENT_UPDATE_FIELDNAMES:
				break;
			case L1_CUSTOMER:
				break;
			case L1_FUNDAMENTAL_FIELDNAMES:
				break;
			case L1_KEY:
				break;
			case L1_KEY_OK:
				break;
			case L1_RECONNECT_FAILED:
				break;
			case L1_SERVER_CONNECTED:
				return ConnectionEvent.getConnectionEvent(Feed.L1, timeZone, receiveTime, true);
			case L1_SERVER_DISCONNECTED:
				return ConnectionEvent.getConnectionEvent(Feed.L1, timeZone, receiveTime, false);
			case L1_STATS:
				break;
			case L1_SYMBOL_LIMIT_REACHED:
				break;
			case L1_UPDATE_FIELDNAMES:
				break;
			case L1_USED_IPs:
				break;
			case L1_WATCHES:
				break;
			case UNKNOWN:
				break;
			default:
				break;
			
			}
		}
		return null;
	}
	
}
