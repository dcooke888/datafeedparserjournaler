package com.JDatabase.IQFeed.L1Events;

import java.text.ParseException;
import java.util.TimeZone;

import com.JDatabase.Events.DataEvent;
import com.JDatabase.Events.EventType;
import com.JDatabase.Events.TOBEvent;
import com.JDatabase.Utils.StringUtils;

public class L1RegionalMessage{
	
	public static DataEvent[]  getRegionalMessage(TimeZone timeZone, long receiveTime, String line)
	{
		String[] tokens = StringUtils.getCommaSplitString(line);
		if(tokens.length == 12)
		{
			String symbol = tokens[1];
			String exchange = tokens[2];
			String mktCenter = tokens[11];
			DataEvent[] out = new DataEvent[2];
			
			double regionalBid = Double.valueOf(tokens[3]);
			int regionalBidSize = Integer.valueOf(tokens[4]);
			String regionalBidTime = tokens[5];
			try {
				out[0] = new TOBEvent(timeZone, receiveTime, symbol, EventType.REGIONAL_BID, exchange, mktCenter, regionalBidTime, regionalBid, regionalBidSize);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			double regionalAsk = Double.valueOf(tokens[6]);
			int regionalAskSize = Integer.valueOf(tokens[7]);
			String regionalAskTime = tokens[8];
			try {
				out[1] = new TOBEvent(timeZone, receiveTime, symbol, EventType.REGIONAL_ASK, exchange, mktCenter, regionalAskTime, regionalAsk, regionalAskSize);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return out;
//			String fractionDisplayCode = tokens[9];
//			String decimalPrecision = tokens[10];
			
		}
		return null;
	}
}
