package com.JDatabase.IQFeed;

import java.util.TimeZone;

import com.JDatabase.Events.DataEvent;
import com.JDatabase.Port.Listeners.StringMessageListener;
import com.lmax.disruptor.EventFactory;

public class IQFeedUpdateEvent implements StringMessageListener {

	private Feed feed;
	private TimeZone timeZone;
	private long receiveTime;
	private String message;
	private DataEvent[] events;
	private byte[] serializedData;
	
	public Feed getFeedType()			{ return feed;			}
	public TimeZone getTimeZone()		{ return timeZone;		}
	public long getReceiveTime()		{ return receiveTime;	}
	public String getMessage()			{ return message;		}
	public String getJournalStringNoNewLine()	{	return feed.toString().concat(",").concat(timeZone.getID()).concat(",").concat(String.valueOf(receiveTime)).concat(",").concat(message);	}
	public DataEvent[] getEvents()				{ 	return events;			}
	public byte[] getSerializedEvents()	{ return serializedData;	}
	
	public void setSerializedBytes(byte[] serializedData)	{ this.serializedData = serializedData;	}
	public void setEvents(DataEvent[] events)	{	this.events = events;	}
	public void clearEvents() { events = null;	}

	@Override 
	public void notifyStringMessage(Feed feed, TimeZone timeZone, long receiveTime,
			String message) {
		this.feed = feed;
		this.timeZone = timeZone;
		this.receiveTime = receiveTime;
		this.message = message;
	}

    public static final EventFactory<IQFeedUpdateEvent> EVENT_FACTORY = new EventFactory<IQFeedUpdateEvent>()
    {
        public IQFeedUpdateEvent newInstance()
        {
            return new IQFeedUpdateEvent();
        }
    };
	
}
