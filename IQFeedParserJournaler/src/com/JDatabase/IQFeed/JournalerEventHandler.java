package com.JDatabase.IQFeed;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import com.lmax.disruptor.EventHandler;

public class JournalerEventHandler implements EventHandler<IQFeedUpdateEvent>{
	
	BufferedWriter w;

	public JournalerEventHandler(String filename) throws IOException {
		this.w = new BufferedWriter(new FileWriter(filename));
	}

	@Override
	public void onEvent(IQFeedUpdateEvent event, long sequence, boolean endOfBatch) throws Exception {
		w.write(event.getJournalStringNoNewLine().concat("\r\n"));
		if(endOfBatch) w.flush();
		
		times[(int) count--] = System.nanoTime() - event.getReceiveTime();
	}
	
	private long[] times;
    private long count = 0;
	
    public void reset(final long expectedCount)
    {
        times = new long[(int) expectedCount + 1];
        count = expectedCount;
    }
	
	public long[] getTimes(){
		return times;
	}

}
