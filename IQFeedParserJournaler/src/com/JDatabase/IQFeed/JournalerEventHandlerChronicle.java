package com.JDatabase.IQFeed;

import java.io.IOException;
import com.JDatabase.Journaler.String.ChronicleWriter;
import com.JDatabase.Journaler.String.StringJournaler;
import com.lmax.disruptor.EventHandler;

public class JournalerEventHandlerChronicle implements EventHandler<IQFeedUpdateEvent>{
	
	StringJournaler w;

	public JournalerEventHandlerChronicle(String filename) throws IOException {
		this.w = new ChronicleWriter(filename);
	}

	@Override
	public void onEvent(IQFeedUpdateEvent event, long sequence, boolean endOfBatch) throws Exception {
		w.write(event.getFeedType(), event.getTimeZone(), event.getReceiveTime(), event.getMessage());
		times[(int) count--] = System.nanoTime() - event.getReceiveTime();
	}
	
	private long[] times;
    private long count = 0;
	
    public void reset(final long expectedCount)
    {
        times = new long[(int) expectedCount + 1];
        count = expectedCount;
    }
	
	public long[] getTimes(){
		return times;
	}

}
