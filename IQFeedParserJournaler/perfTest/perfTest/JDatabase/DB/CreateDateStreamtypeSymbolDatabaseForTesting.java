package perfTest.JDatabase.DB;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.JDatabase.DB.TicDBMS;
import com.JDatabase.DB.Index.DateStreamTypeSymbolIndex;
import com.JDatabase.Events.DataEvent;
import com.JDatabase.IQFeed.Feed;
import com.JDatabase.IQFeed.Parser;
import com.JDatabase.Utils.DateUtils;

public class CreateDateStreamtypeSymbolDatabaseForTesting {
	public static final String DEFAULT_TEST_DB_FOLDER = System.getProperty("user.dir") + "/testDB/";
	public static final String baseFolderPath = "/Volumes/SSD/iqfeed_data/UnzippedData/11_19_13/";
	public static final String L1FileToParse = baseFolderPath.concat("_L1_JOURNAL_2013_11_19.txt");
	public static final String L2FileToParse = baseFolderPath.concat("_L2_JOURNAL_2013_11_19.txt");
	private static DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd, HH:mm:ss.SSSZ,");
	private static final DateTimeZone tz = DateUtils.est;

	public static void main(String[] args) throws IOException {
		TicDBMS ticDB = new TicDBMS(new DateStreamTypeSymbolIndex(DEFAULT_TEST_DB_FOLDER));
		
		BufferedReader l1Reader = new BufferedReader(new FileReader(L1FileToParse));
		addEventsUntilEmpty(l1Reader, ticDB, Feed.L1);
//		for(int i = 0; i < 1000; i++) {
//			String line = l1Reader.readLine();
//			int idx = (line.substring(0, (line.substring(0,line.lastIndexOf(","))).lastIndexOf(","))).lastIndexOf(",");
////			System.out.println(line.substring(0, idx + 1));
//			DateTime dateTime = formatter.parseDateTime(line.substring(idx + 1));
//			System.out.println(dateTime.getMillis());
//////			System.out.printf("%d,%d,%d,%d,%d,%d,%d%n",dateTime.getYear(), dateTime.getMonthOfYear(), dateTime.getDayOfMonth(),
//////					dateTime.getHourOfDay(), dateTime.getMinuteOfHour(), dateTime.getSecondOfMinute(), dateTime.getMillisOfSecond());
////			DataEvent[] out  = Parser.getDataEvent(Feed.L1, tz.toTimeZone(), dateTime.getMillis(), line.substring(0, idx + 1));
////			if(out != null) for(DataEvent event: out) event.toString();
//		}
		l1Reader.close();
		BufferedReader l2Reader = new BufferedReader(new FileReader(L2FileToParse));
		addEventsUntilEmpty(l2Reader, ticDB, Feed.L2);
//		for(int i = 0; i < 5000; i++) {
//			String line = l2Reader.readLine();
//			int idx = (line.substring(0, (line.substring(0,line.lastIndexOf(","))).lastIndexOf(","))).lastIndexOf(",");
////			System.out.println(line.substring(0,idx + 1));
//			DateTime dateTime = formatter.parseDateTime(line.substring(idx + 1));
//			System.out.println(dateTime.getMillis());
////			System.out.printf("%d,%d,%d,%d,%d,%d,%d%n",dateTime.getYear(), dateTime.getMonthOfYear(), dateTime.getDayOfMonth(),
////					dateTime.getHourOfDay(), dateTime.getMinuteOfHour(), dateTime.getSecondOfMinute(), dateTime.getMillisOfSecond());
////			DataEvent[] out  = Parser.getDataEvent(Feed.L2, tz.toTimeZone(), dateTime.getMillis(), line.substring(0, idx + 1));
////			if(out != null) for(DataEvent event: out) event.toString();
////			else System.out.println(line.substring(0,idx + 1) + dateTime.toString());
//		}
		l2Reader.close();
		ticDB.closeDBForWriting();
	}
	
	private static void addEventsUntilEmpty(BufferedReader reader, TicDBMS ticDB, Feed f) throws IOException {
		String line;
		while((line = reader.readLine()) != null) {
//			System.out.println(line);
			int idx = (line.substring(0, (line.substring(0,line.lastIndexOf(","))).lastIndexOf(","))).lastIndexOf(",");
			DataEvent[] out = null;
			try {
				DateTime dateTime = formatter.parseDateTime(line.substring(idx + 1));
				out = Parser.getDataEvent(f, tz.toTimeZone(), dateTime.getMillis(), line.substring(0, idx + 1));
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				System.out.println(line);
			}

			if(out != null) 
				for(DataEvent event: out) 
					ticDB.write(event);
		}
	}
}