package perfTest.JDatabase.DB;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.JDatabase.DB.TicDBMS;
import com.JDatabase.DB.Index.DateStreamTypeSymbolIndex;
import com.JDatabase.DataSource.TicDataFeed;
import com.JDatabase.DataSource.TicFeedListener;
import com.JDatabase.Events.DepthEvent;
import com.JDatabase.Events.TOBEvent;
import com.JDatabase.Events.TradeEvent;

public class testDateStreamtypeSymbolIndex {
	public static final String DEFAULT_TEST_DB_FOLDER = System.getProperty("user.dir") + "/testDB/";
	public static final String DEFAULT_SSD_TEST_DB_FOLDER = "/Volumes/SSD/iqfeed_data/testDB/";
	
	private TicDBMS ticDB;
	private TicDataFeed[] dataFeeds;
	private long startTime;
	private long endTime;
	private volatile int numRequests = 0;
	private volatile long receivedEvent = 0;
	private volatile boolean streamCompleted;
	
	@Before
	public void setUp() {
		ticDB = new TicDBMS(new DateStreamTypeSymbolIndex(DEFAULT_TEST_DB_FOLDER));
//		ticDB = new TicDBMS(new DateStreamTypeSymbolIndex(DEFAULT_SSD_TEST_DB_FOLDER));
		dataFeeds = new TicDataFeed[1];
		dataFeeds[0] = new TicDataFeed();
		dataFeeds[0].registerTicFeedListener(new TestTicListener());
		startTime = Long.MIN_VALUE;
		endTime = Long.MIN_VALUE;
		receivedEvent = 0;
		numRequests = 0;
	}
	
	@After
	public void tearDown() {
		ticDB = null;
		dataFeeds = null;
		Thread.yield();
	}
	
	@Test
	public void shouldTestTradeStreamOneTickerPerformance() {
		String[] symbols = {"@ES#"};
		streamCompleted = false;
		startTime = System.nanoTime();
		ticDB.streamTrade(dataFeeds, 0, Long.MAX_VALUE, symbols);
		numRequests++;
		waitUntilStreamIsDone();
		endTime = System.nanoTime();
		printThroughputPerformance("Trades_One_Ticker");
	}
	
	@Test
	public void shouldTestTOBStreamOneTickerPerformance() {
		String[] symbols = {"@ES#"};
		streamCompleted = false;
		startTime = System.nanoTime();
		ticDB.streamTOB(dataFeeds, 0, Long.MAX_VALUE, symbols);
		numRequests++;
		waitUntilStreamIsDone();
		endTime = System.nanoTime();
		printThroughputPerformance("TOB_One_Ticker");
	}
	
	@Test
	public void shouldTestDepthStreamOneTickerPerformance() {
		String[] symbols = {"@ES#"};
		streamCompleted = false;
		startTime = System.nanoTime();
		ticDB.streamDepth(dataFeeds, 0, Long.MAX_VALUE, symbols);
		numRequests++;
		waitUntilStreamIsDone();
		endTime = System.nanoTime();
		printThroughputPerformance("Depth_One_Ticker");
	}
	
	@Test
	public void shouldTestTradeTOBStreamOneTickerPerformance() {
		String[] symbols = {"@ES#"};
		streamCompleted = false;
		startTime = System.nanoTime();
		ticDB.streamTradeTOB(dataFeeds, 0, Long.MAX_VALUE, symbols);
		numRequests++;
		waitUntilStreamIsDone();
		endTime = System.nanoTime();
		printThroughputPerformance("Trades/TOB_One_Ticker");
	}
	
	@Test
	public void shouldTestTOBDepthStreamOneTickerPerformance() {
		String[] symbols = {"@ES#"};
		streamCompleted = false;
		startTime = System.nanoTime();
		ticDB.streamTOBDepth(dataFeeds, 0, Long.MAX_VALUE, symbols);
		numRequests++;
		waitUntilStreamIsDone();
		endTime = System.nanoTime();
		printThroughputPerformance("TOB/Depth_One_Ticker");
	}
	
	@Test
	public void shouldTestTradeTOBDepthStreamOneTickerPerformance() {
		String[] symbols = {"@ES#"};
		streamCompleted = false;
		startTime = System.nanoTime();
		ticDB.streamTradeTOBDepth(dataFeeds, 0, Long.MAX_VALUE, symbols);
		numRequests++;
		waitUntilStreamIsDone();
		endTime = System.nanoTime();
		printThroughputPerformance("Trade/TOB/Depth_One_Ticker");
	}
	
	@Test
	public void shouldTestTradeTOBDepthStreamTenTickersPerformance() {
		String[] symbols = {"@ES#", "SPY", "@ESZ13", "QCLF14", "VXX", "@NQZ13", "DIA", "EWZ", "IWM", "QCLG14"};
		streamCompleted = false;
		startTime = System.nanoTime();
		ticDB.streamTrade(dataFeeds, 0, Long.MAX_VALUE, symbols);
		numRequests++;
		waitUntilStreamIsDone();
		endTime = System.nanoTime();
		printThroughputPerformance("Trade/TOB/Depth_10_Tickers");
	}
	
	@Test
	public void shouldTestTradeTOBDepthStreamMultipleLengthsPerformance() {
		String[] symbols = {"@ES#", "QCLF14", "@NQZ13", "@ESZ13", "QCLG14", "QNGZ13", "QHOF14", "QNGF14", "QRBF14", "QPLF14", "@MEZ13", "@JEZ13", "@QMF14", "QHOZ13", "QRBZ13", "SPY", "IWM", "QPAZ13", "VXX", "EEM", "GDX", "EWZ", "UVXY", "EFA", "XLE", "FXI", "XIV", "+CLF14", "DIA", "GLD", "TNA", "QLD", "TZA", "SSO", "TLT", "FAS", "SPXS", "SDS", "VWO", "XLY", "IYR", "IVV", "XOP", "USO", "NUGT", "XLV", "XLI", "TBT", "XLF", "XLP", "MDY", "VOO", "FAZ", "XLB", "OIH", "XLK", "TVIX", "VIXY", "XHB", "SPXU", "+CLG14", "SCO", "SLV", "XLU", "DXJ", "ITB", "+NGZ13", "UNG", "QID", "VGK", "+HOF14", "IWF", "UCO", "VTI", "+NGF14", "EWY", "+RBF14", "QPAF14", "DGAZ", "VNQ", "IWD", "SH", "VEA", "TWM", "BAC", "FB", "EWW", "AGQ", "RSX", "EWG", "EWJ", "C", "GM", "+HOZ13", "KBE", "ABX", "MU", "DBC", "CSCO", "EBAY", "XRT", "INTC", "MSFT", "JPM", "EWC", "GE", "IWN", "AIG", "GILD", "FEZ", "EWA", "F", "USLV", "YHOO", "CMCSA", "QCOM", "PFE", "EMC", "IEFA", "QQQ", "DAL", "GLL", "LOW", "DDD", "AAPL", "EPI", "CLF", "JCP", "IAU", "XOM", "P", "PBR", "@ENYZ13", "MS", "DHI", "T", "ORCL", "FCX", "GG", "MRK", "BBY", "JNJ", "XME", "PG", "SMH", "WFC", "VALE", "PM", "HPQ", "CHK", "AA", "NEM", "KO", "HAL", "TSLA", "TGT", "BMY", "COG", "GMCR", "LEN", "VZ", "MET", "+PLF14", "EWT", "CAT", "DIS", "CVX", "DOW", "MO", "HD", "GRPN", "FOXA", "EWH", "DVN", "MRO", "POT", "PHM", "AMAT", "+RBZ13", "ANR", "DE", "JCI", "SLB", "TSM", "BA", "AXP", "VLO", "KEY", "USB", "FITB", "SCHW", "EPP", "MDLZ", "BRK.B", "BRCM", "X", "FE", "HOLX", "ABT", "ECA", "BP", "BSX", "SPLS", "WMT", "CBS", "BK", "ZNGA", "EWI", "BHI", "CCL", "CX", "ADM", "CRM", "DTV", "HTZ", "BTU", "NOK", "IBM", "GLW", "S", "GNW", "EXC", "CSX", "YUM", "GPS", "RIG", "JNPR", "MGM", "M", "HYG", "AMGN", "CTL", "COP", "MRVL", "XRX", "ATVI", "DZZ", "AEP", "CVS", "SBUX", "LLY", "WLT", "SO", "DUK", "VIG", "ABBV", "TXN", "TEVA", "TWX", "VOD", "ALTR", "GDXJ", "MDT", "APA", "HFC", "FCG", "RF", "NLY", "SPLV", "ESRX", "DD", "KR", "MCD", "EA", "MMM", "JNK", "BAX", "ARUN", "TSN", "UNH", "PHYS", "PRU", "IP", "IVZ", "WAG", "PEP", "GS", "TJX", "COH", "WU", "CAM", "SWN", "AMLP", "AGNC", "AET", "LCC", "CELG", "ADBE", "GIS", "TSO", "KMI", "SYMC", "CTRP", "EWM", "EWS", "UAL", "SNDK", "AES", "NTAP", "NVDA", "OXY", "MT", "KGC", "WMB", "RAD", "NKE", "MOS", "CIEN", "FSLR", "VIAB", "LQD", "TMUS", "ADP", "EMR", "HBAN", "AMZN", "MON", "TSL", "DG", "MYL", "IR", "CA", "KORS", "SQQQ", "SWKS", "COF", "ACN", "MTG", "KSS", "SIRI", "PFF", "LYB", "TWTR", "TWC", "SCTY", "BBRY", "APC", "+PAZ13", "STX", "CAH", "CPB", "NUAN", "NOV", "MPC", "BRCD", "BKLN", "NWSA", "PSX", "SID", "ACI", "HON", "FDX", "AGN", "BIIB", "LVS", "CHRW", "MDR", "CTXS", "PSLV", "MLPN", "HIMX", "BIDU", "HES", "WFM", "CSIQ", "CTSH", "TTWO", "IBB", "SD", "UTX", "NFLX", "IAG", "UPS", "FTR", "EXPE", "ETFC", "HUM", "FLEX", "BYD", "APOL", "NCT", "UNP", "V", "LSI", "ALU", "RAX", "HL", "COST", "SSYS", "EOG", "KMB", "ONNN", "WLP", "URBN", "GOOG", "AMD", "MCHP", "YGE", "UNIS", "PXD", "ARNA", "FST", "CRUS", "DLPH", "ENDP", "YELP", "FIO", "WEN", "MCK", "IGN", "MBI", "SPWR", "TIVO", "QTTZ13", "AMRN", "QIHU", "NEE", "NIHD", "HCBK", "MJN", "ICE", "LNKD", "SINA", "EQIX", "WIN", "ACT", "NXPI", "MDRX", "ILMN", "MCP", "SPG", "APO", "MNKD", "+PAF14", "PCLN", "WYNN", "MA", "TRIP", "DRYS", "CF", "REGN", "RL", "DNDN", "VPHM", "AMAP", "THRX", "NBG", "VOLC", "SNTS", "SQNM", "SREV", "+CLZ13", "DRRX", "QCJH14", "UMC", "PTIE", "NBCB", "ZU", "@QMZ13", "CLF14", "CLG14", "HOF14", "HOZ13", "RBF14", "RBZ13", "PAF14", "PAZ13", "PLF14", "QRBBX13", "QRBBZ14", "QRMX13", "QRMZ14", "AAMRQ", "NGF14", "NGZ13", "AFFY"};
		for(int i = symbols.length; i > 0; i-=10) {
//		int i = symbols.length;
			receivedEvent = 0;
			streamCompleted = false;
			String[] toStream = new String[i];
			System.arraycopy(symbols, 0, toStream, 0, i);
			startTime = System.nanoTime();
			ticDB.streamTrade(dataFeeds, 0, Long.MAX_VALUE, toStream);
			numRequests++;
			waitUntilStreamIsDone();
			endTime = System.nanoTime();
			printThroughputPerformance("Trade/TOB/Depth_"+i+"_Tickers");
		}
	}
	
	private void waitUntilStreamIsDone() {
		while(numRequests > 0 && !streamCompleted) {
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void printThroughputPerformance(String testName) {
		double totalSecs = (endTime - startTime) / (1000.0 * 1000.0 * 1000.0);
		System.out.printf("%s - ReceivedEvents: %d, Secs: %.2f, EventsPerSec: %.2f%n", testName, receivedEvent, totalSecs, receivedEvent / totalSecs);
		
	}

	
	class TestTicListener extends TicFeedListener {

		public TestTicListener() {
			super(true, true, true);
		}

		@Override
		public void notify(TradeEvent event) {
			receivedEvent++;
		}

		@Override
		public void notify(TOBEvent event) {
			receivedEvent++;
		}

		@Override
		public void notify(DepthEvent event) {
			receivedEvent++;
		}

		@Override
		public void notifyStreamCompleted(int streamReqId) {
			endTime = System.nanoTime();
			numRequests--;
			streamCompleted = true;
			
		}
		
	}
}
