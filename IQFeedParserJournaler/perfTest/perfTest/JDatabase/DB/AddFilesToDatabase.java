package perfTest.JDatabase.DB;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.JDatabase.DB.TicDBMS;
import com.JDatabase.DB.Index.DateStreamTypeSymbolIndex;
import com.JDatabase.Events.DataEvent;
import com.JDatabase.IQFeed.Feed;
import com.JDatabase.IQFeed.Parser;
import com.JDatabase.Utils.DateUtils;
import com.JDatabase.Utils.FileUtilities;

public class AddFilesToDatabase {
	public static final String DEFAULT_TEST_DB_FOLDER = new File(".").getAbsolutePath();
	private static DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd, HH:mm:ss.SSSZ,");
	private static final DateTimeZone tz = DateUtils.est;

	public static void main(String[] args) throws IOException {
		String folderName = DEFAULT_TEST_DB_FOLDER.substring(0, DEFAULT_TEST_DB_FOLDER.length() - 1);
		System.out.println(folderName);
		String[] filenames = FileUtilities.getAbsoluteFileNamesOfType(folderName, ".txt");
		TicDBMS ticDB = new TicDBMS(new DateStreamTypeSymbolIndex(folderName));
		for(String filename: filenames)
		{
			System.out.printf("%s started%n", filename);
			if(filename.contains("L1_JOURNAL"))
			{
				BufferedReader l1Reader = new BufferedReader(new FileReader(filename));
				addEventsUntilEmpty(l1Reader, ticDB, Feed.L1);
				l1Reader.close();
				System.out.printf("%s completed%n", filename);
			} 
			else if (filename.contains("L1_JOURNAL"))
			{
				BufferedReader l2Reader = new BufferedReader(new FileReader(filename));
				addEventsUntilEmpty(l2Reader, ticDB, Feed.L2);
				l2Reader.close();
				System.out.printf("%s completed%n", filename);
			}
			else
			{
				System.out.printf("%s is not a valid file%n", filename);
			}
		}
		ticDB.closeDBForWriting();
	}
	
	private static void addEventsUntilEmpty(BufferedReader reader, TicDBMS ticDB, Feed f) throws IOException {
		String line;
		while((line = reader.readLine()) != null) {
//			System.out.println(line);
			int idx = (line.substring(0, (line.substring(0,line.lastIndexOf(","))).lastIndexOf(","))).lastIndexOf(",");
			DataEvent[] out = null;
			try {
				DateTime dateTime = formatter.parseDateTime(line.substring(idx + 1));
				out = Parser.getDataEvent(f, tz.toTimeZone(), dateTime.getMillis(), line.substring(0, idx + 1));
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				System.out.printf("DATE PARSE ERROR: %s",line);
			}

			if(out != null) 
				for(DataEvent event: out) 
					ticDB.write(event);
		}
	}
}