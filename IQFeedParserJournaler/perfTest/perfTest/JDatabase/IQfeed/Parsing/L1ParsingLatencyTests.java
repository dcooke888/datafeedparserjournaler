package perfTest.JDatabase.IQfeed.Parsing;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.TimeZone;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.JDatabase.Events.DataEvent;
import com.JDatabase.IQFeed.Feed;
import com.JDatabase.IQFeed.L1Events.L1Parser;

public class L1ParsingLatencyTests {
	String[] tenLinesToParse = {
			"Q,AAPL,ba,17,5,26,12/16/2013,09:02:56.764,557.2100,100,8511,,555.8517,26,09:03:05.963,557.2500,300,26,09:03:05.963,557.3800,500,",
			"Q,AAPL,E,17,5,19,12/16/2013,09:03:07.075,557.3800,250,8522,,555.8553,26,09:03:05.963,557.2500,300,26,09:03:05.963,557.3800,500,",
			"Q,AAPL,b,17,5,19,12/16/2013,09:03:07.075,557.3800,250,8522,,555.8553,5,09:03:10.099,557.3000,100,26,09:03:10.099,557.3800,500,",
			"Q,@ES#,a,01,22,43,12/16/2013,09:03:11.023,1778.00,1,1704475,1446253,,43,09:03:11.023,1777.75,232,43,09:03:11.366,1778.00,186,",
			"Q,AAPL,O,173D,5,5,12/16/2013,09:03:11.668,557.3000,100,8528,,555.8567,5,09:03:10.099,557.3000,100,26,09:03:10.099,557.3800,500,",
			"Q,AAPL,C,173D,5,5,12/16/2013,09:03:11.668,557.3000,100,8529,,555.8581,5,09:03:10.099,557.3000,100,26,09:03:10.099,557.3800,500,",
			"Q,AAPL,Cba,173D,5,5,12/16/2013,09:03:11.668,557.3000,100,8529,,555.8581,26,09:03:11.669,557.2500,300,26,09:03:11.669,557.3800,500,",
			"Q,AAPL,ba,17,5,26,12/16/2013,09:02:56.764,557.2100,100,8511,,555.8517,26,09:03:05.963,557.2500,300,26,09:03:05.963,557.3800,500,",
			"Q,AAPL,E,17,5,19,12/16/2013,09:03:07.075,557.3800,250,8522,,555.8553,26,09:03:05.963,557.2500,300,26,09:03:05.963,557.3800,500,",
			"Q,@ES#,a,01,22,43,12/16/2013,09:03:11.023,1778.00,1,1704475,1446253,,43,09:03:11.023,1777.75,232,43,09:03:11.366,1778.00,186,"
	};
	String singleMsg = "Q,AAPL,ba,17,5,26,12/16/2013,09:02:56.764,557.2100,100,8511,,555.8517,26,09:03:05.963,557.2500,300,26,09:03:05.963,557.3800,500,";
	
    final int warmup = 5000 * 100;
    final int repeats = 5000 * 1000;
	int[] times;
	
	

	@Before
	public void setUp() throws Exception {
		times = new int[repeats];
	}

	@After
	public void tearDown() throws Exception {
		times = null;
	}

	@Test
	public void testSingleLineLatency() {	
		long startTime, endTime;
		DataEvent[] out;
		for (int count = -warmup; count < repeats; count++) {
			out = null;
			TimeZone timeZone = TimeZone.getDefault();
			long receiveTime = System.currentTimeMillis();
			startTime = System.nanoTime();
			out = L1Parser.getParsedMessage(Feed.L1, timeZone, receiveTime, singleMsg);
			endTime = System.nanoTime();
	        if (count >= 0)	times[count] = (int) (endTime - startTime);
	        assertFalse(out[0] == null);
		}
	    Arrays.sort(times);
	    System.out.print("SingleLineLatency: ");
	    for (double perc : new double[]{50, 90, 99, 99.9, 99.99}) {
	        System.out.printf("%s%% took %.1f �s, ", perc, (double) times[((int) (repeats * perc / 100))] / 1000.0);
	    }
	    System.out.printf("worst took %d �s%n", times[times.length - 1] / 1000);
	}
	@Test
	public void testBatchLineLatency() {	
		long startTime, endTime;
		DataEvent[] out;
		for (int count = -warmup; count < repeats; count++) {
			out = null;
			TimeZone timeZone = TimeZone.getDefault();
			long receiveTime = System.currentTimeMillis();
			Thread.yield();
			startTime = System.nanoTime();
			for(String message: tenLinesToParse)
			{
				out = L1Parser.getParsedMessage(Feed.L1, timeZone, receiveTime, message);
			}
			endTime = System.nanoTime();
			
	        if (count >= 0)	times[count] = (int) (endTime - startTime);
	        assertFalse(out[0] == null);
		}
	    Arrays.sort(times);
	    System.out.print("BatchLineLatency: ");
	    for (double perc : new double[]{50, 90, 99, 99.9, 99.99}) {
	        System.out.printf("%s%% took %.1f �s, ", perc, (double) times[((int) (repeats * perc / 100))] / 1000.0 / 10);
	    }
	    System.out.printf("worst took %d �s%n", times[times.length - 1] / 1000);
	}

}
