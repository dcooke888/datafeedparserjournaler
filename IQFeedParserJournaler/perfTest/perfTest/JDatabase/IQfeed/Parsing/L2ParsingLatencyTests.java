package perfTest.JDatabase.IQfeed.Parsing;

import static org.junit.Assert.assertFalse;

import java.util.Arrays;
import java.util.TimeZone;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.JDatabase.Events.DataEvent;
import com.JDatabase.IQFeed.Feed;
import com.JDatabase.IQFeed.L2Events.L2Parser;

public class L2ParsingLatencyTests {
	String[] tenLinesToParse = {
			"Z,@ES#,MD06,1776.5,1779.25,554,592,09:03:10.266,2013-12-16,52,09:03:10.334,T,T,T,",
			"Z,@ES#,MD10,1775.5,1780.25,844,719,09:03:08.430,2013-12-16,52,09:03:08.430,T,T,T,",
			"Z,@ES#,MD07,1776.25,1779.5,727,757,09:03:08.010,2013-12-16,52,09:03:08.010,T,T,T,",
			"Z,@ES#,MD01,1777.75,1778.,232,185,09:03:11.023,2013-12-16,52,09:03:11.023,T,T,T,",
			"Z,@ES#,MD02,1777.5,1778.25,832,974,09:03:08.429,2013-12-16,52,09:03:07.734,T,T,T,",
			"Z,@ES#,MD03,1777.25,1778.5,731,871,09:03:08.429,2013-12-16,52,09:03:07.810,T,T,T,",
			"Z,@ES#,MD08,1776.,1779.75,773,703,09:03:08.728,2013-12-16,52,09:03:08.100,T,T,T,",
			"Z,@ES#,MD09,1775.75,1780.,573,995,09:03:08.191,2013-12-16,52,09:03:08.191,T,T,T,",
			"2,@ES#,MD06,1776.5,1779.25,552,592,09:03:12.092,2013-12-16,52,09:03:10.334,T,T,T,",
			"2,@ES#,MD05,1776.75,1779.,908,677,09:03:12.236,2013-12-16,52,09:03:07.867,T,T,T,",		
	};
	String singleMsg = "2,@ES#,MD05,1776.75,1779.,908,677,09:03:12.236,2013-12-16,52,09:03:07.867,T,T,T,";
	
    final int warmup = 5000 * 100;
    final int repeats = 5000 * 1000;
	int[] times;
	
	

	@Before
	public void setUp() throws Exception {
		times = new int[repeats];
	}

	@After
	public void tearDown() throws Exception {
		times = null;
	}

	@Test
	public void testSingleLineLatency() {	
		long startTime, endTime;
		DataEvent[] out;
		for (int count = -warmup; count < repeats; count++) {
			out = null;
			TimeZone timeZone = TimeZone.getDefault();
			long receiveTime = System.currentTimeMillis();
			startTime = System.nanoTime();
			out = L2Parser.getParsedMessage(Feed.L2, timeZone, receiveTime, singleMsg);
			endTime = System.nanoTime();
	        if (count >= 0)	times[count] = (int) (endTime - startTime);
	        assertFalse(out[0] == null);
		}
	    Arrays.sort(times);
	    System.out.print("SingleLineLatency: ");
	    for (double perc : new double[]{50, 90, 99, 99.9, 99.99}) {
	        System.out.printf("%s%% took %.1f �s, ", perc, (double) times[((int) (repeats * perc / 100))] / 1000.0);
	    }
	    System.out.printf("worst took %d �s%n", times[times.length - 1] / 1000);
	}
	@Test
	public void testBatchLineLatency() {	
		long startTime, endTime;
		DataEvent[] out;
		for (int count = -warmup; count < repeats; count++) {
			out = null;
			TimeZone timeZone = TimeZone.getDefault();
			long receiveTime = System.currentTimeMillis();
			Thread.yield();
			startTime = System.nanoTime();
			for(String message: tenLinesToParse)
			{
				out = L2Parser.getParsedMessage(Feed.L1, timeZone, receiveTime, message);
			}
			endTime = System.nanoTime();
			
	        if (count >= 0)	times[count] = (int) (endTime - startTime);
	        assertFalse(out[0] == null);
		}
	    Arrays.sort(times);
	    System.out.print("BatchLineLatency: ");
	    for (double perc : new double[]{50, 90, 99, 99.9, 99.99}) {
	        System.out.printf("%s%% took %.1f �s, ", perc, (double) times[((int) (repeats * perc / 100))] / 1000.0 / 10);
	    }
	    System.out.printf("worst took %d �s%n", times[times.length - 1] / 1000);
	}

}