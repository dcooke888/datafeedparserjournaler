package perfTest.JDatabase.IQfeed;

import static com.lmax.disruptor.RingBuffer.createSingleProducer;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.TimeZone;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.JDatabase.IQFeed.CountDownEventHandler;
import com.JDatabase.IQFeed.Feed;
import com.JDatabase.IQFeed.IQFeedUpdateEvent;
import com.JDatabase.IQFeed.JournalerEventHandler;
import com.JDatabase.IQFeed.ParserEventHandler;
import com.JDatabase.IQFeed.UnsafeSerializedEventHandler;
import com.lmax.disruptor.BatchEventProcessor;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.SequenceBarrier;
import com.lmax.disruptor.SleepingWaitStrategy;

public class DisruptorOneToThreeDiamond {
    private static final int NUM_EVENT_PROCESSORS = 4;
    private static final int BUFFER_SIZE = 1024 * 8;
    private static final long ITERATIONS = 1000L * 1000L * 5;
    private final ExecutorService executor = Executors.newFixedThreadPool(NUM_EVENT_PROCESSORS);
    private static final String toParse = "Q,AAPL,Cba,173D,5,5,12/16/2013,09:03:11.668,557.3000,100,8529,,555.8581,26,09:03:11.669,557.2500,300,26,09:03:11.669,557.3800,500,";
	 
	    ///////////////////////////////////////////////////////////////////////////////////////////////

	    private final RingBuffer<IQFeedUpdateEvent> ringBuffer;
	    private final SequenceBarrier stepOneSequenceBarrier;
	    private final JournalerEventHandler stepOneFunctionHandler;
	    private final BatchEventProcessor<IQFeedUpdateEvent> stepOneBatchProcessor;

	    private final ParserEventHandler stepTwoFunctionHandler;
	    private final BatchEventProcessor<IQFeedUpdateEvent> stepTwoBatchProcessor;

	    private final SequenceBarrier stepThreeSequenceBarrier;
	    private final UnsafeSerializedEventHandler stepThreeFunctionHandler;
	    private final BatchEventProcessor<IQFeedUpdateEvent> stepThreeBatchProcessor;
	    
	    private final SequenceBarrier stepFourSequenceBarrier;
	    private final CountDownEventHandler stepFourFunctionHandler;
	    private final BatchEventProcessor<IQFeedUpdateEvent> stepFourBatchProcessor;

	    ///////////////////////////////////////////////////////////////////////////////////////////////
	 
	public DisruptorOneToThreeDiamond() throws IOException
	{
			ringBuffer = createSingleProducer(IQFeedUpdateEvent.EVENT_FACTORY, BUFFER_SIZE, new SleepingWaitStrategy());
			stepOneSequenceBarrier = ringBuffer.newBarrier();
			stepOneFunctionHandler = new JournalerEventHandler(File.createTempFile("temp", ".tmp").getAbsolutePath());
			stepOneBatchProcessor = new BatchEventProcessor<IQFeedUpdateEvent>(ringBuffer, stepOneSequenceBarrier, stepOneFunctionHandler);
			
			stepTwoFunctionHandler = new ParserEventHandler();
			stepTwoBatchProcessor = new BatchEventProcessor<IQFeedUpdateEvent>(ringBuffer, stepOneSequenceBarrier, stepTwoFunctionHandler);

			stepThreeSequenceBarrier = ringBuffer.newBarrier(stepTwoBatchProcessor.getSequence());
			stepThreeFunctionHandler = new UnsafeSerializedEventHandler(1024 * 100);
			stepThreeBatchProcessor = new BatchEventProcessor<IQFeedUpdateEvent>(ringBuffer, stepThreeSequenceBarrier, stepThreeFunctionHandler);
			
			stepFourSequenceBarrier = ringBuffer.newBarrier(stepThreeBatchProcessor.getSequence(), stepOneBatchProcessor.getSequence());
			stepFourFunctionHandler = new CountDownEventHandler();
			stepFourBatchProcessor = new BatchEventProcessor<IQFeedUpdateEvent>(ringBuffer, stepFourSequenceBarrier, stepFourFunctionHandler);
			{
			    ringBuffer.addGatingSequences(stepFourBatchProcessor.getSequence());
			}
	}

	public void test() throws InterruptedException {
		TimeZone defaultTimeZone = TimeZone.getDefault();
        CountDownLatch latch = new CountDownLatch(1);
        stepOneFunctionHandler.reset(ITERATIONS);
        stepTwoFunctionHandler.reset(ITERATIONS);
        stepThreeFunctionHandler.reset(ITERATIONS);
        stepFourFunctionHandler.reset(latch, ITERATIONS);

        executor.submit(stepOneBatchProcessor);
        executor.submit(stepTwoBatchProcessor);
        executor.submit(stepThreeBatchProcessor);
        executor.submit(stepFourBatchProcessor);

        long start = System.currentTimeMillis();
        
        long pauseTimeNs = 1000L;

        for (long i = 0; i < ITERATIONS; i++)
        {
            long sequence = ringBuffer.next();
            IQFeedUpdateEvent event = ringBuffer.get(sequence);
            long time = System.nanoTime();
            event.notifyStringMessage(Feed.L1, defaultTimeZone, time, toParse);
            while (pauseTimeNs > (System.nanoTime() - time))
            {
                Thread.yield();
            }
            ringBuffer.publish(sequence);
        }

        latch.await();
        long opsPerSecond = (ITERATIONS * 1000L) / (System.currentTimeMillis() - start);

        stepOneBatchProcessor.halt();
        stepTwoBatchProcessor.halt();
        stepThreeBatchProcessor.halt();
        stepFourBatchProcessor.halt();
        System.out.println("OPS Per Second " + opsPerSecond);
    	int numBytes = toParse.getBytes().length;
    	System.out.println("Message Size(bytes): " + numBytes);
    	System.out.printf("TotalThroughput (KB/S): %.1f%n", (double) numBytes * opsPerSecond / 1000.0);
    	printLatency("Journaler", stepOneFunctionHandler.getTimes());
    	printLatency("Parser", stepTwoFunctionHandler.getTimes());
    	printLatency("Serializer", stepThreeFunctionHandler.getTimes());
    	printLatency("OverallTime", stepFourFunctionHandler.getTimes());

    }
	
	private void printLatency(String name, long[] times)
	{
        Arrays.sort(times);
        System.out.printf("%s  %n", name);
        for (double perc : new double[]{50, 90, 99, 99.9, 99.99}) {
        	System.out.printf("%s%% took %.1f �s, ", perc, times[((int) (ITERATIONS * perc / 100))] / 1000.0);
        }
        System.out.printf("worst took %d �s%n", times[times.length - 1] / 1000);
	}

    public static void main(String[] args) throws Exception
    {
        new DisruptorOneToThreeDiamond().test();
    }
}
