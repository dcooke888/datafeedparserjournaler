package perfTest.JDatabase.IQfeed.Serialization;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.TimeZone;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.JDatabase.Events.DataEvent;
import com.JDatabase.Events.EventType;
import com.JDatabase.Events.TradeEvent;
import com.JDatabase.Events.TOBEvent;
import com.JDatabase.IQFeed.Feed;
import com.JDatabase.IQFeed.IQFeedUpdateEvent;
import com.JDatabase.IQFeed.L1Events.L1Parser;
import com.JDatabase.Serialize.DirectBufferSerializer;
import com.JDatabase.Serialize.Serializer;
import com.JDatabase.Serialize.UnsafeMemory;

public class SerializationTests {
	Serializer serializer;
	TradeEvent last;
	TOBEvent bid;
	TOBEvent ask;
    final int warmup = 5000 * 100;
    final int repeats = 5000 * 1000;
	int[] times;

	@Before
	public void setUp() throws Exception {
		String L1_UPDATE_MESSAGE = "Q,AAPL,Cba,173D,5,5,12/16/2013,09:03:11.668,557.3000,100,8529,,555.8581,26,09:03:11.669,557.2500,300,26,09:03:11.669,557.3800,500,";
		TimeZone timeZone = TimeZone.getDefault();
		long receiveTime = System.currentTimeMillis();
		IQFeedUpdateEvent iqFeedUpdate = IQFeedUpdateEvent.EVENT_FACTORY.newInstance();
		iqFeedUpdate.notifyStringMessage(Feed.L1, timeZone, receiveTime, L1_UPDATE_MESSAGE);
		DataEvent[] out = L1Parser.getParsedMessage(iqFeedUpdate.getFeedType(), iqFeedUpdate.getTimeZone(),
					iqFeedUpdate.getReceiveTime(), iqFeedUpdate.getMessage());
		last = (TradeEvent) out[0];
		bid = (TOBEvent) out[1];
		ask = (TOBEvent) out[2];
		times = new int[repeats];
	}

	@After
	public void tearDown() throws Exception {
		last = null;
		bid = null;
		ask = null;
	}

	@Test
	public void testUnsafe() {
		serializer = new UnsafeMemory(1024 * 1000);
		long overallStart = System.currentTimeMillis(), startTime, endTime;
		for (int count = -warmup; count < repeats; count++) {
			startTime = System.nanoTime();
			last.write(serializer);
			bid.write(serializer);
			ask.write(serializer);
			serializer.flip();
			TradeEvent lastRead = last.read(serializer, EventType.fromByteId(serializer.getByte()));
			TOBEvent bidRead = bid.read(serializer, EventType.fromByteId(serializer.getByte()));
			TOBEvent askRead = ask.read(serializer, EventType.fromByteId(serializer.getByte()));
			assertTrue(last.equals(lastRead));
			assertTrue(bid.equals(bidRead));
			assertTrue(ask.equals(askRead));
			serializer.clear();
			endTime = System.nanoTime();
			
	        if (count >= 0)	times[count] = (int) (endTime - startTime);
		}
		long overallTime = System.currentTimeMillis() - overallStart;
	    Arrays.sort(times);
	    System.out.println("UnsafeMemorySerialization: Total Time: " + overallTime);
	    for (double perc : new double[]{50, 90, 99, 99.9, 99.99}) {
	        System.out.printf("%s%% took %.1f �s, ", perc, (double) times[((int) (repeats * perc / 100))] / 1000.0 / 10);
	    }
	    System.out.printf("worst took %d �s%n", times[times.length - 1] / 1000);
	}
	
	@Test
	public void testDirectBuffer() {
		serializer = new DirectBufferSerializer(1024 * 1000);
		long overallStart = System.currentTimeMillis(), startTime, endTime;
		for (int count = -warmup; count < repeats; count++) {
			startTime = System.nanoTime();
			last.write(serializer);
			bid.write(serializer);
			ask.write(serializer);
			serializer.flip();
			TradeEvent lastRead = last.read(serializer, EventType.fromByteId(serializer.getByte()));
			TOBEvent bidRead = bid.read(serializer, EventType.fromByteId(serializer.getByte()));
			TOBEvent askRead = ask.read(serializer, EventType.fromByteId(serializer.getByte()));
			assertTrue(last.equals(lastRead));
			assertTrue(bid.equals(bidRead));
			assertTrue(ask.equals(askRead));
			serializer.clear();
			endTime = System.nanoTime();
			
	        if (count >= 0)	times[count] = (int) (endTime - startTime);
		}
		long overallTime = System.currentTimeMillis() - overallStart;
	    Arrays.sort(times);
	    System.out.println("DirectBufferSerialization: Total Time: " + overallTime);
	    for (double perc : new double[]{50, 90, 99, 99.9, 99.99}) {
	        System.out.printf("%s%% took %.1f �s, ", perc, (double) times[((int) (repeats * perc / 100))] / 1000.0 / 10);
	    }
	    System.out.printf("worst took %d �s%n", times[times.length - 1] / 1000);
	}

}
