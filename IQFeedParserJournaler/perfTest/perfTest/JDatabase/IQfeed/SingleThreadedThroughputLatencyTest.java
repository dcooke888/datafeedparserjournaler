package perfTest.JDatabase.IQfeed;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.TimeZone;

import com.JDatabase.Events.DataEvent;
import com.JDatabase.IQFeed.Feed;
import com.JDatabase.IQFeed.L1Events.L1Parser;
import com.JDatabase.IQFeed.L2Events.L2Parser;
import com.JDatabase.Journaler.String.ChronicleWriter;
import com.JDatabase.Serialize.Serializer;
import com.JDatabase.Serialize.UnsafeMemory;

public class SingleThreadedThroughputLatencyTest {

    private static final long ITERATIONS = 1000L * 1000L * 5;
    private long[] times;
    private int length;
    
    private final ChronicleWriter writer;
    private final Serializer serializer;
    
    private static final String toParse = "Q,AAPL,Cba,173D,5,5,12/16/2013,09:03:11.668,557.3000,100,8529,,555.8581,26,09:03:11.669,557.2500,300,26,09:03:11.669,557.3800,500,";
    private static final TimeZone timeZone = TimeZone.getDefault();
    private static final long receiveTime = System.currentTimeMillis();
    
    public SingleThreadedThroughputLatencyTest() throws IOException
    {
    	writer = new ChronicleWriter(File.createTempFile("temp", ".tmp").getAbsolutePath());
    	serializer = new UnsafeMemory(1024 * 100);
    	times = new long[(int) ITERATIONS];

    }
    
    public void runTest(){
    	long start, end;
    	long overallStart = System.currentTimeMillis(), overallEnd;
    	for(int i = 0; i < ITERATIONS; i++)
    	{
    		start = System.nanoTime();
    		process(toParse);
    		end = System.nanoTime();
    		times[i] = end-start;
    	}
    	overallEnd = System.currentTimeMillis();
    	writer.close();
    	
        long opsPerSecond = (ITERATIONS * 1000L) / (overallEnd - overallStart);
        System.out.println("OPS Per Second " + opsPerSecond);
    	int numBytes = toParse.getBytes().length;
    	System.out.println("Message Size(bytes): " + numBytes);
    	System.out.printf("TotalThroughput (KB/S): %.1f%n", (double) numBytes * opsPerSecond / 1000.0);
	    Arrays.sort(times);
	    System.out.print("SingleThreadedLatency: ");
	    for (double perc : new double[]{50, 90, 99, 99.9, 99.99}) {
	        System.out.printf("%s%% took %.1f �s, ", perc, (double) times[((int) (ITERATIONS * perc / 100))] / 1000.0);
	    }
	    System.out.printf("worst took %d �s%n", times[times.length - 1] / 1000);
	    System.out.println(length);
    }


	private void process(String toParse) {
		writer.write(Feed.L1, timeZone, receiveTime, toParse);
		DataEvent[] out = getDataEvents(Feed.L1, timeZone, receiveTime, toParse);
		for(DataEvent event: out)
		{
			event.write(serializer);
		}
		byte[] data = serializer.getWrittenBytes();
		length += data.length;
	}
	
	private DataEvent[] getDataEvents(Feed feed, TimeZone t, long receiveTime, String message) {
		switch(feed)
		{
		case L1:
			return L1Parser.getParsedMessage(feed, t, receiveTime, message);
		case L2:
			return L2Parser.getParsedMessage(feed, t, receiveTime, message);
		case ADMIN: 
			break;
		case LOOKUP:
			break;
		default:
			break;
		
		}
		return null;
	}
	
	public static void main(String[] args) throws IOException
	{
		new SingleThreadedThroughputLatencyTest().runTest();
	}
}
