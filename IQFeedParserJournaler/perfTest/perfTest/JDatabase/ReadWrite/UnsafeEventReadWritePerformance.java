package perfTest.JDatabase.ReadWrite;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.TimeZone;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.JDatabase.Byte.Reader.ByteReader;
import com.JDatabase.Byte.Reader.FileStreamReader;
import com.JDatabase.Byte.Reader.LZ4StreamReader;
import com.JDatabase.Byte.Reader.SnappyStreamReader;
import com.JDatabase.Byte.Writer.ByteWriter;
import com.JDatabase.Byte.Writer.FileStreamWriter;
import com.JDatabase.Byte.Writer.LZ4StreamWriter;
import com.JDatabase.Byte.Writer.SnappyStreamWriter;
import com.JDatabase.Events.DataEvent;
import com.JDatabase.Events.Serialize.EventDeserializer;
import com.JDatabase.IQFeed.Feed;
import com.JDatabase.IQFeed.L1Events.L1Parser;
import com.JDatabase.Serialize.Serializer;
import com.JDatabase.Serialize.UnsafeMemory;

public class UnsafeEventReadWritePerformance {
	static final String L1LineToParse = "Q,AAPL,Cba,173D,5,5,12/16/2013,09:03:11.668,557.3000,100,8529,,555.8581,26,09:03:11.669,557.2500,300,26,09:03:11.669,557.3800,500,";
    final int warmup = 5000 * 100;
    final int repeats = 5000 * 1000 * 2;
    final int bufferSize = 1024 * 1000;
    String filename = null;
    Serializer serializer = null;
    Serializer serializerBuffer = null;
	long bytesRead;
	int count = 0;
	
	public static final DataEvent[] events;
	static {
		events = L1Parser.getParsedMessage(Feed.L1, TimeZone.getDefault(), System.currentTimeMillis(), L1LineToParse);
	}

	@Before
	public void setUp() throws Exception {
		bytesRead = 0;
		filename = File.createTempFile("Temp", ".tmp").getAbsolutePath();
//		filename = "/Volumes/SSD/temp" + count++;
		serializer = new UnsafeMemory(1024 * 100);
		serializerBuffer = new UnsafeMemory(1024*100);
	}

	@After
	public void tearDown() throws Exception {
		filename = null;
		serializer = null;
		serializerBuffer = null;
	}
	
	private void runWriteTest(String testName, ByteWriter writer) throws IOException {
		for (int count = -warmup; count < 0; count++) {
			for(int i = 0; i < 3; i++) writer.write(events[i]);
		}
		writer.flush();
		long overallStartTime = System.currentTimeMillis();
		for (int count = 0; count < repeats; count++)
			for(int i = 0; i < 3; i++) bytesRead += writer.write(events[i]);
		writer.close();
		long overallEndTime = System.currentTimeMillis();
		System.out.printf("Write Perf: %s - Total Time(sec): %.3f, Total KB: %d, KB/Sec: %.2f%n", testName, (overallEndTime - overallStartTime) / 1000.0, bytesRead / 1000, bytesRead / (double) (overallEndTime - overallStartTime));
	}
	
	private void runReadTest(String testName, ByteReader reader) throws Exception {
		byte[] readEvent;
		DataEvent event;
		for (int count = -warmup; count < 0; count++) {
			for(int i = 0; i < 3; i++) {
				readEvent = reader.readSingleEvent();
				serializerBuffer.clearAndPutWrittenBytes(readEvent);
				event = EventDeserializer.deserializeEvent(serializerBuffer);
				assertTrue(event.equals(events[i]));
			}
		}
		long overallStartTime = System.currentTimeMillis();
		for (int count = 0; count < repeats; count++) {
			for(int i = 0; i < 3; i++) {
				readEvent = reader.readSingleEvent();
				serializerBuffer.clearAndPutWrittenBytes(readEvent);
				event = EventDeserializer.deserializeEvent(serializerBuffer);
				assertTrue(event.equals(events[i]));
			}
		}
		long overallEndTime = System.currentTimeMillis();
		System.out.printf("Read Perf: %s - Total Time(sec): %.3f, Total KB: %d, KB/Sec: %.2f%n", testName, (overallEndTime - overallStartTime) / 1000.0, bytesRead / 1000, bytesRead / (double) (overallEndTime - overallStartTime));
	}
	
	@Test
	public void testFileStreamReadWritePerformance() throws Exception {
		String testName = "FileStream";
		bytesRead = 0;
		runWriteTest(testName, new FileStreamWriter(filename, serializer));
		runReadTest(testName, new FileStreamReader(filename, bufferSize));
	}

	@Test
	public void testSnappyEventReadWritePerformance() throws Exception {
		String testName = "Snappy";
		runWriteTest(testName, new SnappyStreamWriter(filename, serializer));
		runReadTest(testName, new SnappyStreamReader(filename, bufferSize));
	}
	
	@Test
	public void testLZ4EventReadWritePerformance() throws Exception {
		String testName = "LZ4";
		runWriteTest(testName, new LZ4StreamWriter(filename, serializer));
		runReadTest(testName, new LZ4StreamReader(filename, bufferSize));
	}
}
