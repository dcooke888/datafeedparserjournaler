package perfTest.JDatabase.ReadWrite;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.JDatabase.Byte.Reader.ByteReader;
import com.JDatabase.Byte.Reader.FileStreamReader;
import com.JDatabase.Byte.Reader.LZ4StreamReader;
import com.JDatabase.Byte.Reader.SnappyStreamReader;
import com.JDatabase.Byte.Writer.ByteWriter;
import com.JDatabase.Byte.Writer.FileStreamWriter;
import com.JDatabase.Byte.Writer.LZ4StreamWriter;
import com.JDatabase.Byte.Writer.SnappyStreamWriter;
import com.JDatabase.Serialize.Serializer;
import com.JDatabase.Serialize.UnsafeMemory;

public class ReadWriteBytePerformance {

	static final String L1LineToParse = "Q,AAPL,ba,17,5,26,12/16/2013,09:02:56.764,557.2100,100,8511,,555.8517,26,09:03:05.963,557.2500,300,26,09:03:05.963,557.3800,500,";
	
	byte[] toWrite = L1LineToParse.getBytes();
    final int warmup = 5000 * 100;
    final int repeats = 5000 * 1000 * 5;
    final int bufferSize = 1024 * 1000;
    String filename = null;
    Serializer serializer = null;
	
	@Before
	public void setUp() throws Exception {
		filename = File.createTempFile("temp", ".tmp").getAbsolutePath();
		serializer = new UnsafeMemory(1024 * 100);
//		System.out.println(filename);
		
	}
	
	@After
	public void tearDown() throws Exception {
		filename = null;
		serializer = null;
	}
	
	private void runWriteTest(String testName, ByteWriter writer) throws IOException {
		for (int count = -warmup; count < 0; count++) writer.write(toWrite);
		writer.flush();
		long overallStartTime = System.currentTimeMillis();
		for (int count = 0; count < repeats; count++) writer.write(toWrite);
		writer.close();
		long overallEndTime = System.currentTimeMillis();
		long bytesRead = ((long) repeats) * toWrite.length;
		System.out.printf("Write Perf: %s - Total Time(sec): %.3f, Total KB: %d, KB/Sec: %.2f%n", testName, (overallEndTime - overallStartTime) / 1000.0, bytesRead / 1000, bytesRead / (double) (overallEndTime - overallStartTime));
	}
	
	private void runReadTest(String testName, ByteReader reader) throws Exception {
		for (int count = -warmup; count < 0; count++) {
			byte[] written = reader.readSingleEvent();
			assertTrue(Arrays.equals(written, toWrite));
		}
		long overallStartTime = System.currentTimeMillis();
		for (int count = 0; count < repeats; count++) {
			byte[] written = reader.readSingleEvent();
			if(written != null) assertTrue(Arrays.equals(written, toWrite));
			else throw new Exception("Didn't write data to disk properly");
		}
		long overallEndTime = System.currentTimeMillis();
		long bytesRead = ((long) repeats) * toWrite.length;
		System.out.printf("Read Perf: %s - Total Time(sec): %.3f, Total KB: %d, KB/Sec: %.2f%n", testName, toWrite.length, (overallEndTime - overallStartTime) / 1000.0, bytesRead / 1000, bytesRead / (double) (overallEndTime - overallStartTime));
	}
	
	@Test
	public void testFileStreamReadWritePerformance() throws Exception {
		String testName = "FileStream";
		runWriteTest(testName, new FileStreamWriter(filename, serializer));
		runReadTest(testName, new FileStreamReader(filename, bufferSize));
	}

	@Test
	public void testSnappyStreamReadWritePerformance() throws Exception {
		String testName = "Snappy";
		runWriteTest(testName, new SnappyStreamWriter(filename, serializer));
		runReadTest(testName, new SnappyStreamReader(filename, bufferSize));
	}
	@Test
	public void testLZ4StreamReadWritePerformance() throws Exception {
		String testName = "LZ4";
		runWriteTest(testName, new LZ4StreamWriter(filename, serializer));
		runReadTest(testName, new LZ4StreamReader(filename, bufferSize));
	}
}
