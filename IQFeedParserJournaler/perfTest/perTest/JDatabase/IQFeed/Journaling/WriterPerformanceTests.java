package perTest.JDatabase.IQFeed.Journaling;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.TimeZone;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.JDatabase.IQFeed.Feed;
import com.JDatabase.Journaler.String.BufferedWriterJournaler;
import com.JDatabase.Journaler.String.ChronicleWriter;
import com.JDatabase.Journaler.String.StringJournaler;

public class WriterPerformanceTests {
	String singleMsg = "Q,AAPL,ba,17,5,26,12/16/2013,09:02:56.764,557.2100,100,8511,,555.8517,26,09:03:05.963,557.2500,300,26,09:03:05.963,557.3800,500,";
	
    final int warmup = 5000 * 10;
    final int repeats = 5000 * 100;
	int[] times;
	
	

	@Before
	public void setUp() throws Exception {
		times = new int[repeats];
	}

	@After
	public void tearDown() throws Exception {
		times = null;
	}

	@Test
	public void testBufferedWriterLatency() throws IOException {	
		StringJournaler writer = new BufferedWriterJournaler(File.createTempFile("temp", ".tmp").getAbsolutePath());
		long startTime, endTime;
		TimeZone timeZone = TimeZone.getDefault();
		long receiveTime = System.currentTimeMillis();
		for (int count = -warmup; count < repeats; count++) {
			startTime = System.nanoTime();
			writer.write(Feed.L1, timeZone, receiveTime, singleMsg);
			endTime = System.nanoTime();
	        if (count >= 0)	times[count] = (int) (endTime - startTime);
		}
		writer.close();
	    Arrays.sort(times);
	    System.out.print("BufferedWriterLatency: ");
	    for (double perc : new double[]{50, 90, 99, 99.9, 99.99}) {
	        System.out.printf("%s%% took %.1f �s, ", perc, (double) times[((int) (repeats * perc / 100))] / 1000.0);
	    }
	    System.out.printf("worst took %d �s%n", times[times.length - 1] / 1000);
	}
	@Test
	public void testChronicleWriterLatency() throws IOException {	
		StringJournaler writer = new ChronicleWriter(File.createTempFile("temp", ".tmp").getAbsolutePath());
		long startTime, endTime;
		TimeZone timeZone = TimeZone.getDefault();
		long receiveTime = System.currentTimeMillis();
		for (int count = -warmup; count < repeats; count++) {
			startTime = System.nanoTime();
			writer.write(Feed.L1, timeZone, receiveTime, singleMsg);
			endTime = System.nanoTime();
	        if (count >= 0)	times[count] = (int) (endTime - startTime);
		}
		writer.close();
	    Arrays.sort(times);
	    System.out.print("ChronicleWriterLatency: ");
	    for (double perc : new double[]{50, 90, 99, 99.9, 99.99}) {
	        System.out.printf("%s%% took %.1f �s, ", perc, (double) times[((int) (repeats * perc / 100))] / 1000.0);
	    }
	    System.out.printf("worst took %d �s%n", times[times.length - 1] / 1000);
	}
}